<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parts', function (Blueprint $table) {
			$table->integer('id', true)->comment('ID');
			$table->string('parts_name', 60)->comment('パーツ名');
			$table->integer('display_no')->nullable()->comment('表示番号');
			$table->text('remarks')->nullable()->comment('備考');
			$table->string('add_user_name', 20)->nullable()->comment('登録者');
			$table->string('upd_user_name', 20)->nullable()->comment('更新者');
			$table->timestamps();
            $table->softDeletes();
        });
        // ALTER 文を実行しテーブルにコメントを設定
        DB::statement("ALTER TABLE parts COMMENT 'パーツマスタ'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parts');
    }
}
