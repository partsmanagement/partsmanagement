<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
			$table->integer('id', true)->comment('ID');
			$table->integer('vehicles_id')->comment('車両ID');
			$table->string('vehicle_file_name')->comment('パーツ画像ファイル名（格納パスはConst定義）');
			$table->string('add_user_name', 20)->nullable()->comment('登録者');
			$table->string('upd_user_name', 20)->nullable()->comment('更新者');
			$table->timestamps();
        });
        // ALTER 文を実行しテーブルにコメントを設定
        DB::statement("ALTER TABLE images COMMENT '画像テーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
