<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufacturersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacturers', function (Blueprint $table) {
			$table->integer('id', true)->comment('ID');
			$table->string('manufacturer_name', 60)->comment('メーカー名');
			$table->string('manufacturer_kana', 80)->nullable()->comment('メーカー名カナ');
			$table->string('add_user_name', 20)->nullable()->comment('登録者');
			$table->string('upd_user_name', 20)->nullable()->comment('更新者');
			$table->timestamps();
            $table->softDeletes();
        });
        // ALTER 文を実行しテーブルにコメントを設定
        DB::statement("ALTER TABLE manufacturers COMMENT 'メーカーマスタ'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacturers');
    }
}
