<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTaxesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('taxes', function(Blueprint $table)
		{
			$table->integer('id', true)->comment('税率ID');
			$table->date('const_date')->comment('施工日');
			$table->decimal('tax_basic_rate', 2, 0)->comment('消費税率');
			$table->decimal('reduction_tax_rate', 2, 0)->comment('軽減税率');
			$table->boolean('tax_flag')->default(1)->comment('税区分（1：外税、2：内税）');
			$table->boolean('tax_calc_flag')->default(1)->comment('税計算区分（1：伝票単位、2：商品単位）');
			$table->boolean('tax_fraction_type')->default(1)->comment('税端数計算区分（1：切捨て、2：四捨五入、3：切上げ）');
			$table->string('add_user_name', 20)->comment('登録者');
			$table->string('upd_user_name', 20)->comment('更新者');
			$table->timestamps();
            $table->softDeletes();
        });
        // ALTER 文を実行しテーブルにコメントを設定
        DB::statement("ALTER TABLE taxes COMMENT '税マスタ'");
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('taxes');
	}

}
