<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id')->comment('ログインID');
			$table->string('user_id', 60)->unique()->comment('スタッフID');
			$table->string('user_name')->comment('スタッフ名');
			$table->string('password')->comment('パスワード');
			$table->string('user_type', 1)->comment('スタッフタイプ')->comment('スタッフタイプ（1：管理者、2：一般、3：得意先)');
			$table->integer('customer_detail_id')->nullable()->comment('得意先詳細ID');
			$table->string('remember_token', 100)->nullable()->comment('オートログイン認証用トークン');
			$table->string('add_user_name', 20)->nullable()->comment('登録者');
			$table->string('upd_user_name', 20)->nullable()->comment('更新者');
			$table->timestamps();
		});
        // ALTER 文を実行しテーブルにコメントを設定
        DB::statement("ALTER TABLE users COMMENT 'ユーザーテーブル'");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
