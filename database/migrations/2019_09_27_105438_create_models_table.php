<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('models', function (Blueprint $table) {
			$table->integer('id', true)->comment('ID');
			$table->string('model_name', 60)->comment('車種名');
			$table->string('model_kana', 80)->nullable()->comment('車種名カナ');
			$table->integer('manufacturer_id')->comment('メーカーID');
			$table->text('remarks')->nullable()->comment('備考');
			$table->string('add_user_name', 20)->nullable()->comment('登録者');
			$table->string('upd_user_name', 20)->nullable()->comment('更新者');
			$table->timestamps();
            $table->softDeletes();
        });
        // ALTER 文を実行しテーブルにコメントを設定
        DB::statement("ALTER TABLE models COMMENT '車種マスタ'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('models');
    }
}
