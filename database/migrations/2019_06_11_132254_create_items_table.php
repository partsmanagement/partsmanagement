<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function(Blueprint $table)
		{
			$table->integer('id', true)->comment('商品ID');
			$table->string('item_code', 20)->comment('商品コード');
			$table->string('item_name', 60)->comment('商品名');
			$table->string('item_name_kana', 80)->nullable()->comment('商品名カナ');
			$table->string('item_short_name', 40)->nullable()->comment('商品簡略名');
			$table->string('item_file_name')->nullable()->comment('商品画像ファイル名（格納パスはConst定義）');
			$table->decimal('item_cost_price', 10)->nullable()->comment('商品原価');
			$table->string('item_method', 60)->nullable()->comment('商品規格');
			$table->decimal('item_quantity', 7)->nullable()->comment('商品入数');
			$table->string('item_quantity_unit', 20)->nullable()->comment('商品入数単位');
			$table->string('item_unit', 20)->nullable()->comment('商品単位');
			$table->decimal('wholesale_basic_price', 8, 0)->nullable()->comment('卸売通常価格');
			$table->decimal('retail_basic_price', 8, 0)->nullable()->comment('小売通常価格');
			$table->decimal('wholesale_sale_price', 8, 0)->nullable()->comment('卸売特売価格');
			$table->decimal('retail_sale_price', 8, 0)->nullable()->comment('小売特売価格');
			$table->decimal('wholesale_rank_price1', 8, 0)->default(0)->comment('卸売ランク価格１');
			$table->decimal('retail_rank_price1', 8, 0)->default(0)->comment('小売ランク価格１');
			$table->decimal('wholesale_rank_price2', 8, 0)->default(0)->comment('卸売ランク価格２');
			$table->decimal('retail_rank_price2', 8, 0)->default(0)->comment('小売ランク価格２');
			$table->decimal('wholesale_rank_price3', 8, 0)->default(0)->comment('卸売ランク価格３');
			$table->decimal('retail_rank_price3', 8, 0)->default(0)->comment('小売ランク価格３');
			$table->decimal('wholesale_rank_price4', 8, 0)->default(0)->comment('卸売ランク価格４');
			$table->decimal('retail_rank_price4', 8, 0)->default(0)->comment('小売ランク価格４');
			$table->decimal('wholesale_rank_price5', 8, 0)->default(0)->comment('卸売ランク価格５');
			$table->decimal('retail_rank_price5', 8, 0)->default(0)->comment('小売ランク価格５');
			$table->integer('item_disp_type')->default(1)->comment('商品表示区分（1：表示、99：非表示）');
			$table->string('add_user_name', 20)->nullable()->comment('登録者');
			$table->string('upd_user_name', 20)->nullable()->comment('更新者');
			$table->timestamps();
            $table->softDeletes();
        });
        // ALTER 文を実行しテーブルにコメントを設定
        DB::statement("ALTER TABLE items COMMENT '商品マスタ'");
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}
