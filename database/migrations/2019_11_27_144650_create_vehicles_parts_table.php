<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles_parts', function (Blueprint $table) {
			$table->integer('id', true)->comment('ID');
			$table->integer('vehicles_id')->comment('車両ID');
			$table->integer('parts_id')->comment('車両パーツID ');
			$table->tinyInteger('parts_status')->default(1)->comment('パーツステータス（1：-、2：OK、3：NG）');
			$table->text('remarks')->nullable()->comment('備考');
			$table->string('add_user_name', 20)->nullable()->comment('登録者');
			$table->string('upd_user_name', 20)->nullable()->comment('更新者');
			$table->timestamps();
        });
        // ALTER 文を実行しテーブルにコメントを設定
            DB::statement("ALTER TABLE vehicles_parts COMMENT '車両パーツテーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles_parts');
    }
}
