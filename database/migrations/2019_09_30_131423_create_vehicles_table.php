<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
			$table->integer('id', true)->comment('ID');
			$table->integer('manufacturer_id')->comment('メーカーID');
//			$table->integer('model_id')->comment('車種ID');
			$table->integer('model_id')->nullable()->comment('車種ID');
			$table->string('model_name', 60)->comment('車種名');
			$table->string('vehicle_number', 40)->nullable()->comment('車体番号');
			$table->string('first_registration_date', 7)->nullable()->comment('初年度検査年月');
			$table->string('vehicle_color', 2)->nullable()->comment('カラーカテゴリー');
			$table->string('color_code', 40)->nullable()->comment('カラー識別コード');
			$table->boolean('transmission')->nullable()->default(1)->comment('トランスミッション（1：AT、2：MT）');
			$table->string('model_number', 40)->nullable()->comment('型式');
			$table->string('prime_mover_model', 40)->nullable()->comment('原動機型式');
			$table->string('model_designation_number', 40)->nullable()->comment('型式指定番号');
			$table->string('category_classification_number', 40)->nullable()->comment('類別区分番号');
			$table->integer('yard_id')->nullable()->comment('ヤードID');
			$table->text('remarks')->nullable()->comment('特記事項');
			$table->string('provider_name', 20)->nullable()->comment('提供者名');
			$table->decimal('acquisition_amount', 10, 0)->nullable()->comment('取得金額');
			$table->text('search_keyword')->nullable()->comment('検索キーワード');
			$table->text('main_file_names')->nullable()->comment('メイン画像ファイル名（格納パスはConst定義）');
			$table->string('add_user_name', 20)->nullable()->comment('登録者');
			$table->string('upd_user_name', 20)->nullable()->comment('更新者');
			$table->timestamps();
            $table->softDeletes();
        });
        // ALTER 文を実行しテーブルにコメントを設定
        DB::statement("ALTER TABLE vehicles COMMENT '車両テーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
