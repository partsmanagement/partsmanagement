<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['id' => '1', 'user_id' => 'administrator', 'user_name' => 'システム管理者','password' => '$2y$10$0IT0/1IxITgeUdWMKix/oOLHFdfIMQ5MkCLvtHTMQXdJmTn75QQaS','user_type' => '1','customer_detail_id' => NULL,'remember_token' => 'fzkaubo8iFzK2Fouqk1JldeMQlHIaZEHBOD6tINuWzYJQLcJinTVk6TFbXnA','add_user_name' => 'システム管理者','upd_user_name' => 'システム管理者','created_at' => '2019-06-04 11:15:01','updated_at' => '2019-06-04 11:15:01','deleted_at' => NULL,],
            ['id' => '2', 'user_id' => 'propeller',     'user_name' => 'プロペラ',      'password' => '$2y$10$BU0/gE/7fgSx5ovdbxoaZ.9PmCeUvBGJ1AhUWK1PE3VBhhEMkGmcK','user_type' => '1','customer_detail_id' => NULL,'remember_token' => NULL,'add_user_name' => 'システム管理者','upd_user_name' => 'システム管理者','created_at' => '2019-06-04 11:15:44','updated_at' => '2019-06-04 11:15:44','deleted_at' => NULL,],
            ['id' => '3', 'user_id' => 'staffa',        'user_name' => 'スタッフA',     'password' => '$2y$10$ryT1nica0/WFdyL2KR9Tb./Cb2j5/KpBpQKCNK9zW.v49DrmDML82','user_type' => '2','customer_detail_id' => NULL,'remember_token' => NULL,'add_user_name' => 'システム管理者','upd_user_name' => 'システム管理者','created_at' => '2019-06-04 11:19:26','updated_at' => '2019-06-04 11:19:26','deleted_at' => NULL,],
            ['id' => '4', 'user_id' => 'staffb',        'user_name' => 'スタッフB',     'password' => '$2y$10$ApwiCY49XLuEc4b8o0TI4uZ7BsBjIpzDvNtzw/E28.cexwUElCaDe','user_type' => '2','customer_detail_id' => NULL,'remember_token' => NULL,'add_user_name' => 'システム管理者','upd_user_name' => 'システム管理者','created_at' => '2019-06-04 11:19:47','updated_at' => '2019-06-04 11:19:47','deleted_at' => NULL,],
            ['id' => '5', 'user_id' => 'staffc',        'user_name' => 'スタッフC',     'password' => '$2y$10$2EO4i0vKvGQg.TKwSAiKDeGDZOPBT6VgbXOZziDURfmFIdkQOAJ/K','user_type' => '2','customer_detail_id' => NULL,'remember_token' => NULL,'add_user_name' => 'システム管理者','upd_user_name' => 'システム管理者','created_at' => '2019-06-04 11:20:14','updated_at' => '2019-06-04 11:20:14','deleted_at' => NULL,],
            ['id' => '6', 'user_id' => 'staffd',        'user_name' => 'スタッフD',     'password' => '$2y$10$w2sCpi6vFGbrJacgGYL4oefaXKUfhQkIttLXvLfiz0bfYOap5m5o.','user_type' => '2','customer_detail_id' => NULL,'remember_token' => NULL,'add_user_name' => 'システム管理者','upd_user_name' => 'システム管理者','created_at' => '2019-06-04 11:20:41','updated_at' => '2019-06-04 11:20:41','deleted_at' => NULL,],
            ['id' => '7', 'user_id' => 'staffe',        'user_name' => 'スタッフE',     'password' => '$2y$10$TSby6AfcLwQgKi8KuHIt7OeCWUrNoE9hS8kJly7SzbXAN5gMw.92K','user_type' => '2','customer_detail_id' => NULL,'remember_token' => NULL,'add_user_name' => 'システム管理者','upd_user_name' => 'システム管理者','created_at' => '2019-06-04 11:21:35','updated_at' => '2019-06-04 11:21:35','deleted_at' => NULL,],
            ['id' => '8', 'user_id' => 'helios',        'user_name' => 'ヘリオスパブ',  'password' => '$2y$10$Berp3vL23sUzcvuf3M/LsOA6g3XM0xTFg0kFNCOhpGeijibQwB2ee','user_type' => '3','customer_detail_id' => '6','remember_token' => NULL,'add_user_name' => 'システム管理者','upd_user_name' => 'システム管理者','created_at' => '2019-06-04 11:21:58','updated_at' => '2019-06-04 11:21:58','deleted_at' => NULL,],
            ['id' => '9', 'user_id' => 'san-a',         'user_name' => 'サンエー本社',  'password' => '$2y$10$VjxKkNql4IdMg2xEL2hJq.PU3shAJ0J1YmorT.wlRqIxz8Lyvd9XG','user_type' => '3','customer_detail_id' => '11','remember_token' => '1BO62o3AWR2dU5IIvo3GFoqMYUSSTTMBCpcIbIHIdn5ugCNs3AoehPpwparA','add_user_name' => 'システム管理者','upd_user_name' => 'システム管理者','created_at' => '2019-06-04 11:25:59','updated_at' => '2019-06-04 11:25:59','deleted_at' => NULL,],
            ['id' => '10','user_id' => 'san-a-chatan',  'user_name' => 'サンエー北谷店','password' => '$2y$10$VjxKkNql4IdMg2xEL2hJq.PU3shAJ0J1YmorT.wlRqIxz8Lyvd9XG','user_type' => '3','customer_detail_id' => '13','remember_token' => '6KxOKuFvYqHGiWOjAPhE1dHaU5cDYEuXkvlKpLp16ODLwi5wwyKUaEPI50pv','add_user_name' => 'システム管理者','upd_user_name' => 'システム管理者','created_at' => '2019-06-04 11:25:59','updated_at' => '2019-06-04 11:25:59','deleted_at' => NULL,],
        ]);
    }
}
