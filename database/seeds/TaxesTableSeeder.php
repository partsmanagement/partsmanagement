<?php

use Illuminate\Database\Seeder;

class TaxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('taxes')->insert([
            ['id' => '1','const_date' => '1989-04-01','tax_basic_rate' => '3','reduction_tax_rate' => '2','tax_flag' => '1','tax_calc_flag' => '1','tax_fraction_type' => '1','add_user_name' => 'システム管理者','upd_user_name' => 'システム管理者','created_at' => '2019-06-11 15:24:09','updated_at' => '2019-06-11 15:24:13','deleted_at' => NULL,],
            ['id' => '2','const_date' => '1997-04-01','tax_basic_rate' => '5','reduction_tax_rate' => '2','tax_flag' => '1','tax_calc_flag' => '1','tax_fraction_type' => '1','add_user_name' => 'システム管理者','upd_user_name' => 'システム管理者','created_at' => '2019-06-11 15:24:09','updated_at' => '2019-06-11 15:24:13','deleted_at' => NULL,],
            ['id' => '3','const_date' => '2014-04-01','tax_basic_rate' => '8','reduction_tax_rate' => '2','tax_flag' => '1','tax_calc_flag' => '1','tax_fraction_type' => '1','add_user_name' => 'システム管理者','upd_user_name' => 'システム管理者','created_at' => '2019-06-11 15:24:09','updated_at' => '2019-06-11 15:24:13','deleted_at' => NULL,],
        ]);
    }
}
