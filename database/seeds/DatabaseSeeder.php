<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call([
            UsersTableSeeder::class,
            ItemsTableSeeder::class,
            TaxesTableSeeder::class,
            ShipRoutesTableSeeder::class,
            ShipsTableSeeder::class,
            CustomerItemPricesTableSeeder::class,
            CustomerTableSeeder::class,
            CustomerDetailsTableSeeder::class,
            ScheduleTableSeeder::class,
        ]);

        Model::reguard();
    }
}
