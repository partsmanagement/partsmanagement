<?php
//DBで定義されている区分などは「defines.php」へ
return [

    'system_name' => env('APP_NAME',      'かたずけ屋'),
    'company_name' => env('COMPANY_NAME', '株式会社 かたずけ屋'),
    'company_zip' => env('COMPANY_ZIP', '〒904-2151'),
    'company_address' => env('COMPANY_ADDRESS', '沖縄県沖縄市松本字松本972番地'),
    'company_tel' => env('COMPANY_TEL', '098-938-5616'),

    'firebase_server_key' => env('FIREBASE_SERVER_KEY'),
    'firebase_server_url' => env('FIREBASE_SERVER_URL'),

    'default_maxrowsize'    => env('DEFAULT_MAXROWSIZE',    10),
    'default_popuprowsize'  => env('DEFAULT_POPUPROWSIZE',  10),
    'default_routerowsize'  => env('DEFAULT_MAXROWSIZE',    100),
    'default_30rows'        => env('DEFAULT_MAXROWSIZE',    30),
    'default_50rows'        => env('DEFAULT_MAXROWSIZE',    50),

    // アップロード画像横表示件数
    'default_upimages_disp' => env('DEFAULT_UPIMAGES_DISP', 8),

    // 非課税対象選択 初期値
    'default_tax_exemption_flag' => env('DEFAULT_TAX_EXEMPTION_FLAG', '1'),
    // 軽減税対象選択 初期値
    'default_reduction_tax_flag' => env('DEFAULT_REDUCTION_TAX_FLAG', '2'),

    // INPUTタグのクラス
    // @see resources/views/layouts/inputs/*.blade.php
    'inputclasses' => [
        'text'          => 'form-control ja input-sm',
        'check'         => 'form-control ja input-sm transform: scale(1.5);',
        'select'        => 'form-control ja input-sm',
        'textarea'      => 'form-control ja input-sm',
        'num'           => 'form-control ja input-sm text-right',
        'datepicker'    => 'form-control ja input-sm datepicker',
        'monthpicker'   => 'form-control ja input-sm monthpicker',
        'yearpicker'    => 'form-control ja input-sm yearpicker',
    ],

    //取込データ識別区分（0：取込なし、1：InfoMart（ホテル）、2：PromotionBox（リウボウ）、3：COREC（飲食店卸売）
    'order_inport_type_list' => [
        '1' => 'InfoMart（ホテル）',
//        '2' => 'PromotionBox（リウボウ）',
//        '3' => 'COREC（飲食店卸売）',
    ],

    //スタッフタイプ（1：管理者、2：一般、3：得意先)
    'user_type_list' => [
        '1' => '管理者',
        '2' => '一般',
        '3' => '得意先',
    ],

    'user_type_list_cd' => [
        'admin'     => 1,   //管理者
        'general'   => 2,   //一般
        'customer'  => 3,   //得意先
    ],

    // トランスミッション（1：AT、2：MT）
    'transmission_list' => [
        '' => '',
        '1' => 'MT',
        '2' => 'AT',
        '3' => 'CVT',
    ],

    // 車両パーツステータス（1：-、2：OK、3：NG）
    'parts_status_list' => [
        '' => '',
        '1' => '--',
        '2' => 'OK',
        '3' => 'NG',
    ],

    // 車両パーツステータスコード（1：-、2：OK、3：NG）
    'parts_status_cd' => [
        '-' => '1',
        'OK' => '2',
        'NG' => '3',
    ],

    // カラーカテゴリー
    'vehicle_color_list' => [
        '1'  => 'ホワイト系',
        '2'  => 'ゴールド・シルバー系',
        '3'  => 'ブラック系',
        '4'  => 'ガンメタ系',
        '5'  => 'シルバー系',
        '6'  => 'レッド系',
        '7'  => 'グリーン系',
        '8'  => 'ワイン系',
        '9'  => 'ブルー系',
        '10' => 'イエロー系',
        '11' => 'オレンジ系',
        '12' => 'パープル系',
        '13' => 'ピンク系',
        '14' => 'ブラウン系',
        '15' => 'その他の色',
    ],

    // お知らせ区分（1：通常、2：重要）
    'notice_type_list' => [
        '' => '',
        '1' => '通常',
        '2' => '重要',
    ],

    //お知らせ対象区分（1：全員、2：管理者、3：スタッフ、４：得意先）
    'notice_target_type_list' => [
        '' => '',
        '1' => '全員',
        '2' => '管理者',
        '3' => 'スタッフ',
        '4' => '得意先',
    ],

    //お知らせ公開フラグ（1：公開、２：非公開）
    'notice_release_flag_list' => [
        '' => '',
        '1' => '公開',
        '2' => '非公開',
    ],

    //税区分
    'tax_flag_list' => [
        '' => '',
        '1' => '外税',
        '2' => '内税',
    ],

    //税区分
    'tax_flag_cd_list' => [
        '外税'      => '1',
        '内税'      => '2',
        '非課税'    => '3',
    ],

    //税計算区分
    'tax_calc_flag_list' => [
        '' => '',
        '1' => '伝票単位',
        '2' => '商品単位',
    ],

    //税端数計算区分
    'tax_fraction_type_list' => [
        '' => '',
        '1' => '切捨て',
        '2' => '四捨五入',
        '3' => '切り上げ',
    ],

    // 販売区分
    'sales_type_list' => [
        '' => '',
        '1' => '卸売販売',          //卸売
        '2' => '小売販売',          //小売
    ],

    // 販売区分（1：卸売、2：小売）
    'sales_type_list_cd' => [
        'wholesale' => '1',         //卸売
        'retail'    => '2',         //小売
    ],

    // 取引区分（1：売掛、2：現金）
    'transaction_type_list' => [
        '' => '',
        '1' => '売掛取引',          //売掛
        '2' => '現金取引',          //現金
    ],

    // 取引価格区分（1：通常、2：特売）
    'transaction_price_type_list' => [
        '' => '',
        '1' => '通常価格',          //通常
        '2' => '特売価格',          //特売
    ],

    // 作業状態（1：未、2：済）
    'status_list' => [
        '' => '',
        '1' => '納品未',         //納品未
        '2' => '納品済',         //納品済
    ],

    // 伝票区分（1：通常伝票、2：取消伝票（赤伝）、3：返却伝票）
    'slip_type_list' => [
        '' => '',
        '1' => '注文伝票',  //注文伝票
        '2' => '取消伝票',  //取消伝票（赤伝）
        '3' => '返却伝票',  //取消伝票（赤伝）
    ],

    // 伝票区分（1：通常伝票、2：取消伝票（赤伝）、3：返却伝票）
    'deliv_slip_type_list' => [
        '' => '',
        '1' => '納品伝票',  //注文伝票
        '2' => '取消伝票',  //取消伝票（赤伝）
        '3' => '返却伝票',  //取消伝票（赤伝）
    ],

    // 納品時間帯区分（0：指定なし、1～はConst定義）
    'delivery_wished_timezone_list' => [
        '0' => '',          //
        '1' => '09時～12時',          //
        '2' => '12時～15時',          //
        '3' => '15時～18時',          //
        '4' => '18時～21時',          //
    ],

    // 軽減税率フラグ（1：対象外、2：対象）
    'reduction_tax_flag_list' => [
        '' => '',
        '1' => '対象外',
        '2' => '対象',
    ],

    // 軽減税率フラグ（1：対象外、2：対象）
    'sp_reduction_tax_flag_list' => [
        '' => '',
        '1' => '軽減税率 対象外',
        '2' => '軽減税率 対象',
    ],

    // 非課税フラグ（1：対象外、2：対象）
    'tax_exemption_flag_list' => [
        '' => '',
        '1' => '課税',
        '2' => '非課税',
    ],

    // 商品表示区分（1：表示、99：非表示）
    'item_disp_type_list' => [
        ''   => '',
        '1'  => '表示',
        '99' => '非表示',
    ],

    // 送信
    'send_flg' => [
        '0' => '送信しない',    //送信しない
        '1' => '送信する',      //送信する
    ],

    // 送信
    'send_flg_cd' => [
        'nosend'    => '0',     //送信しない
        'send'      => '1',     //送信する
    ],

    // 有無
    'ye_or_no_flg' => [
        '1' => '有り',          //有り
        '2' => '無し',          //無し
    ],

    'week' => [
        '0' => '日',
        '1' => '月',
        '2' => '火',
        '3' => '水',
        '4' => '木',
        '5' => '金',
        '6' => '土',
    ],

    // 時刻
    'hour_list' => [
        '07' => '07',
        '08' => '08',
        '09' => '09',
        '10' => '10',
        '11' => '11',
        '12' => '12',
        '13' => '13',
        '14' => '14',
        '15' => '15',
        '16' => '16',
        '17' => '17',
        '18' => '18',
        '19' => '19',
        '20' => '20',
        '21' => '21',
        '22' => '22',
        '23' => '23',
        '00' => '00',
        '01' => '01',
        '02' => '02',
        '03' => '03',
        '04' => '04',
        '05' => '05',
        '06' => '06',
    ],
    // 分
    'minute_list' => [
        '00' => '00',
        '01' => '01',
        '02' => '02',
        '03' => '03',
        '04' => '04',
        '05' => '05',
        '06' => '06',
        '07' => '07',
        '08' => '08',
        '09' => '09',
        '10' => '10',
        '11' => '11',
        '12' => '12',
        '13' => '13',
        '14' => '14',
        '15' => '15',
        '16' => '16',
        '17' => '17',
        '18' => '18',
        '19' => '19',
        '20' => '20',
        '21' => '21',
        '22' => '22',
        '23' => '23',
        '24' => '24',
        '25' => '25',
        '26' => '26',
        '27' => '27',
        '28' => '28',
        '29' => '29',
        '30' => '30',
        '31' => '31',
        '32' => '32',
        '33' => '33',
        '34' => '34',
        '35' => '35',
        '36' => '36',
        '37' => '37',
        '38' => '38',
        '39' => '39',
        '40' => '40',
        '41' => '41',
        '42' => '42',
        '43' => '43',
        '44' => '44',
        '45' => '45',
        '46' => '46',
        '47' => '47',
        '48' => '48',
        '49' => '49',
        '50' => '50',
        '51' => '51',
        '52' => '52',
        '53' => '53',
        '54' => '54',
        '55' => '55',
        '56' => '56',
        '57' => '57',
        '58' => '58',
        '59' => '59',
    ],

    // 時刻

    // 時刻
    'time_list' => [
        '09:00:00' => '09:00',
        '10:00:00' => '10:00',
        '11:00:00' => '11:00',
        '12:00:00' => '12:00',
        '13:00:00' => '13:00',
        '14:00:00' => '14:00',
        '15:00:00' => '15:00',
        '16:00:00' => '16:00',
        '17:00:00' => '17:00',
        '18:00:00' => '18:00',
        '19:00:00' => '19:00',
        '20:00:00' => '20:00',
        '21:00:00' => '21:00',
        '22:00:00' => '22:00',
        '23:00:00' => '23:00',
        '00:00:00' => '00:00',
        '01:00:00' => '01:00',
        '02:00:00' => '02:00',
        '03:00:00' => '03:00',
        '04:00:00' => '04:00',
        '05:00:00' => '05:00',
        '06:00:00' => '06:00',
        '07:00:00' => '07:00',
        '08:00:00' => '08:00',
    ],

    //都道府県
    'pref' => array(
        "01" => "北海道",
        "02" => "青森県",
        "03" => "岩手県",
        "04" => "宮城県",
        "05" => "秋田県",
        "06" => "山形県",
        "07" => "福島県",
        "08" => "茨城県",
        "09" => "栃木県",
        "10" => "群馬県",
        "11" => "埼玉県",
        "12" => "千葉県",
        "13" => "東京都",
        "14" => "神奈川",
        "15" => "新潟県",
        "16" => "富山県",
        "17" => "石川県",
        "18" => "福井県",
        "19" => "山梨県",
        "20" => "長野県",
        "21" => "岐阜県",
        "22" => "静岡県",
        "23" => "愛知県",
        "24" => "三重県",
        "25" => "滋賀県",
        "26" => "京都府",
        "27" => "大阪府",
        "28" => "兵庫県",
        "29" => "奈良県",
        "30" => "和歌山県",
        "31" => "鳥取県",
        "32" => "島根県",
        "33" => "岡山県",
        "34" => "広島県",
        "35" => "山口県",
        "36" => "徳島県",
        "37" => "香川県",
        "38" => "愛媛県",
        "39" => "高知県",
        "40" => "福岡県",
        "41" => "佐賀県",
        "42" => "長崎県",
        "43" => "熊本県",
        "44" => "大分県",
        "45" => "宮崎県",
        "46" => "鹿児島県",
        "47" => "沖縄県",
    ),

    //都道府県
    'pref_key_nm' => array(
        ""         => "",
        "北海道"   => "北海道",
        "青森県"   => "青森県",
        "岩手県"   => "岩手県",
        "宮城県"   => "宮城県",
        "秋田県"   => "秋田県",
        "山形県"   => "山形県",
        "福島県"   => "福島県",
        "茨城県"   => "茨城県",
        "栃木県"   => "栃木県",
        "群馬県"   => "群馬県",
        "埼玉県"   => "埼玉県",
        "千葉県"   => "千葉県",
        "東京都"   => "東京都",
        "神奈川"   => "神奈川",
        "新潟県"   => "新潟県",
        "富山県"   => "富山県",
        "石川県"   => "石川県",
        "福井県"   => "福井県",
        "山梨県"   => "山梨県",
        "長野県"   => "長野県",
        "岐阜県"   => "岐阜県",
        "静岡県"   => "静岡県",
        "愛知県"   => "愛知県",
        "三重県"   => "三重県",
        "滋賀県"   => "滋賀県",
        "京都府"   => "京都府",
        "大阪府"   => "大阪府",
        "兵庫県"   => "兵庫県",
        "奈良県"   => "奈良県",
        "和歌山県" => "和歌山県",
        "鳥取県"   => "鳥取県",
        "島根県"   => "島根県",
        "岡山県"   => "岡山県",
        "広島県"   => "広島県",
        "山口県"   => "山口県",
        "徳島県"   => "徳島県",
        "香川県"   => "香川県",
        "愛媛県"   => "愛媛県",
        "高知県"   => "高知県",
        "福岡県"   => "福岡県",
        "佐賀県"   => "佐賀県",
        "長崎県"   => "長崎県",
        "熊本県"   => "熊本県",
        "大分県"   => "大分県",
        "宮崎県"   => "宮崎県",
        "鹿児島県" => "鹿児島県",
        "沖縄県"   => "沖縄県",
    ),

    'month' => [
        '01' => '1月',
        '02' => '2月',
        '03' => '3月',
        '04' => '4月',
        '05' => '5月',
        '06' => '6月',
        '07' => '7月',
        '08' => '8月',
        '09' => '9月',
        '10' => '10月',
        '11' => '11月',
        '12' => '12月',
    ],

];
