<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attributeを承認してください。',
    'active_url'           => ':attributeは正しいURLではありません。',
    'after'                => ':attributeは:date以降の日付にしてください。',
    'alpha'                => ':attributeは英字のみにしてください。',
    'alpha_dash'           => ':attributeは英数字とハイフンのみにしてください。',
    'alpha_num'            => ':attributeは英数字のみにしてください。',
    'array'                => ':attributeは配列にしてください。',
    'before'               => ':attributeは:date以前の日付にしてください。',
    'between'              => [
        'numeric' => ':attributeは:min〜:maxまでにしてください。',
        'file'    => ':attributeは:min〜:max KBまでのファイルにしてください。',
        'string'  => ':attributeは:min〜:max文字にしてください。',
        'array'   => ':attributeは:min〜:max個までにしてください。',
    ],
    'boolean'              => ':attributeはtrueかfalseにしてください。',
    'confirmed'            => ':attributeは確認用項目と一致していません。',
    'date'                 => ':attributeは正しい日付ではありません。',
    'date_format'          => ':attributeは":format"書式と一致していません。',
    'different'            => ':attributeは:otherと違うものにしてください。',
    'digits'               => ':attributeは:digits桁にしてください',
    'digits_between'       => ':attributeは:min〜:max桁にしてください。',
    'email'                => ':attributeを正しいメールアドレスにしてください。',
    'filled'               => ':attributeは必須です。',
    'exists'               => '選択された:attributeは正しくありません。',
    'image'                => ':attributeは画像にしてください。',
    'in'                   => '選択された:attributeは正しくありません。',
    'integer'              => ':attributeは整数にしてください。',
    'ip'                   => ':attributeを正しいIPアドレスにしてください。',
    'max'                  => [
        'numeric' => ':attributeは:max以下にしてください。',
        'file'    => ':attributeは:max KB以下のファイルにしてください。',
        'string'  => ':attributeは:max文字以下にしてください。',
        'array'   => ':attributeは:max個以下にしてください。',
    ],
    'mimes'                => ':attributeは:valuesタイプのファイルにしてください。',
    'min'                  => [
        'numeric' => ':attributeは:min以上にしてください。',
        'file'    => ':attributeは:min KB以上のファイルにしてください。',
        'string'  => ':attributeは:min文字以上にしてください。',
        'array'   => ':attributeは:min個以上にしてください。',
    ],
    'not_in'               => '選択された:attributeは正しくありません。',
    'numeric'              => ':attributeは数字にしてください。',
    'regex'                => ':attributeの書式が正しくありません。',
    'required'             => ':attributeは必須です。',
    'required_if'          => ':otherが:valueの時、:attributeは必須です。',
    'required_unless'      => ':otherが:value以外の時、:attributeは必須です。',
    'required_with'        => ':valuesが存在する時、:attributeは必須です。',
    'required_with_all'    => ':valuesが存在する時、:attributeは必須です。',
    'required_without'     => ':valuesが存在しない時、:attributeは必須です。',
    'required_without_all' => ':valuesが存在しない時、:attributeは必須です。',
    'same'                 => ':attributeと:otherは一致していません。',
    'size'                 => [
        'numeric' => ':attributeは:sizeにしてください。',
        'file'    => ':attributeは:size KBにしてください。.',
        'string'  => ':attribute:size文字にしてください。',
        'array'   => ':attributeは:size個にしてください。',
    ],
    'string'               => ':attributeは文字列にしてください。',
    'timezone'             => ':attributeは正しいタイムゾーンをしていしてください。',
    'unique'               => ':attributeは既に存在します。',
    'url'                  => ':attributeを正しい書式にしてください。',
    'all_em_kana'          => ':attributeは全角カタカナで入力してください。',
    'all_kana'             => ':attributeはカタカナで入力してください。',
    'tel_hankaku_num'      => ':attributeは半角数字で入力してください。',
    'zip_hyphen'           => ':attributeはハイフン付きの半角数字3桁-半角数字4桁で入力してください。',
    'tel_hyphen_hankaku_num' => ':attributeはハイフン付き半角数字で入力してください。',
    'num_check'            => ':attributeは半角数字で入力してください。',
    'alpha_check'          => ':attributeは半角英字で入力してください。',
    'alpha_num_check'      => ':attributeは半角英数字で入力してください。',
    'alpha_num_check'      => ':attributeは半角英数字で入力してください。',
    'alpha_dash_check'     => ':attributeは半角英数字、半角-または_で入力してください。',
    'file'                 => ':attributeはファイルでなければなりません。',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'delivery_wished_date' => [
            'after'  => '納品希望日は本日以降の日付にしてください。',
            'before' => '納品希望日は本日以前の日付にしてください。',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
            'user_id'                   => 'ユーザーID',
            'user_name'                 => 'ユーザー名',
            'user_type'                 => 'ユーザータイプ',
            'password'                  => '新しいパスワード',
            'password_confirmation'     => '新しいパスワード(確認)',

            //メーカーマスタ項目
            'manufacturer_name'         => 'メーカー名',
            'manufacturer_kana'         => 'メーカー名カナ',

            //車種マスタ項目
            'model_name'                => '車種名',
            'model_kana'                => '車種名カナ',
            'manufacturer_id'           => 'メーカー',

            //パーツマスタ項目
            'parts_name'                => 'パーツ名',

            //ヤードマスタ項目
            'yard_name'                 => 'ヤード名',

            //車両項目
            'model_id'                          => '車種',
            'vehicle_number'                    => '車体番号',
            'first_registration_date'           => '初年度検査年月',
            'vehicle_color'                     => '色',
            'transmission'                      => 'AT/MT',
            'model_number'                      => '型式',
            'prime_mover_model'                 => '原動機型式',
            'model_designation_number'          => '型式指定番号',
            'category_classification_number'    => '類別区分番号',
            'yard'                              => 'ヤード',
            'provider_name'                     => '提供者名',
            'acquisition_amount'                => '取得金額',
            'parts'                             => 'パーツ',
            'remarks'                           => '特記事項',
            'search_keyword'                    => '検索キーワード',
            'main_file_names'                   => 'メイン車両画像',
            //得意先マスタ項目
            'customer_name'             => '得意先名',
            'customer_name_kana'        => '得意先名カナ',
            'zip_code'                  => '郵便番号',
            'pref_name'                 => '都道府県名',
            'city_name'                 => '市町村名',
            'address1'                  => '住所',
            'address2'                  => '住所２',
            'telno'                     => '電話番号',
            'faxno'                     => 'FAX番号',
            'email'                     => 'メールアドレス',
            'close_site'                => '請求締日',
            'remarks'                   => '備考',

            //得意先詳細マスタ項目
//            'id'                        => '得意先詳細ID',
            'customer_id'               => '得意先ID',
            'customer_detail_name'      => '納品先名',
            'customer_detail_name_kana' => '納品先名カナ',
            'delivery_location_name'    => '納品場所名',
            'responsible_name'          => 'ご担当者名',
            'transaction_code'          => '取引先コード',
            'customer_claim_id'         => '請求先',
            'sales_type'                => '販売区分',
            'rank'                      => 'ランク',

            //商品マスタ項目
            'item_code'                 => '商品コード',
            'item_name'                 => '商品名',
            'item_name_kana'            => '商品名カナ',
            'item_short_name'           => '商品簡略名',
            'item_file_name'            => '商品画像ファイル名',
            'item_cost_price'           => '商品原価',
            'item_method'               => '商品規格',
            'item_quantity'             => '商品入数',
            'item_quantity_unit'        => '商品入数単位',
            'item_unit'                 => '商品単位',
            'wholesale_basic_price'     => '卸売通常価格',
            'retail_basic_price'        => '小売通常価格',
            'wholesale_sale_price'      => '卸売特売価格',
            'retail_sale_price'         => '小売特売価格',
            'wholesale_rank_price1'     => '卸売ランク価格１',
            'retail_rank_price1'        => '小売ランク価格１',
            'wholesale_rank_price2'     => '卸売ランク価格２',
            'retail_rank_price2'        => '小売ランク価格２',
            'wholesale_rank_price3'     => '卸売ランク価格３',
            'retail_rank_price3'        => '小売ランク価格３',
            'wholesale_rank_price4'     => '卸売ランク価格４',
            'retail_rank_price4'        => '小売ランク価格４',
            'wholesale_rank_price5'     => '卸売ランク価格５',
            'retail_rank_price5'        => '小売ランク価格５',
            'item_disp_type'            => '商品表示区分',

            //税率マスタ項目
            'const_date'                => '施工日',
            'tax_basic_rate'            => '消費税率',
            'reduction_tax_rate'        => '軽減税率',
            'tax_flag'                  => '税区分',
            'tax_calc_flag'             => '税計算区分',
            'tax_fraction_type'         => '税端数計算区分',

            //得意先別商品価格マスタ項目
            'customer_detail_id'        => '納品先',
            'ext_system_item_code'      => '外部システム連携用商品コード',
            'item_id'          			=> '商品',
            'cust_wholesale_rank_price' => '卸売価格',
            'cust_retail_rank_price'    => '小売価格',
            'cust_wholesale_sale_price' => '卸売特売価格',
            'cust_retail_sale_price'    => '小売特売価格',

            // 注文データ
            'delivery_wished_date'      => '納品希望日',
            'slip_type'                 => '伝票区分',
            'slip_date'                 => '伝票日付',
            'transaction_type'          => '取引区分',
            'transaction_price_type'    => '取引価格',

            'item_code.*'               => '商品コード',
            'item_name.*'               => '商品名',
            'quantity.*'                => '数量',
            'unit.*'                    => '単位',
            'item_price.*'              => '単価額',
            'amount_taxnon.*'           => '金額',
            'remark.*'                  => '備考',

            // CSVファイル外部注文取り込み
            'csv_file'                  => 'CSVファイル',
            'slip_date.*'               => '伝票日付',
            'inport_slip_no.*'          => '伝票No',
            'delivery_location_name.*'  => '納品場所名',
            'delivery_wished_date.*'    => '納品日',
            'responsible_name.*'        => 'ご担当者名',
            'total_amount_taxnon.*'     => '総合計',
            'tax_flag.*'                => '税区分',
            'total_tax_amount.*'        => '合計消費税額',
            'tax_rate.*'                => '消費税率',

            // ルート設定
            'ship_route_name'           => 'ルート名',
            'staff_id'                  => '担当者名',


    ],

];
