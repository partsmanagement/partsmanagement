<?php
    $title = 'ファイルアップ';
    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';
    // コントローラ
    $subnavs = [
        ['text'=>'一覧','href'=>url('item')],
    ];

    // モーダルウィンドウ
    $modalid_customer = 'findcustomer';

    // ダイアログ
    $modalid_delete = 'dialogdelete';

?>
@extends('layouts.app')

@section('content')
{{ Form::open(['id'=>'input-form', 'method' => 'post', 'url' => 'item/save', 'files' => true, 'class'=>'form-horizontal form-label-left']) }}
    <div class="row justify-content-center w1024">
        <div class="col-12">
            <div class="card">
                <!-- card-body -->
                <div class="card-body">
                    @if( !empty( Funcs::rq('id', $data['results']) ) )
                        <div class="row pb-2 pl-1">
                            <div class="w130 mr-2 align-self-center text-nowrap text-right">ID</div>
                            <div class="w600 mr-2 align-self-center">
                                {{ Funcs::rq('id', $data['results']) }}
                            </div>
                        </div><!--/.row-->
                    @endif
                    <div class="row pb-2 pl-1">
                        <div class="w130 mr-2 align-self-center text-nowrap text-right">商品画像</div>
                        <div class="w350 mr-2 align-self-center">
                            <div class="form-group">
                                @if(!empty(Funcs::rq('item_file_name', $data['results'])))
                                <div class="pb-1">
                                    <img id="itemimg" src="{{ asset('storage/items/200-' . Funcs::rq('item_file_name', $data['results'])) }}" alt="item-image" style="width:200px;height:200px;"/>
                                </div>
                                @else
                                <div class="pb-1">
                                    <img id="itemimg" src="{{ asset('storage/items/200-nophoto.png') }}" alt="item-image" style="width:200px;height:200px;"/>
                                </div>
                                @endif
                                 <div class="mt-2">
                                    <div class="btn-group pb-1">
                                        <div class="pr-1">
                                            @if(!empty(Funcs::rq('item_file_name', $data['results'])))
                                                <button id="delimage" type="button" class="btn btn-danger btn-sm" style="display: block;">商品画像を削除</button>
                                            @else
                                                <button id="delimage" type="button" class="btn btn-danger btn-sm" style="display: none;">商品画像を削除</button>
                                            @endif
                                        </div>
                                        <div class="pr-1">
                                            {!! Form::file('item_file_name', ['id' => 'item_file_name']) !!}
                                        </div>
                                    </div>
                                </div>
                                {{ Form::hidden('nophoto_file', asset('storage/items/200-nophoto.png')) }}
                                {{ Form::hidden('item_db_filename',   Funcs::rq('item_file_name', $data['results'])) }}
                                {{ Form::hidden('item_del_filename',  '') }}
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                            @if( !empty( Funcs::rq('id', $data['results']) ) )
                                <!-- 編集の時のみ動作する -->
                                <button type="button" id="btn_delete" class="btn btn-danger" data-toggle="modal" data-target="#{{$modalid_delete}}">削除</button>
                            @endif
                            {{ Form::button('登録', ['id'=>'btn_save', 'class' => 'btn btn-primary', 'data-toggle'=>'tooltip', 'title'=>'入力データを保存する']) }}
                            <a href="{{url('/item')}}" class="btn btn-success" >戻る</a>
                        </div>
                    </div>
                    <br>
                    <br>
                </div>
                <!-- / card-body -->

                {{ Form::hidden('id',               Funcs::rq('id',             $data['results'])) }}
            </div>
            <br>
            <br>
            <br>
        </div>
    </div>
    @csrf
{{ Form::close() }}
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
<!-- イベント -->
<script type="text/javascript">
$(function(){
    //
    $('#btn_save').on('click', function(){
        $('#input-form').submit();
    });
});
</script>
<!-- /イベント -->
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')
<!-- イベント -->
<script type="text/javascript">
$(function(){

    // 小数点以下の桁数を返す
    function getNumPointLength(num) {
        var num = String(num).split('.'),
        result = 0;
        if (num[1]) {
            result = num[1].length;
        }
        return result;
    };

    // 小数点1桁の入力制限チェック
    $('.decimal_point_1').on('change',  function () {
        var len = getNumPointLength($(this).val());
        if(len > 1) {
            alert('小数点第1位で入力してください。');
            $(this).focus();
        }
    });

});

$(function(){
    $('#item_file_name').change(function(e){
        var file = e.target.files[0];
        var reader = new FileReader();

        if(file.type.indexOf("image") < 0){
          alert("画像ファイルを指定してください。");
          return false;
        }

        //選択した画像ファイルのプレビュー表示
        reader.onload = (function(file){
          return function(e){
            $("#itemimg").attr("src", e.target.result);
            $("#itemimg").attr("title", file.name);
          };
        })(file);

        // 画像ファイル削除ボタンを表示
        $("#delimage").css("display", "block");

        //削除する画像ファイル名（hidden）をクリア
        $("[name=item_del_filename]").val("");

        reader.readAsDataURL(file);
    });

    $('#delimage').click(function(e){
        //プレビュー表示の画像をNo Photo画像に差し替る
        var nophoto_file = $("[name=nophoto_file]").val();
        $("#itemimg").attr("src", nophoto_file);
        
        //削除する画像ファイル名をhiddenにセット
        $("[name=item_del_filename]").val( $("[name=item_db_filename]").val());
        
        //選択した画像ファイル名の表示クリア
        $("[name=item_file_name]").val("");
        
        // 画像ファイル削除ボタンを非表示
        $("#delimage").css("display", "none");
    });
});

</script>
<!-- /イベント -->

<!-- 削除 -->
@if( !empty( Funcs::rq('id', $data['results']) ) )
    @component('layouts.modal')
        @slot('modalid', $modalid_delete)
        @slot('modaltitle', '削除')
        @slot('modalcontent')
            削除してよろしいですか？
        @endslot
        @slot('modalfooter')
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="dodelete();return false;">はい</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">いいえ</button>
            <script type="text/javascript">
                function dodelete(){
                    $('#form-delete').submit();
                }
            </script>
            <form id="form-delete" method="post" action="{{url('item/delete').'/'.Funcs::rq('id',$data['results'])}}">
                {!! Form::hidden('id', Funcs::rq('id',$data['results'])) !!}
                @csrf
            </form>
        @endslot
    @endcomponent
@endif

@endsection
