<?php
    // ページタイトル
    $title = 'パーツ一覧';
    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';
    // コントローラ
    $subnavs = [
        ['text'=>'新規登録','href'=>url('parts/edit')],
    ];

    // モーダルウィンドウ
    $modalid_clear = 'catalog_alert_clear';
?>
@extends('layouts.app')

@section('content')

<script type="text/javascript">
    $(function()
    {
        //初期表示行(ヘッダ行込み)
        var tatalRow = 11;

        //初期表示6行作成
        var rows = tbl.rows;
        if (rows.length < tatalRow) {
            var startRows = tatalRow - rows.length;
            for(var i = 0; i < startRows; i++){
                addRow(); //空行作成
            }
        }else{
            autoNumber();
//            addRow(); //空行作成
        }

        $("table").tableDnD({
            dragHandle: ".handle img",
            onDrop: function(){
                autoNumber();
            }
        });

    });

    /* 空行追加 */
    function addRow()
    {
        $("#row_delete").attr('disabled', false);
        $lastTr = $('#tbl tr:last').clone( true );
        $lastTr.find('label').text('');
        $lastTr.find('input[type=text]').val('');
        $lastTr.find('input[type=hidden]').val('');
        $lastTr.find('a').text('');
        $lastTr.find('select').val(1);
        $lastTr.find('textarea').val('');
        $lastTr.find('.help-block').empty();
        $lastTr.find('.has-error').attr('class', '');
        $('#tbl').append($lastTr);
        autoNumber();
    }

    /* 行番号の再採番 */
    function autoNumber()
    {
        var rows = tbl.rows;
        //ヘッダー行考慮のため"i"は2。
        for(var i = 1; i < rows.length; i++){
            //No.ラベル
            rows[i].cells[1].firstChild.nodeValue = (parseInt(i));
            //No.HIDDIN
            var objInput = rows[i].cells[1].childNodes[1];
            objInput.value = (parseInt(i));

            //TR.id連番の振り直し(tableDnDでは行番号が必須のため)
            rows[i].setAttribute("id", "id"+(parseInt(i)));

            //入力項目のname連番の振り直し
            rows[i].cells[0].childNodes[1].setAttribute("name", "id["+(parseInt(i))+"]");
            rows[i].cells[0].childNodes[3].setAttribute("id",   "test_img"+(parseInt(i)));
            rows[i].cells[1].childNodes[1].setAttribute("name", "no["+(parseInt(i))+"]");
//            rows[i].cells[2].childNodes[1].childNodes[1].childNodes[1].setAttribute("name", "customer_detail_id["+(parseInt(i))+"]");
//            rows[i].cells[2].childNodes[1].childNodes[3].childNodes[1].setAttribute("name", "customer_detail_name["+(parseInt(i))+"]");
//            rows[i].cells[3].childNodes[1].setAttribute("name", "customer_name["+(parseInt(i))+"]");
//            rows[i].cells[5].childNodes[1].setAttribute("name", "address["+(parseInt(i))+"]");

        }
    }

</script>

<style>
    .tDnD_whileDrag {
        background-color: #E8F1FB;
    }

    .handle img{
        cursor: move;
    }

    #map {
        width: 600px;
        height: 400px;
        margin: 0 auto;
    }

</style>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form id="form-search" method="get" action="{{ url('parts/search') }}" class="form-horizontal form-label-left">
                    <div class="row pb-2 pl-1">
                        <div class="w50 mx-3 align-self-center text-nowrap">パーツ名</div>
                        <div class="w300 mx-3 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'parts_name')
                            @endcomponent
                        </div>
                    </div><!-- .row -->
                    <div class="col">
                        <div class="row justify-content-end">
                            <div class="mx-3">
                                <button type="button" id="btn_exec" class="btn btn-primary">検索</button>
                            </div>
                            <div class="mx-3">
                                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#{{$modalid_clear}}">クリア</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-12">
        <div class="card">
            {{ Form::open(['id'=>'input-form', 'method' => 'post', 'url' => 'parts/saveno', 'files' => true, 'class'=>'form-horizontal form-label-left']) }}
            <!--div class="card-header"></div-->
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row mb-2">
                            <div class="col">
                                {{ Form::button('表示順変更', ['id'=>'btn_save', 'class' => 'btn btn-success', 'data-toggle'=>'tooltip', 'title'=>'入力データを保存する']) }}
                            </div>
                        </div>
                        <table id="tbl" class="table table-responsive table-striped table-condensed jambo_table bulk_action table_frame table-hove table-bordered">
                            <thead class="thead-light">
                                <tr>
                                    <!--<th scope="col" width="3%">ID</th>-->
                                    <th scope="col" width="3%" colspan="2">表示順番</th>
                                    <th scope="col" width="90%">車種名</th>
                                    <th scope="col" width="7%">編集</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; ?>
                                @foreach($results as $row)
                                <tr class="odd pointer itm" id="id{{$i}}">
                                    <td class="text-center handle" width="20px">
                                        {!! Form::hidden('id['.$i.']', Funcs::rq('id', $row)) !!}
                                        <img id="{{ 'test_img'.$i }}" width="20" height="20" src="{{ asset('asset/imgs/narabikae.png') }}">
                                    </td>
                                    <td class="text-center handle" width="20px">
                                        {{$i+1}}
                                        {!! Form::hidden('no['.$i.']', $i+1) !!}
                                    </td>
                                    <!--<td class="text-right">{{ $row->id }}</td>-->
                                    <td class="text-left">
                                        {{ $row->parts_name }}
                                        {!! Form::hidden('parts_name['.$i.']', $row->parts_name) !!}
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-primary" href="{{url('parts/edit')}}/{{$row->id}}">編集</a>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
            </div>
            @csrf
            {{ Form::close() }}
        </div>
        <br>
        <br>
    </div>
</div>
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
<!-- イベント -->
<script type="text/javascript">
$(function(){
    //
    $('#btn_exec').on('click', function(){
        $('#form-search').submit();
    });

    $('#btn_save').on('click', function(){
        $('#input-form').submit();
    });
});
</script>
<!-- /イベント -->
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')

@component('layouts.modal')
    @slot('modalid', $modalid_clear)
    @slot('modaltitle', 'クリア')
    @slot('modalcontent')
        クリアします
    @endslot
    @slot('modalfooter')
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doclear();return false;">はい</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">いいえ</button>
        <script type="text/javascript">
            function doclear(){
                window.location.href='{{ url('parts/search') }}';
            }
        </script>
    @endslot
@endcomponent

@endsection
