<?php
/**
*
*/
?>
@component('layouts.modal')
    @slot('modalid', $modalid_item)
    @slot('modaltitle', '商品検索')
    @slot('modalsize', 1)
    @slot('modalcontent')
    <div class="container">
        <div class="row pb-3">
            <div class="col-12">
                <div class="input-group ">
                    <input class="form-control ja input-sm" id="finditem_keyword" name="finditem_keyword" type="text" value="">
                    <span class="input-group-append">
                        <button class="btn btn-primary" type="button" tabindex="-1" onclick="{{$modalid_item}}_findit();return false;">
                            検索
                        </button>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <!-- 検索結果 -->
                <div id="{{$modalid_item}}_result_container">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
            </div>
        </div>
    </div>
    @endslot
    @slot('modalfooter')
        <div class="container">
            <div class="row">
                <div class="col-2">
                    <button id="{{$modalid_item}}_addnew" type="button" class="btn btn-info" data-dismiss="modal">
                        新規
                    </button>
                </div>
                <!--div class="col">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doclear();return false;">
                        撰択
                    </button>
                </div-->
            </div>
        </div>
    @endslot
    @slot('modalevents')
        <script type="text/javascript">
            // 検索ボタン
            // モーダルが開く前に検索を実行する
            $('#{{$modalid_item}}').on('show.bs.modal', function(){
                {{$modalid_item}}_findit();
            });
            // 検索実行
            function {{$modalid_item}}_findit(page=0){
                $('#{{$modalid_item}}_result_container').empty();
                //リクエスト
                $.ajax({
                    url: "{{url('search/item/catalog')}}",
                    type: "get",
                    /*dataType: 'json',*/
                    data: { finditem_keyword: $('#finditem_keyword').val(), page: page },
                    fail: function() {
                        var res = $('<div>該当するデータがありません。</div>');
                        $('#{{$modalid_item}}_result_container').append(res);
                    },
                    success: function(response) {
                        if (response.length == 0){
                            var res = $('<div>該当するデータがありません。</div>');
                            $('#{{$modalid_item}}_result_container').append(res);
                        }else{
                            $('#{{$modalid_item}}_result_container').append(response);
                        }
                    }
                });
            }
        </script>
    @endslot
@endcomponent
