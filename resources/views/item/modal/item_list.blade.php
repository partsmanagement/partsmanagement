<div class="scroll480">
@forelse($result as $key=>$row)
    @if($loop->first)
    <table class="table table-striped table-hover table-bordered">
        <tbody>
    @endif
        <tr onclick="setitem({{$row['id']}});">
            <td class="text-left" width="110px">
                @if(!empty($row['item_file_name']))
                    <img src="{{ asset('storage/items/100-' .$row['item_file_name']) }}" alt="item-image" />
                @else
                    <img src="{{ asset('storage/items/100-nophoto.png') }}" alt="item-image" />
                @endif
            </td>
            <td>
                <?php
                    $item_quantity = $row['item_quantity'];
                    $item_quantity_decimalnon  = (preg_match('/\./', $item_quantity)) ? preg_replace('/\.?0+$/', '', $item_quantity) : $item_quantity;
                ?>
                <div class="fs20">{{ $row['item_name'] }}</div>
                <div class="fs10">{{ $row['item_name_kana'] }}</div>
                <div class="fs10">商品コード：{{ $row['item_code'] }}&nbsp;／&nbsp;商品規格：{{ $row['item_method'] }}&nbsp;／&nbsp;商品入数：{{ $item_quantity_decimalnon }}{{ $row['item_quantity_unit'] }}&nbsp;／&nbsp;商品単位：{{ $row['item_unit'] }}</div>
                <div class="fs12">卸売通常価格：&yen;{{ $row['wholesale_basic_price'] }}&nbsp;／&nbsp;小売通常価格：&yen;{{ $row['retail_basic_price'] }}</div>
                <div class="fs12">卸売特売価格：&yen;{{ $row['wholesale_sale_price'] }}&nbsp;／&nbsp;小売特売価格：&yen;{{ $row['retail_sale_price'] }}</div>
            </td>
        </tr>
    @if($loop->last)
        </tbody>
    </table>
    @endif
@empty
    <!-- 該当なし -->
@endforelse
</div>
