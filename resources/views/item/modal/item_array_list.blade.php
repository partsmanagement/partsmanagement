<div class="scroll480">
@forelse($result as $key=>$row)
    @if($loop->first)
    <table class="table table-striped table-hover table-bordered">
        <tbody>
    @endif
        <tr onclick="setitem({{$row['id']}});">
            <td><a name="{{ mb_substr($row['item_name_kana'],1,1 ) }}"></a>{{$row['item_name']}}</td>
        </tr>
    @if($loop->last)
        </tbody>
    </table>
    @endif
@empty
    <!-- 該当なし -->
@endforelse
</div>
