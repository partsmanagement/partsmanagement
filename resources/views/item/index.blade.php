<?php
    // ページタイトル
    $title = '商品一覧';
    // コントローラ
    $subnavs = [
        ['text'=>'新規登録','href'=>url('item/edit')],
        ['text'=>'商品一覧をCSV出力','href'=>'#', 'class'=>'btn-success', 'title'=>'商品一覧をCSV出力','id'=>'csvput'],
    ];

    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';

    // モーダルウィンドウ
    $modalid_clear = 'item_alert_clear';
?>
@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form id="form-search" method="get" action="{{ url('item/search') }}" class="form-horizontal form-label-left">
                    <div class="row pb-2 pl-1">
                        <div class="w60 mx-3 align-self-center text-nowrap">商品名</div>
                        <div class="w300 mx-3 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'item_name')
                            @endcomponent
                        </div>
                    </div><!-- .row -->
                    <div class="row pb-2 pl-1">
                        <div class="w60 mx-3 align-self-center text-nowrap">商品名カナ</div>
                        <div class="w300 mx-3 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'item_name_kana')
                            @endcomponent
                        </div>
                    </div><!-- .row -->
                    <div class="row pb-2 pl-1 align-items-start">
                        <div class="col">
                            <div class="row justify-content-end">
                                <div class="mx-3">
                                    <button type="button" id="btn_exec" class="btn btn-primary">検索</button>
                                </div>
                                <div class="mx-3">
                                    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#{{$modalid_clear}}">クリア</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- .row -->
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-12">
        <div class="card">
            <!--div class="card-header"></div-->
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <!-- ページャ -->
            			<nav  class="nav_style_height">
            			    {{ $results->links() }}
            			</nav>
            			<!-- /ページャ -->
                    </div>
                </div>
                <div class="number_style">
                    @if ( $results->Total() )
                        該当件数 <span>{{ $results->Total() }}</span> 件のうち <span>{{ $results->firstItem() }} ～ {{ $results->lastItem() }}</span> 件目を表示
                    @else
                        該当件数 <span>{{ $results->Total() }}</span> 件
                    @endif
                </div>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="thead-light text-center">
                                <tr>
                                    <th scope="col" width="3%">ID</th>
                                    <th scope="col" width="8%">商品コード</th>
                                    <th scope="col" width="20%" colspan="2">商品名</th>
                                    <th scope="col" width="5%">商品入数</th>
                                    <th scope="col" width="5%">商品単位</th>
                                    <th scope="col" width="5%">商品原価</th>
                                    <th scope="col" width="5%">卸売通常価格</th>
                                    <th scope="col" width="5%">小売通常価格</th>
                                    <th scope="col" width="5%">卸売特売価格</th>
                                    <th scope="col" width="5%">小売特売価格</th>
                                    <th scope="col" width="5%">編集</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($results as $row)
                                <tr>
                                    <td class="text-right">{{ $row->id }}</td>
                                    <td class="text-left">{{ $row->item_code }}</td>
                                    <td class="text-left" width="110px">
                                        @if(!empty($row->item_file_name))
                                            <img src="{{ asset('storage/items/100-' .$row->item_file_name) }}" alt="item-image" />
                                        @else
                                            <img src="{{ asset('storage/items/100-nophoto.png') }}" alt="item-image" />
                                        @endif
                                    </td>
                                    <td class="text-left" >
                                        <div style="font-size : 20px;">{{ $row->item_name }}</div>
                                        <span>{{ $row->item_name_kana }}</span>
                                    </td>
                                    <td class="text-center">
                                        <?php
                                            $item_quantity = Funcs::rq('item_quantity', $row);
                                            $item_quantity_decimalnon  = (preg_match('/\./', $item_quantity)) ? preg_replace('/\.?0+$/', '', $item_quantity) : $item_quantity;
                                        ?>
                                        {{ $item_quantity_decimalnon }}&nbsp;{{ $row->item_quantity_unit }}
                                    </td>
                                    <td class="text-center">{{ $row->item_unit }}</td>
                                    <td class="text-right">&yen;{{ number_format( $row->item_cost_price, 0) }}</td>
                                    <td class="text-right">&yen;{{ number_format( $row->wholesale_basic_price, 0) }}</td>
                                    <td class="text-right">&yen;{{ number_format( $row->retail_basic_price, 0) }}</td>
                                    <td class="text-right">&yen;{{ number_format( $row->wholesale_sale_price, 0) }}</td>
                                    <td class="text-right">&yen;{{ number_format( $row->retail_sale_price, 0) }}</td>
                                    <td class="text-center">
                                        <a class="btn btn-primary" href="{{url('item/edit')}}/{{$row->id}}">編集</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <!-- ページャ -->
            			<nav  class="nav_style_height">
            			    {{ $results->links() }}
            			</nav>
            			<!-- /ページャ -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
<!-- イベント -->
<script type="text/javascript">
$(function(){
    $('#btn_exec').on('click', function(){
        $('#form-search').submit();
    });

    $('#csvput').click(function() {
        var item_name = $('#item_name').val();
        var item_name_kana = $('#item_name_kana').val();
        var action_url = '{{ url('item/csvput') }}';
        var prms = '?item_name=' + item_name +
                    '&item_name_kana=' + item_name_kana;
        window.open(action_url + prms, '_self');
        return true;
    });
});
</script>
<!-- /イベント -->
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')

@component('layouts.modal')
    @slot('modalid', $modalid_clear)
    @slot('modaltitle', 'クリア')
    @slot('modalcontent')
        クリアします
    @endslot
    @slot('modalfooter')
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doclear();return false;">はい</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">いいえ</button>
        <script type="text/javascript">
            function doclear(){
                window.location.href='{{ url('item/search') }}';
            }
        </script>
    @endslot
@endcomponent

@endsection
