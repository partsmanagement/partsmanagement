<?php
    if (!empty( Funcs::rq('id', $data['results']))) {
        // ページタイトル
        $title = '編集';
    } else {
        // ページタイトル
        $title = '登録';
    }    

    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';

    // コントローラ
    $subnavs = [
        ['text'=>'注文一覧','href'=>url('order')],
    ];

    // ダイアログ
    $modalid_delete = 'dialogdelete';   //削除
?>
@extends('layouts.app')

@section('content')

{{ Form::open(['id'=>'input-form', 'method' => 'post', 'url' => 'mypage/save', 'class'=>'form-horizontal form-label-left']) }}
    <div class="row justify-content-center w1024">
        <div class="col-12">
            <div class="card">
                <!-- card-body -->
                <div class="card-body">
                    <div class="row pb-2 pl-1">
                        <div class="w200 mr-2 align-self-center text-nowrap">会社名</div>
                        <div class="w400 mr-2 align-self-center">
                            {{ Funcs::rq('customer_detail_name', $data['results']) }}
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w200 mr-2 align-self-center text-nowrap">ID</div>
                        <div class="w400 mr-2 align-self-center">
                            {{ Funcs::rq('user_id', $data['results']) }}
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w200 mr-2 align-self-center text-nowrap">担当者</div>
                        <div class="w400 mr-2 align-self-center">
                            {{ Funcs::rq('user_name', $data['results']) }}
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w200 mr-2 align-self-center text-nowrap">新しいパスワード<span class="label label-danger ml-1">必須</span></div>
                        <div class="w400 mr-2 align-self-center">
                            @component('layouts.inputs.password')
                                @slot('accesskey', 'password')
                                @slot('loading', $data['results'])
                                @slot('default', '')
                                @slot('placeholder', '')
                                @slot('options', ['autocomplete'=>'off', 'class'=> $errors->has("password") ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                            <span style="color:#dc3545;">{{ $errors->first('password') }}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w200 mr-2 align-self-center text-nowrap">新しいパスワード(確認)<span class="label label-danger ml-1">必須</span></div>
                        <div class="w400 mr-2 align-self-center">
                            @component('layouts.inputs.password')
                                @slot('accesskey', 'password_confirmation')
                                @slot('loading', $data['results'])
                                @slot('placeholder', '')
                                @slot('options', ['autocomplete'=>'off', 'class'=> $errors->has("password_confirmation") ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                            <span style="color:#dc3545;">{{ $errors->first('password_confirmation') }}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <!-- 更新 -->
                        @if( !empty( Funcs::rq('id', $data['results']) ) )

                            @if ( 'administrator' == $user_id || 'root' == $user_id )
                                <div class="pl-2">
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#{{$modalid_delete}}">削除</button>
                                </div>
                            @endif
                            <div class="pl-2">
                                {{ Form::submit('更新', ['id'=>'btn_save', 'class' => 'btn btn-primary', 'data-toggle'=>'tooltip', 'title'=>'入力データを保存する']) }}
                            </div>
                        @else
                        <!-- 新規 -->
                            <div class="pl-2">
                                {{ Form::submit('登録', ['id'=>'btn_save', 'class' => 'btn btn-primary', 'data-toggle'=>'tooltip', 'title'=>'入力データを保存する']) }}
                            </div>
                        @endif
                        <div class="pl-2">
                            <a href="{{url('/order')}}" class="btn btn-success" >戻る</a>
                        </div>
                    </div>
                </div>
                <!-- / card-body -->
                <!-- 更新の時post -->
                {{ Form::hidden('id',                   Funcs::rq('id',                     $data['results'])) }}
                {{ Form::hidden('user_id',              Funcs::rq('user_id',                $data['results'])) }}
                {{ Form::hidden('user_name',            Funcs::rq('user_name',              $data['results'])) }}
                {{ Form::hidden('user_type',            Funcs::rq('user_type',              $data['results'])) }}
                {{ Form::hidden('customer_detail_id',   Funcs::rq('customer_detail_id',     $data['results'])) }}
                {{ Form::hidden('customer_detail_name', Funcs::rq('customer_detail_name',   $data['results'])) }}
            </div>
        </div>
    </div>
    @csrf
{{ Form::close() }}
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')

@if( !empty( Funcs::rq('id', $data['results']) ) )
    <!-- 削除 -->
    @component('layouts.modal')
        @slot('modalid', $modalid_delete)
        @slot('modaltitle', '削除')
        @slot('modalcontent')
            削除します、よろしいですか？
        @endslot
        @slot('modalfooter')
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="dodelete();return false;">OK</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCEL</button>
            <script type="text/javascript">
                function dodelete(){
                    $('#form-delete').submit();
                }
            </script>
            <form id="form-delete" method="post" action="{{url('auth/delete').'/'.Funcs::rq('id',$data['results'])}}">
                {!! Form::hidden('id', Funcs::rq('id',$data['results'])) !!}
                @csrf
            </form>
        @endslot
    @endcomponent
@endif

@endsection
