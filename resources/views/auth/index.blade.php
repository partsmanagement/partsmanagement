<?php
    // ページタイトル
    $title = 'ユーザー一覧';

    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';

    $subnavs = [
        ['text'=>'新規登録','href'=>url('auth/edit')],
    ];

    // モーダルウィンドウ
    $modalid_clear = 'catalog_alert_clear';
?>
@inject('charge', 'partsmanagement\Libs\ChargeInfo')
@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                {{ Form::open(['method' => 'get', 'url' => 'auth/search', 'class'=>'form-horizontal form-label-left']) }}
                    <div class="row pb-2 pl-1">
                        <div class="w70 mx-3 align-self-center text-nowrap">ユーザーID</div>
                        <div class="w400 mx-3 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'user_id')
                            @endcomponent
                        </div>
                    </div><!-- .row -->
                    <div class="row pb-2 pl-1">
                        <div class="w70 mx-3 align-self-center text-nowrap">ユーザー名</div>
                        <div class="w400 mx-3 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'user_name')
                            @endcomponent
                        </div>
                    </div><!-- .row -->
<!--                    <div class="row pb-2 pl-1 align-items-start">
                        <div class="w70 mx-3 align-self-center text-nowrap">ユーザー種別</div>
                        <div class="w150 mx-3 align-self-center">
                            @component('layouts.inputs.select')
                                @slot('accesskey', 'user_type')
                                @slot('pulldown', $user_type_list)
                                @slot('options', ['class'=> $errors->has("user_type") ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                        </div>
                    </div> .row -->
                    <div class="row pb-2 pl-1 align-reserves-start">
                        <div class="col">
                            <div class="row justify-content-end">
                                <div class="mx-3">
                                    <button type="submit" class="btn btn-primary">検索</button>
                                </div>
                                <div class="mx-3">
                                    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#{{$modalid_clear}}">クリア</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- .row -->
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-12">
        <div class="card">
            <!--div class="card-header"></div-->
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <!-- ページャ -->
            			<nav  class="nav_style_height">
            			    {{ $results->links() }}
            			</nav>
            			<!-- /ページャ -->
                    </div>
                </div>
                <div class="number_style">
                    @if ( $results->Total() )
                        該当件数 <span>{{ $results->Total() }}</span> 件のうち <span>{{ $results->firstItem() }} ～ {{ $results->lastItem() }}</span> 件目を表示
                    @else
                        該当件数 <span>{{ $results->Total() }}</span> 件
                    @endif
                </div>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" width="20%">ユーザーID</th>
                                    <th scope="col" width="20%">ユーザー名</th>
                                    <!--<th scope="col" width="10%">ユーザー種別</th>-->
                                    <th scope="col" width="20%">最終更新者</th>
                                    <th scope="col" width="15%">最終更新日</th>
                                    <th scope="col" width="10%">編集</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($results as $row)
                                    <tr>
                                        <td class="text-left">{{ $row->user_id }}</td>
                                        <td class="text-left">{{ $row->user_name }}</td>
                                        <!--<td class="text-left">{{ Config::get('const.user_type_list')[$row->user_type] }}</td>-->
                                        <td class="text-left">{{ $row->upd_user_name }}</td>
                                        <td class="text-center">{{ date_format( date_create($row->updated_at), 'Y/m/d H:i') }}</td>
                                        @if ( $user_id == $row->user_id || $charge->user_type == Config::get('const.user_type_list_cd')['admin'] )
                                            <td class="text-center"><a class="btn btn-primary" href="{{url('auth/edit')}}/{{$row->id}}">編集</a></td>
                                        @else
                                            <td class="text-center"></td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-12">
                        <!-- ページャ -->
            			<nav  class="nav_style_height">
            			    {{ $results->links() }}
            			</nav>
            			<!-- /ページャ -->
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
    </div>
</div>

@endsection
{{-- 追加文末部分 --}}
@section('postdocument')

    @component('layouts.modal')
        @slot('modalid', $modalid_clear)
        @slot('modaltitle', 'クリア')
        @slot('modalcontent')
            クリアします
        @endslot
        @slot('modalfooter')
            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doclear();return false;">OK</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCEL</button>
            <script type="text/javascript">
                function doclear(){
                    window.location.href='{{ url('auth') }}';
                }
            </script>
        @endslot
    @endcomponent

@endsection
