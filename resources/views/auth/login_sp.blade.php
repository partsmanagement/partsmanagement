<?php
    // ページタイトル
    $title = 'ログイン';
    // ページタイトル
    $title_en = 'Login';
    // ページ説明
    $description = '';
    // コントローラ
    $subnavs = [
    ];

?>
@extends('layouts.app_sp_type1')

@section('content')
<script type="text/javascript">


</script>

<style>

    body {
      background: linear-gradient(#123B6F, #287AB6, #00AFF0, #123B6F);

    }
    .cp_iptxt {
        position: relative;
        /*width: 80%;*/
    /*	margin: 40px;*/
    }
    .cp_iptxt i {
        position: absolute;
        top: 8px;
        left: 0px;
        padding: 5px 11px;
        transition: 0.3s;
        color: #aaaaaa;
    }

    .cp_iptxt input[type=text] {
        font: 15px/24px sans-serif;
        box-sizing: border-box;
        width: 100%;
        margin: 8px 0;
        padding: 0.3em;
        transition: 0.3s;
        border: 1px solid #1b2538;
        border-radius: 4px;
        outline: none;
    }
    .cp_iptxt input[type=text]:focus {
        border-color: #1e90ff;
    }
    .cp_iptxt input[type=text] {
        padding-left: 40px;
    }
    .cp_iptxt input[type=text]:focus + i {
        color: #1e90ff;
    }

    .cp_iptxt input[type=password] {
        font: 15px/24px sans-serif;
        box-sizing: border-box;
        width: 100%;
        margin: 8px 0;
        padding: 0.3em;
        transition: 0.3s;
        border: 1px solid #1b2538;
        border-radius: 4px;
        outline: none;
    }
    .cp_iptxt input[type=password]:focus {
        border-color: #1e90ff;
    }
    .cp_iptxt input[type=password] {
        padding-left: 40px;
    }
    .cp_iptxt input[type=password]:focus + i {
        color: #1e90ff;
    }
</style>

<div class="container">
    <div class="row" style="margin: 50px 0px 0px 0px;">
        <div class="col-md-12 text-center">
<!--            <h1 class="text-white">RIM</h1>-->
            <p class="h3 text-white">株式会社 かたずけ屋<br>WEB管理システム</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="cp_iptxt" style="margin: 50px 40px 0px 40px;">
                    <input id="user_id" type="text" class="form-control  form-control-lg{{ $errors->has('user_id') ? ' is-invalid' : '' }}" name="user_id" value="{{ old('user_id') }}" placeholder="Id" required autofocus>
                    <i class="fa fa-user fa-lg" aria-hidden="true"></i>
                    @if ($errors->has('user_id'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('user_id') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="cp_iptxt" style="margin: 0px 40px 0px 40px;">
                    <input id="password" type="password" class="form-control  form-control-lg{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>
                    <i class="fas fa-lock fa-lg" aria-hidden="true"></i>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="cp_iptxt" style="margin: 30px 40px 0px 40px;">
                    <button type="submit" class="btn btn-secondary btn-lg btn-block">
                        {{ __('ログイン') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
