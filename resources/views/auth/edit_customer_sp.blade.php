<?php
    // ページタイトル
    $title = 'ユーザー設定';
    // ページタイトル
    $title_en = 'Property';
    // ページ説明
    $description = '指定した日付で配送ルートにある配達先への出荷商品を担当者別に表示します。';

    // コントローラ
    $subnavs = [
    ];

    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'OFF';

    // ダイアログ
    $modalid_delete = 'dialogdelete';   //削除
?>
@extends('layouts.app_sp_type1')

@section('content')
<script type="text/javascript">
</script>

<style>

    body {
      /*background: linear-gradient(#123B6F, #287AB6, #00AFF0, #123B6F);*/

    }
    .cp_iptxt {
        position: relative;
        /*width: 80%;*/
    /*	margin: 40px;*/
    }
    .cp_iptxt i {
        position: absolute;
        top: 8px;
        left: 0px;
        padding: 5px 11px;
        transition: 0.3s;
        color: #aaaaaa;
    }

    .cp_iptxt input[type=text] {
        font: 15px/24px sans-serif;
        box-sizing: border-box;
        width: 100%;
        margin: 8px 0;
        padding: 0.3em;
        transition: 0.3s;
        border: 1px solid #1b2538;
        border-radius: 4px;
        outline: none;
    }
    .cp_iptxt input[type=text]:focus {
        border-color: #1e90ff;
    }
    .cp_iptxt input[type=text] {
        padding-left: 40px;
    }
    .cp_iptxt input[type=text]:focus + i {
        color: #1e90ff;
    }

    .cp_iptxt input[type=label] {
        padding-left: 40px;
    }

    .cp_iptxt input[type=password] {
        font: 15px/24px sans-serif;
        box-sizing: border-box;
        width: 100%;
        margin: 8px 0;
        padding: 0.3em;
        transition: 0.3s;
        border: 1px solid #1b2538;
        border-radius: 4px;
        outline: none;
    }
    .cp_iptxt input[type=password]:focus {
        border-color: #1e90ff;
    }
    .cp_iptxt input[type=password] {
        padding-left: 40px;
    }
    .cp_iptxt input[type=password]:focus + i {
        color: #1e90ff;
    }
</style>

<h5 id="return" class="my-3 text-center text-nowrap"><p class="far fa-edit mr-1" style="color:#1e90ff;"></p>{{ $title }}</h5>

<h5 id="return" class="my-0 text-center text-nowrap"><p class="fas fa-user-circle  fa-7x mr-1" style="color:#1e90ff;"></p></h5>

{{ Form::open(['id'=>'input-form', 'method' => 'post', 'url' => 'mypage/save', 'class'=>'form-horizontal form-label-left']) }}
    <div class="row">
        <div class="col-12 text-center">
            <div class="">
                <h3 class="text-center">
                    {{ Funcs::rq('customer_detail_name', $data['results']) }}
                </h3>
            </div><!--/.row-->
            <div class="cp_iptxt" style="margin: 30px 40px 0px 40px;">
                <input readonly type="label" class="form-control" value="{{ Funcs::rq('user_id', $data['results']) }}">
                <i class="far fa-user fa-lg" aria-hidden="true"></i>
            </div>

            <div class="cp_iptxt" style="margin: 0px 40px 0px 40px;">
                <input id="user_name" type="text" value="{{ Funcs::rq('user_name', $data['results']) }}" class="form-control  form-control-lg{{ $errors->has('user_name') ? ' is-invalid' : '' }}" placeholder="ユーザー名" name="user_name">
                <i class="fas fa-user-edit fa-lg" aria-hidden="true"></i>
                @if ($errors->has('user_name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('user_name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="cp_iptxt" style="margin: 0px 40px 0px 40px;">
                <input id="password" type="password" class="form-control  form-control-lg{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="新しいパスワード" name="password">
                <i class="fas fa-lock fa-lg" aria-hidden="true"></i>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="cp_iptxt" style="margin: 0px 40px 0px 40px;">
                <input id="password_confirmation" type="password" class="form-control  form-control-lg{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" placeholder="新しいパスワード(確認)" name="password_confirmation">
                <i class="fas fa-lock fa-lg" aria-hidden="true"></i>
                @if ($errors->has('password_confirmation'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>

            <div class="cp_iptxt" style="margin: 30px 40px 0px 40px;">
                <!-- 更新 -->
                @if( !empty( Funcs::rq('id', $data['results']) ) )
                    @if ( 'administrator' == $user_id || 'root' == $user_id )
                        <button type="button" class="btn btn-danger btn-lg btn-block" data-toggle="modal" data-target="#{{$modalid_delete}}">削除</button>
                    @endif
                    {{ Form::submit('更新', ['id'=>'btn_save', 'class' => 'btn btn-primary btn-lg btn-block', 'data-toggle'=>'tooltip', 'title'=>'入力データを保存する']) }}
                @else
                <!-- 新規 -->
                    {{ Form::submit('登録', ['id'=>'btn_save', 'class' => 'btn btn-primary btn-lg btn-block', 'data-toggle'=>'tooltip', 'title'=>'入力データを保存する']) }}
                @endif
                <a href="{{url('/order')}}" class="btn btn-success btn-lg btn-block" >戻る</a>
            </div>
            <!-- 更新の時post -->
            {{ Form::hidden('id',                   Funcs::rq('id',                     $data['results'])) }}
            {{ Form::hidden('user_id',              Funcs::rq('user_id',                $data['results'])) }}
            {{ Form::hidden('user_type',            Funcs::rq('user_type',              $data['results'])) }}
            {{ Form::hidden('customer_detail_id',   Funcs::rq('customer_detail_id',     $data['results'])) }}
            {{ Form::hidden('customer_detail_name', Funcs::rq('customer_detail_name',   $data['results'])) }}
        </div>
    </div>
    @csrf
{{ Form::close() }}
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')

@if( !empty( Funcs::rq('id', $data['results']) ) )
    <!-- 削除 -->
    @component('layouts.modal')
        @slot('modalid', $modalid_delete)
        @slot('modaltitle', '削除')
        @slot('modalcontent')
            削除します、よろしいですか？
        @endslot
        @slot('modalfooter')
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="dodelete();return false;">OK</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCEL</button>
            <script type="text/javascript">
                function dodelete(){
                    $('#form-delete').submit();
                }
            </script>
            <form id="form-delete" method="post" action="{{url('auth/delete').'/'.Funcs::rq('id',$data['results'])}}">
                {!! Form::hidden('id', Funcs::rq('id',$data['results'])) !!}
                @csrf
            </form>
        @endslot
    @endcomponent
@endif

@endsection
