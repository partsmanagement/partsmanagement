<?php
    // ページタイトル
    $title = 'MENU';
    // ページタイトル
    $title_en = 'Route Map';
    // ページ説明
    $description = '指定した日付で配送ルートにある配達先への出荷商品を担当者別に表示します。';

    // コントローラ
    $subnavs = [
    ];

    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'OFF';
?>
@extends('layouts.app_sp_type1')

@section('content')
<script type="text/javascript">


</script>

<style>
img {
  width: 100%;
}

</style>

<!--        <span class="" id="real-datebox" style=""></span>
        <span class="" id="real-timebox" style=""></span>-->

    <h5 id="return" class="my-3 text-center text-nowrap"><p class="far fa-list-alt" style="color:#1e90ff;"></p> Menu</h5>

    <div class="row border" style="color:#666;background-color:#f5f5f5;">
        <a href="{{url('/vehicle')}}" class="col border-bottom text-nowrap">
            <div class="py-4 col-6" style="" >
                 <p class="fas fa-truck fa-2x" style="color:#1e90ff;"></p><span class="h4 font-weight-bold" style="color:#333;">  車両一覧</span>
                 <p class=" font-weight-bold" style="color:#1e90ff;">取り扱い車両を一覧で表示します。</p>
            </div>
        </a>
        <a href="{{url('/order/edit')}}" class="col  border-bottom text-nowrap">
            <div class="py-4 col-6" style="" >
                 <p class="far fa-list-alt fa-2x" style="color:#1e90ff;"></p><span class="h4 font-weight-bold" style="color:#333;">&nbsp&nbsp車両登録</span>
                 <p class=" font-weight-bold" style="color:#1e90ff;">新規に車両詳細情報(パーツ、画像、etc)を<br>登録します。</p>
            </div>
        </a>
    </div>

@endsection

{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
    <!-- カナ入力（同期） -->
    <!--<script src="{{ asset('asset/jquery/jquery.autoKana.js') }}"></script>-->
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')

<!-- イベント -->
<script type="text/javascript">
    var realTimeDatetimeDisplay = function() {
        var dateObj        = new Date(),
            dateYear       = dateObj.getFullYear(),
            dateMonth      = dateObj.getMonth() + 1,
            dateDay        = dateObj.getDate(),
            dateWeek       = dateObj.getDay(),
            timeHour       = dateObj.getHours(),
            timeMinutes    = dateObj.getMinutes(),
            timeSeconds    = dateObj.getSeconds(),
            weekNames      = ['日', '月', '火', '水', '木', '金', '土'],
            displayElementDate = document.getElementById('real-datebox'),
            displayElementTime = document.getElementById('real-timebox'),
            strDate            = '';
            strTime            = '';

        // 一桁の場合は0を追加
        if (timeHour < 10) {
            timeHour = '0' + timeHour;
        }
        if (timeMinutes < 10) {
            timeMinutes = '0' + timeMinutes;
        }
        if (timeSeconds < 10) {
            timeSeconds = '0' + timeSeconds;
        }

        // 文字列の結合
        strDate = dateYear + '年' + dateMonth + '月' + dateDay + '日　' + weekNames[dateWeek] + '曜日<br>';
        strTime = timeHour + '時' + timeMinutes + '分' + timeSeconds + '秒';

        // 出力
        if (displayElementDate) {
            displayElementDate.innerHTML = strDate;
        }
        if (displayElementTime) {
            displayElementTime.innerHTML = strTime;
        }

        // 繰り返し実行
        setTimeout(realTimeDatetimeDisplay, 1000);
    };
    // 実行
    realTimeDatetimeDisplay();
</script>

<script type="text/javascript">
    $('#btn_mode').on('click', function(){
        $('#input-form').submit();
    });
</script>

@endsection