{{-- フラッシュメッセージの表示 --}}
@if (Session::has('flash_success'))
    <div class="alert alert-success" role="alert">{{ Session::get('flash_success') }}</div>
    {{ Session::forget('flash_success') }}
@endif
@if (Session::has('flash_info'))
    <div class="alert alert-info" role="alert">{{ Session::get('flash_info') }}</div>
    {{ Session::forget('flash_info') }}
@endif
@if (Session::has('flash_warning'))
    <div class="alert alert-warning" role="alert">{{ Session::get('flash_warning') }}</div>
    {{ Session::forget('flash_warning') }}
@endif
@if (Session::has('flash_danger'))
    <div class="alert alert-danger" role="alert">{{ Session::get('flash_danger') }}</div>
    {{ Session::forget('flash_danger') }}
@endif

{{-- エラーメッセージ：ページ上部 表示:ON 非表示:OFF --}}
@if ($err_message_flg == 'ON')
    {{-- フォームエラー --}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    @if(!empty(trim($error)))
                    <li>{{ $error }}</li>
                    @endif
                @endforeach
            </ul>
        </div>
    @endif
@endif
