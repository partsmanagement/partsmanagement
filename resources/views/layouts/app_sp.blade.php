<?php
    // 各テンプレート変数
    if(!isset($title)) $title = false;
    if(!isset($subnavs)) $subnavs = false;
    if(!isset($isprint)) $isprint = false;
    // if(!isset($pconly)) $pconly = false;
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
{{-- 担当者マスタへのアクセス --}}
@inject('charge', 'App\Libs\ChargeInfo')
<head>
    <meta charset="utf-8">
    <!-- meta http-equiv="X-UA-Compatible" content="IE=edge" -->
    <!-- meta name="viewport" content="width=device-width, initial-scale=1" -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    {{-- ▼▽▼ LARAVELデフォルト外部ファイル ▽▼▽ --}}
    <!-- Scripts -->{{--Bootstrap含む--}}
    <script src="{{ asset('js/app.js') }}" ></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">


    <!-- Styles -->
    <link href="{{ asset('css/app_sp.css') }}" rel="stylesheet">

    {{-- ▲△▲ LARAVELデフォルト外部ファイル △▲△ --}}

    {{-- ▼▽▼ 外部ファイル追加 ▽▼▽ --}}
    <link href="{{ asset('css/offcanvas.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/fontawesome/css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('css/positions.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sizing.css') }}" rel="stylesheet">
    <link href="{{ asset('css/printmedia_sp.css') }}" rel="stylesheet">
    <link href="{{ asset('sp/css/base.css') }}" rel="stylesheet">

    <!-- query-ui-1.12.1 -->
    <link href="{{ asset('asset/jquery/jquery-ui-1.12.1/jquery-ui.min.css') }}" rel="stylesheet">
    <script src="{{ asset('asset/jquery/jquery-ui-1.12.1/jquery-ui.min.js') }}"></script>

    <!-- datetimepicker -->
    <link href="{{ asset('asset/datetimepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
    <script src="{{ asset('asset/moment-with-locales.min.js') }}"></script>
    <script src="{{ asset('asset/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <!-- ajax zip -->
    <script src="{{ asset('js/ajaxzip3/ajaxzip3.js', config('web.ssl')) }}" type="text/javascript" ></script>

    <!-- functions -->
    <script src="{{ asset('js/functions.js') }}"></script>

    <!-- tablednd -->
<!--    <script src="{{ asset('asset/jquery/tablednd/jquery.tablednd.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('asset/jquery/tablednd/jquery.tablednd.min.js') }}" type="text/javascript" ></script>-->

    <!-- sweetalert -->
    <link href="{{ asset('asset/sweetalert/dist/sweetalert.css') }}" rel="stylesheet">
    <script src="{{ asset('asset/sweetalert/dist/sweetalert.min.js') }}" type="text/javascript" ></script>
    {{-- ▲△▲ 外部ファイル追加 △▲△ --}}

    {{-- AJAX通信のCSRFトークンをヘッダへ追加 --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- AJAX通信のCSRFトークンを登録 --}}
    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    </script>

    {{-- 各VIEWから追加されるドキュメント --}}
    @yield('addheader')
</head>

<?php
// 現在のルートから<body>にIDを割振り、ページ個別にCSSを設定できるようにする
$routeid = '';
$route = Route::current();
$name = Route::currentRouteName();
$action = Route::currentRouteAction();
if (!empty($name)) {
    $routeid = $name;
} elseif (!empty($route)) {
    $regexp = '/\/.+$/m';
    $routeid = preg_replace($regexp, '', $route->uri);
}
?>
<body id="{{ $routeid }}">
    <div id="app">
        @auth
        <nav id="holdoffcanvas" class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">

            <!-- ハンバーガー -->
            <!--得意先によるログインの場合、メニューを表示しない-->
            @if ( Auth::user()->user_type != Config::get('const.user_type_list_cd')['customer'] )
                <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
                    <!--span class="navbar-toggler-icon"></span-->
                    <!--div class="navbar-toggler-icon icon-animation type-1">
                        <span class="top"></span>
                        <span class="middle"></span>
                        <span class="bottom"></span>
                    </div-->
                    <div class="navbar-toggler-icon menu-trigger">
                        <span class="top"></span>
                        <span class="middle"></span>
                        <span class="bottom"></span>
                    </div>
                </button>
            @endif

            <!--------------------------------------------
            * 画面タイトルの表示
            * 配達経路画面以外表示
            -------------------------------------------->
            <div class="center-block" >
                @if ($routeid != 'routeshow' && $routeid != 'routedetail')
                    <div id="pagetitle" class="row mx-auto" >
                        <div class="heading " >{{$title}}</div>
                    </div>
                @endif
            </div>

            <!-- メインメニュー -->
            <div class="navbar-collapse offcanvas-collapse" >
                <ul class="navbar-nav mr-auto w300">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('top') }}">Top</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="menu_yardmenu_1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">作業管理</a>
                        <div class="dropdown-menu" aria-labelledby="menu_yardmenu">
                            <a class="dropdown-item" href="{{asset('shipment')}}">出荷一覧</a>
                            <a class="dropdown-item" href="{{asset('delivery')}}">納品一覧</a>
                            <a class="dropdown-item" href="{{asset('order')}}">注文一覧</a>
                            <a class="dropdown-item" href="{{asset('ship')}}">ルート一覧</a>
                        </div>
                    </li>
                </ul>
            </div>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->user_name }}{{-- $charge->user_id --}} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <!-- 得意先でログインした場合表示 -->
                        @if ( Auth::user()->user_type == Config::get('const.user_type_list_cd')['customer'] )
                            <a class="dropdown-item" href="{{asset('mypage/edit/'.Auth::user()->id)}}">設定</a>
                        @endif

                        <!-- ログアウト表示 -->
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>

        </nav>

        <!-- サブメニュー -->
        <?php if(!empty($subnavs)&&is_array($subnavs)){ ?>
            <div class="nav-scroller bg-white box-shadow d-print-none">
                <nav class="nav nav-underline">
                    <?php
                    foreach ($subnavs as $subnav){
                        if(array_key_exists('text',$subnav)&&array_key_exists('href',$subnav)){
                            $text = $subnav['text'];
                            $href = $subnav['href'];
                            $options = '';
                            if(array_key_exists('id',$subnav)){
                                $options.=sprintf(' id="%s"',$subnav['id']);
                            }
                            if(array_key_exists('onclick',$subnav)){
                                $options.=sprintf(' onclick="%s"',$subnav['onclick']);
                            }
                            ?><a class="nav-link p-1" href="{{$href}}" {!!$options!!}>
                                <span class="badge badge-primary p-2 m-1">{{$text}}</span>
                            </a><?php
                        }
                    }
                    ?>
                </nav>
            </div>
        <?php }//if(!empty($subnavs)) ?>
        @endauth

        <main class="py-2">
            <div class="container-fluid m-0 px-3 pb-2 h-100">
                <!--------------------------------------------
                * 画面タイトルの表示
                * 配達経路画面以外表示
                -------------------------------------------->
<!--                @if ($routeid != 'routeshow' && $routeid != 'routedetail')
                    <div id="pagetitle" class="row">
                        <div class="heading">{{$title}}</div>
                    </div>
                @endif-->

                @if ($routeid == 'login')
                    <nav id="holdoffcanvas" class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
                        <ul style="font-size:16pt; color:#fff" class="navbar-nav mr-auto w600">
                            <li class="nav-item dropdown">
                                {{Config::get('const.system_name')}}
                            <li class="nav-item dropdown">
                        <ul class="navbar-nav mr-auto w300">
                    </nav>
                @endif
                @if ($routeid != 'login')
                    <!--メッセージの表示-->
                    @include('layouts.messages')
                @endif

                @yield('content')
                @if ($routeid != 'login')
                    <!-- footer -->
<!--                    <footer class="footer" style="">
                            <div class="pull-right">
                                <div class="footer_company_name">CopyRight &copy; propeller. All rights reserved.</div>
                            </div>
                    </footer>-->
                    <!-- //footer -->
                @endif
            </div>
        </main>

        @if($isprint)
        <!-- 印刷ボタン -->
        <div class="printcontrol d-print-none">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="card">
                        <!--div class="card-header"></div-->
                        <div class="card-body p-1">
                            <button type="button" class="btn btn-success w-100 h-100" onclick="execPrint();">印刷</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>

    {{-- メニューを閉じる --}}
    <script type="text/javascript">
        $(document).ready(function(){
            //テキストボックスをフォーカスが当たった時に実行
            $("input[type='text']").focus(function(){
                //全選択にする
                $(this).select();
            });
            $("input[type='number']").focus(function(){
                //全選択にする
                $(this).select();
            });
        });
    </script>

    {{-- ▼▽▼ PHP連動が必要なスクリプト ▽▼▽ --}}
    <script type="text/javascript">

    </script>
    {{-- ▲△▲ PHP連動が必要なスクリプト △▲△ --}}

    {{-- ▼▽▼ 外部ファイル追加 ▽▼▽ --}}
    <script src="{{ asset('js/offcanvas.js') }}" defer></script>
    {{-- ▲△▲ 外部ファイル追加 △▲△ --}}

    {{-- 各VIEWから追加されるドキュメント --}}
    @yield('postdocument')

    {{-- プリント支持がある場合は自動印刷開始 --}}
    <script type="text/javascript">
        function urlGetParams() {
            // URLから「?」以降の文字列を取り出す
            var query = location.search.substr(1);
            // 「&」で分割して、順に処理する
            var params = {};
            query.split("&").forEach(function (item) {
                // 「=」でパラメーター名と値に分割して、paramsに追加
                var s = item.split("=");
                var k = decodeURIComponent(s[0]);
                var v = decodeURIComponent(s[1]);
                (k in params) ? params[k].push(v) : params[k] = [v];
            });
            return params;
        }

        function execPrint() {
            $("#print_cp").show();
            print();
            $("#print_cp").hide();
        }
    </script>
    <?php
    if (array_key_exists('printing', $_REQUEST)){
        ?>
        <script type="text/javascript">
            print();
            // var params = urlGetParams();
            // if(!params.printing){
            //     window.location.href=document.referrer;
            // }else{
            //     window.location.href='/';
            // }
            var ref = document.referrer;
            if( ref.length == 0 ) {
                window.location.href='/';
            }else{
                window.location.href=document.referrer;
            }
        </script>
        <?php
    }
    ?>
</body>
</html>
