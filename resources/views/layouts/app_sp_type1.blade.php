<?php
    // 各テンプレート変数
    if(!isset($title))   $title = false;
    if(!isset($subnavs)) $subnavs = false;
    if(!isset($isprint)) $isprint = false;
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
{{-- 担当者マスタへのアクセス --}}
@inject('charge', 'partsmanagement\Libs\ChargeInfo')
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    {{-- ▼▽▼ LARAVELデフォルト外部ファイル ▽▼▽ --}}
    <!-- Scripts -->
    {{--Bootstrap含む--}}
    <script src="{{ asset('js/app.js') }}" ></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app_sp.css') }}" rel="stylesheet">
    {{-- ▲△▲ LARAVELデフォルト外部ファイル △▲△ --}}

    {{-- ▼▽▼ 外部ファイル追加 ▽▼▽ --}}
    <link href="{{ asset('asset/fontawesome/css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('css/printmedia_sp.css') }}" rel="stylesheet">
    @if(!$isprint)
    <link href="{{ asset('css/sizing.css') }}" rel="stylesheet">
    <link href="{{ asset('sp/css/base.css') }}" rel="stylesheet">
    @endif

    <!-- query-ui-1.12.1 -->
    <link href="{{ asset('asset/jquery/jquery-ui-1.12.1/jquery-ui.min.css') }}" rel="stylesheet">
    <script src="{{ asset('asset/jquery/jquery-ui-1.12.1/jquery-ui.min.js') }}"></script>

    <!-- ajax zip -->
    <script src="{{ asset('js/ajaxzip3/ajaxzip3.js', config('web.ssl')) }}" type="text/javascript" ></script>

    @if(!$isprint)
    <!-- Bootstrap Core CSS startbootstrap -->
    <link href="{{ asset('sp/startbootstrap/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom Fonts ヘッダー startbootstrap -->
    <link href="{{ asset('sp/startbootstrap/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="{{ asset('sp/startbootstrap/vendor/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">

    <!-- Custom CSS ヘッダー startbootstrap -->
    <link href="{{ asset('sp/startbootstrap/css/stylish-portfolio.min.css') }}" rel="stylesheet">
    @endif
    {{-- ▲△▲ 外部ファイル追加 △▲△ --}}

    {{-- AJAX通信のCSRFトークンをヘッダへ追加 --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- AJAX通信のCSRFトークンを登録 --}}
    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    </script>

    {{-- 各VIEWから追加されるドキュメント --}}
    @yield('addheader')
</head>

<?php
// 現在のルートから<body>にIDを割振り、ページ個別にCSSを設定できるようにする
$routeid = '';
$route = Route::current();
$name = Route::currentRouteName();
$action = Route::currentRouteAction();
if (!empty($name)) {
    $routeid = $name;
} elseif (!empty($route)) {
    $regexp = '/\/.+$/m';
    $routeid = preg_replace($regexp, '', $route->uri);
}
?>
<body id="{{ $routeid }}">
    <div id="app">
        @auth
        <!-- Navigation -->
        <a class="menu-toggle rounded" href="#">
          <i class="fas fa-bars"></i>
        </a>
        <nav id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <p class="far fa-user fa-lg" style="color:#fff;"></p>
                    @if ( Auth::user()->user_type == Config::get('const.user_type_list_cd')['customer'] )
                        <a class="js-scroll-trigger" href="{{asset('mypage/edit/'.Auth::user()->id)}}">{{ Auth::user()->user_name }}さん</a>
                    @else
                        <a class="js-scroll-trigger">{{ Auth::user()->user_name }}さん</a>
                    @endif
                </li>

                <li class="sidebar-nav-item">
                    <a class="js-scroll-trigger" href="{{url('top')}}"><p class="far fa-list-alt" style="color:#fff;"></p>&nbsp&nbspMenu</a>
                </li>
                <li class="sidebar-nav-item">
                    <a class="js-scroll-trigger" href="{{asset('vehicle')}}"><p class="fas fa-truck" style="color:#fff;"></p>&nbsp車両一覧</a>
                </li>
                <li class="sidebar-nav-item">
                    <a class="js-scroll-trigger" href="{{asset('vehicle/edit')}}"><p class="far fa-list-alt" style="color:#fff;"></p>&nbsp&nbsp車両登録</a>
                </li>

                <li class="sidebar-nav-item">
                    <!-- ログアウト表示 -->
                    <a class="js-scroll-trigger" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        <p class="fas fa-sign-out-alt" style="color:#fff;"></p>&nbsp&nbsp{{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
        @endauth


        @if ($routeid == 'login')
        <main class="">
            <div class="">
        @else
        <main class="py-2">
            <div class="container-fluid m-0 px-3 pb-2 h-100">
        @endif
                <!--------------------------------------------
                * 画面タイトルの表示
                * 配達経路画面以外表示
                -------------------------------------------->
<!--                @if ($routeid != 'routeshow' && $routeid != 'routedetail')
                    <div id="pagetitle" class="row">
                        <div class="heading">{{$title}}</div>
                    </div>
                @endif-->

<!--                @if ($routeid == 'login')
                    <nav id="holdoffcanvas" class="">
                        <ul style="font-size:16pt; color:#fff" class="navbar-nav mr-auto w600">
                            <li class="nav-item dropdown">
                                {{Config::get('const.system_name')}}
                            <li class="nav-item dropdown">
                        <ul class="navbar-nav mr-auto w300">
                    </nav>
                @endif-->
                @if ($routeid != 'login')
                    <!--メッセージの表示-->
                    @include('layouts.messages')
                @endif

                @yield('content')
                @if ($routeid != 'login')
                    <!-- footer -->
<!--                    <footer class="footer" style="">
                            <div class="pull-right">
                                <div class="footer_company_name">CopyRight &copy; propeller. All rights reserved.</div>
                            </div>
                    </footer>-->
                    <!-- //footer -->
                @endif
            </div>
        </main>

        @if($isprint)
        <!-- 印刷ボタン -->
        <div class="printcontrol d-print-none">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="card">
                        <!--div class="card-header"></div-->
                        <div class="card-body p-1">
                            <button type="button" class="btn btn-success w-100 h-100" onclick="execPrint();">印刷</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>

    <!-- Footer -->
    @if ($routeid != 'login')
    <footer class="footer text-center">
        <div class="container">
            <ul class="list-inline mb-5">
                <li class="list-inline-item">
                    <a class="social-link rounded-circle text-white mr-3" href="https://www.facebook.com/toufunohiroshiya">
                        <i class="icon-social-facebook"></i>
                    </a>
                </li>
<!--                <li class="list-inline-item">
                    <a class="social-link rounded-circle text-white mr-3" href="#">
                        <i class="icon-social-twitter"></i>
                    </a>
                </li>-->
                <li class="list-inline-item">
                    <a class="social-link rounded-circle text-white" href="#">
                        <i class="fas fa-blog"></i>
                    </a>
                </li>
            </ul>
            <p class="text-muted small mb-0">Copyright &copy; 2019 かたずけ屋, ALL Rights Reserved.</p>
        </div>
    </footer>
    @endif

    {{-- ▼▽▼ PHP連動が必要なスクリプト ▽▼▽ --}}
    <script type="text/javascript">

    </script>
    {{-- ▲△▲ PHP連動が必要なスクリプト △▲△ --}}

    {{-- ▼▽▼ 外部ファイル追加 ▽▼▽ --}}
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#return">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('sp/startbootstrap/vendor/jquery/jquery.min.js') }}"></script>
    <!--<script src="{{ asset('sp/startbootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>-->

    <!-- Plugin JavaScript -->
    <script src="{{ asset('sp/startbootstrap/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{ asset('sp/startbootstrap/js/stylish-portfolio.min.js') }}"></script>
    {{-- ▲△▲ 外部ファイル追加 △▲△ --}}

    {{-- 各VIEWから追加されるドキュメント --}}
    @yield('postdocument')

    <script type="text/javascript">

        {{-- プリント支持がある場合は自動印刷開始 --}}
        @if($isprint)
            var ishide = {{ $isprint }};
        @else
            var ishide = false;
        @endif
        if(ishide) {
            $('#sidebar-wrapper').hide();
            $('.menu-toggle').hide();
            $('.scroll-to-top').remove();
            $('.js-scroll-trigger').remove();
            $('footer').hide();
        }

        function urlGetParams() {
            // URLから「?」以降の文字列を取り出す
            var query = location.search.substr(1);
            // 「&」で分割して、順に処理する
            var params = {};
            query.split("&").forEach(function (item) {
                // 「=」でパラメーター名と値に分割して、paramsに追加
                var s = item.split("=");
                var k = decodeURIComponent(s[0]);
                var v = decodeURIComponent(s[1]);
                (k in params) ? params[k].push(v) : params[k] = [v];
            });
            return params;
        }

        function execPrint() {
            $("#print_cp").show();
            print();
            $("#print_cp").hide();
        }
    </script>
    <?php
    if (array_key_exists('printing', $_REQUEST)){
        ?>
        <script type="text/javascript">
            print();
            // var params = urlGetParams();
            // if(!params.printing){
            //     window.location.href=document.referrer;
            // }else{
            //     window.location.href='/';
            // }
            var ref = document.referrer;
            if( ref.length == 0 ) {
                window.location.href='/';
            }else{
                window.location.href=document.referrer;
            }
        </script>
        <?php
    }
    ?>
</body>
</html>
