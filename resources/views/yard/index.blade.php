<?php
    // ページタイトル
    $title = 'ヤード一覧';
    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';
    // コントローラ
    $subnavs = [
        ['text'=>'新規登録','href'=>url('yard/edit')],
    ];

    // モーダルウィンドウ
    $modalid_clear = 'catalog_alert_clear';
?>
@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form id="form-search" method="get" action="{{ url('yard/search') }}" class="form-horizontal form-label-left">
                    <div class="row pb-2 pl-1">
                        <div class="w50 mx-3 align-self-center text-nowrap">ヤード名</div>
                        <div class="w300 mx-3 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'yard_name')
                            @endcomponent
                        </div>
                    </div><!-- .row -->
                    <div class="col">
                        <div class="row justify-content-end">
                            <div class="mx-3">
                                <button type="button" id="btn_exec" class="btn btn-primary">検索</button>
                            </div>
                            <div class="mx-3">
                                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#{{$modalid_clear}}">クリア</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-12">
        <div class="card">
            <!--div class="card-header"></div-->
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <!-- ページャ -->
            			<nav  class="nav_style_height">
            			    {{ $results->links() }}
            			</nav>
            			<!-- /ページャ -->
                    </div>
                </div>
                <br>
                <div class="number_style">
                    @if ( $results->Total() )
                        該当件数 <span>{{ $results->Total() }}</span> 件のうち <span>{{ $results->firstItem() }} ～ {{ $results->lastItem() }}</span> 件目を表示
                    @else
                        該当件数 <span>{{ $results->Total() }}</span> 件
                    @endif
                </div>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="thead-light">
                                <tr>
                                    <!--<th scope="col" width="3%">ID</th>-->
                                    <th scope="col" width="10%">ID</th>
                                    <th scope="col" width="80%">ヤード名</th>
                                    <th scope="col" width="10%" class="text-center">編集</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($results as $row)
                                <tr>
                                    <!--<td class="text-right">{{ $row->id }}</td>-->
                                    <td class="text-center">{{ $row->id }}</td>
                                    <td class="text-left">{{ $row->yard_name }}</td>
                                    <td class="text-center">
                                        <a class="btn btn-primary" href="{{url('yard/edit')}}/{{$row->id}}">編集</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-12">
                        <!-- ページャ -->
            			<nav  class="nav_style_height">
            			    {{ $results->links() }}
            			</nav>
            			<!-- /ページャ -->
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
    </div>
</div>
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
<!-- イベント -->
<script type="text/javascript">
$(function(){
    //
    $('#btn_exec').on('click', function(){
        $('#form-search').submit();
    });
});
</script>
<!-- /イベント -->
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')

@component('layouts.modal')
    @slot('modalid', $modalid_clear)
    @slot('modaltitle', 'クリア')
    @slot('modalcontent')
        クリアします
    @endslot
    @slot('modalfooter')
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doclear();return false;">はい</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">いいえ</button>
        <script type="text/javascript">
            function doclear(){
                window.location.href='{{ url('yard/search') }}';
            }
        </script>
    @endslot
@endcomponent

@endsection
