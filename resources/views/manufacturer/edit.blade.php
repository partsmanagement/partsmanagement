<?php
    // ページタイトル
    if (!empty( Funcs::rq('id', $data['results']))) {
        // ページタイトル
        $title = 'メーカー 編集';
    } else {
        // ページタイトル
        $title = 'メーカー 登録';
    }
    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';

    // コントローラ
    $subnavs = [
        ['text'=>'一覧','href'=>url('manufacturer')],
    ];

    // モーダルウィンドウ

    // ダイアログ
    $modalid_delete = 'dialogdelete';
?>
@extends('layouts.app')

@section('content')
{{ Form::open(['id'=>'input-form', 'method' => 'post', 'url' => 'manufacturer/save', 'class'=>'form-horizontal form-label-left']) }}
    <div class="row justify-content-center w1024">
        <div class="col-12">
            <div class="card">
                <!-- card-body -->
                <div class="card-body">
                    @if( !empty( Funcs::rq('id', $data['results']) ) )
                        <div class="row pb-2 pl-3">
                            <div class="w130 mr-2 align-self-center text-nowrap text-left">ID</div>
                            <div class="w400 mr-2 align-self-center">
                                {{ Funcs::rq('id', $data['results']) }}
                            </div>
                        </div><!--/.row-->
                    @endif
                    <div class="row pb-2 pl-3">
                        <div class="w130 mr-2 align-self-center text-nowrap text-left">メーカー名<span class="label label-danger ml-1">必須</span></div>
                        <div class="w400 mr-2 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'manufacturer_name')
                                @slot('loading', $data['results'])
                                @slot('options', ['class'=> $errors->has("manufacturer_name") ? 'form-control is-invalid text-left': 'form-control text-left'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('manufacturer_name')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-3">
                        <div class="w130 mr-2 align-self-center text-nowrap text-left">メーカー名カナ</div>
                        <div class="w400 mr-2 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'manufacturer_kana')
                                @slot('loading', $data['results'])
                                @slot('options', ['class'=> $errors->has("manufacturer_kana") ? 'form-control is-invalid text-left': 'form-control text-left'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('manufacturer_kana')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row mt-3">
                        <div class="col">
                            @if( !empty( Funcs::rq('id', $data['results']) ) )
                                <!-- 編集の時のみ動作する -->
                                    <button type="button" id="btn_delete" class="btn btn-danger" data-toggle="modal" data-target="#{{$modalid_delete}}">削除</button>
                            @endif
                            {{ Form::button('登録', ['id'=>'btn_save', 'class' => 'btn btn-primary', 'data-toggle'=>'tooltip', 'title'=>'入力データを保存する']) }}
                            <a href="{{url('/manufacturer')}}" class="btn btn-success" >戻る</a>
                        </div>
                    </div>
                </div>
                <!-- / card-body -->

                {{ Form::hidden('id', Funcs::rq('id',$data['results'])) }}
            </div>
        </div>
    </div>
    @csrf
{{ Form::close() }}
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')
<!-- イベント -->
<script type="text/javascript">
$(function(){
    $('#btn_save').on('click', function(){
        $('#input-form').submit();
    });
});
</script>
<!-- /イベント -->

<!-- 削除 -->
@if( !empty( Funcs::rq('id', $data['results']) ) )
    @component('layouts.modal')
        @slot('modalid', $modalid_delete)
        @slot('modaltitle', '削除')
        @slot('modalcontent')
            削除してよろしいですか？
        @endslot
        @slot('modalfooter')
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="dodelete();return false;">はい</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">いいえ</button>
            <script type="text/javascript">
                function dodelete(){
                    $('#form-delete').submit();
                }
            </script>
            <form id="form-delete" method="post" action="{{url('manufacturer/delete').'/'.Funcs::rq('id',$data['results'])}}">
                {!! Form::hidden('id', Funcs::rq('id',$data['results'])) !!}
                @csrf
            </form>
        @endslot
    @endcomponent
@endif

@endsection
