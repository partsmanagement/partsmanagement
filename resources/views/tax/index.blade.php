<?php
    // ページタイトル
    $title = '税情報一覧';
    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';
    // コントローラ
    $subnavs = [
        ['text'=>'新規登録','href'=>url('tax/edit')],
    ];

    // モーダルウィンドウ
    $modalid_clear = 'catalog_alert_clear';
?>
@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form id="form-search" method="get" action="{{ url('tax/search') }}" class="form-horizontal form-label-left">
                    <div class="row pb-2 pl-1">
                        <div class="w80 mx-3 align-self-center text-nowrap">施工日</div>
                        <div class="w150 mx-3 align-self-center">
                            @component('layouts.inputs.monthpicker')
                                @slot('accesskey', 'const_date')
                                @slot('default', '')
                            @endcomponent
                        </div>
                    </div><!-- .row -->
                    <div class="row pb-2 pl-1">
                        <div class="w80 mx-3 align-self-center text-nowrap">消費税率</div>
                        <div class="w150 mx-3 align-self-center">
                            @component('layouts.inputs.num')
                                @slot('accesskey', 'tax_basic_rate')
                            @endcomponent
                        </div>
                        <div class="w80 mx-3 align-self-center text-nowrap">軽減税率</div>
                        <div class="w150 mx-3 align-self-center">
                            @component('layouts.inputs.num')
                                @slot('accesskey', 'reduction_tax_rate')
                            @endcomponent
                        </div>
                    </div><!-- .row -->
                    <div class="row pb-2 pl-1">
                        <div class="w80 mx-3 align-self-center text-nowrap">税区分</div>
                        <div class="w150 mx-3 align-self-center">
                            @component('layouts.inputs.select')
                                @slot('accesskey', 'tax_flag')
                                @slot('pulldown', Config::get('const.tax_flag_list'))
                                @slot('options', ['class'=> $errors->has('tax_flag') ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                        </div>
                        <div class="w80 mx-3 align-self-center text-nowrap">税計算区分</div>
                        <div class="w150 mx-3 align-self-center">
                            @component('layouts.inputs.select')
                                @slot('accesskey', 'tax_calc_flag')
                                @slot('pulldown', Config::get('const.tax_calc_flag_list'))
                                @slot('options', ['class'=> $errors->has('tax_calc_flag') ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                        </div>
                        <div class="w80 mx-3 align-self-center text-nowrap">税端数計算区分</div>
                        <div class="w150 mx-3 align-self-center">
                            @component('layouts.inputs.select')
                                @slot('accesskey', 'tax_fraction_type')
                                @slot('pulldown', Config::get('const.tax_fraction_type_list'))
                                @slot('options', ['class'=> $errors->has('tax_fraction_type') ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                        </div>
                        <div class="col">
                            <div class="row justify-content-end">
                                <div class="mx-3">
                                    <button type="button" id="btn_exec" class="btn btn-primary">検索</button>
                                </div>
                                <div class="mx-3">
                                    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#{{$modalid_clear}}">クリア</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- .row -->
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-12">
        <div class="card">
            <!--div class="card-header"></div-->
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <!-- ページャ -->
            			<nav  class="nav_style_height">
            			    {{ $results->links() }}
            			</nav>
            			<!-- /ページャ -->
                    </div>
                </div>
                <div class="number_style">
                    @if ( $results->Total() )
                        該当件数 <span>{{ $results->Total() }}</span> 件のうち <span>{{ $results->firstItem() }} ～ {{ $results->lastItem() }}</span> 件目を表示
                    @else
                        該当件数 <span>{{ $results->Total() }}</span> 件
                    @endif
                </div>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" width="3%">ID</th>
                                    <th scope="col" width="">施工日</th>
                                    <th scope="col" width="20%">消費税率</th>
                                    <th scope="col" width="20%">軽減税率</th>
                                    <th scope="col" width="20%">税区分</th>
                                    <th scope="col" width="20%">税計算区分</th>
                                    <th scope="col" width="20%">税端数計算区分</th>
                                    <th scope="col" width="10%">編集</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($results as $row)
                                <tr>
                                    <td class="text-right">{{ $row->id }}</td>
                                    <td class="text-center">{{ date('Y/m/d',strtotime($row->const_date)) }}</td>
                                    <td class="text-right">{{ $row->tax_basic_rate }}%</td>
                                    <td class="text-right">{{ $row->reduction_tax_rate }}%</td>
                                    <td class="text-center">{{ Config::get('const.tax_flag_list')[$row->tax_flag] }}</td>
                                    <td class="text-center">{{ Config::get('const.tax_calc_flag_list')[$row->tax_calc_flag] }}</td>
                                    <td class="text-center">{{ Config::get('const.tax_fraction_type_list')[$row->tax_fraction_type] }}</td>
                                    <td class="text-center">
                                        <a class="btn btn-primary" href="{{url('tax/edit')}}/{{$row->id}}">編集</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-12">
                        <!-- ページャ -->
            			<nav  class="nav_style_height">
            			    {{ $results->links() }}
            			</nav>
            			<!-- /ページャ -->
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
    </div>
</div>
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
<!-- イベント -->
<script type="text/javascript">
$(function(){
    //
    $('#btn_exec').on('click', function(){
        $('#form-search').submit();
    });
});
</script>
<!-- /イベント -->
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')

@component('layouts.modal')
    @slot('modalid', $modalid_clear)
    @slot('modaltitle', 'クリア')
    @slot('modalcontent')
        クリアします
    @endslot
    @slot('modalfooter')
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doclear();return false;">はい</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">いいえ</button>
        <script type="text/javascript">
            function doclear(){
                window.location.href='{{ url('tax/search') }}';
            }
        </script>
    @endslot
@endcomponent

@endsection
