<?php
    // ページタイトル
    if (!empty( Funcs::rq('id', $data['results']))) {
        // ページタイトル
        $title = '税情報 編集';
    } else {
        // ページタイトル
        $title = '税情報 登録';
    }
    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';

    // コントローラ
    $subnavs = [
        ['text'=>'一覧','href'=>url('tax')],
    ];

    // モーダルウィンドウ

    // ダイアログ
    $modalid_delete = 'dialogdelete';
?>
@extends('layouts.app')

@section('content')
{{ Form::open(['id'=>'input-form', 'method' => 'post', 'url' => 'tax/save', 'class'=>'form-horizontal form-label-left']) }}
    <div class="row justify-content-center w1024">
        <div class="col-12">
            <div class="card">
                <!-- card-body -->
                <div class="card-body">
                    @if( !empty( Funcs::rq('id', $data['results']) ) )
                        <div class="row pb-2 pl-1">
                            <div class="w150 mr-2 align-self-center text-nowrap text-right">ID</div>
                            <div class="w600 mr-2 align-self-center">
                                {{ Funcs::rq('id', $data['results']) }}
                            </div>
                        </div><!--/.row-->
                    @endif
                    <div class="row pb-2 pl-1">
                        <div class="w150 mr-2 align-self-center text-nowrap text-right">施工日<span class="label label-danger ml-1">必須</span></div>
                        <div class="w150 mr-2 align-self-center">
                            @component('layouts.inputs.datepicker')
                                @slot('accesskey', 'const_date')
                                @slot('loading', $data['results'])
                                @slot('options', ['class'=> $errors->has("const_date") ? config('const.inputclasses.datepicker') .' is-invalid ': config('const.inputclasses.datepicker')])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('const_date')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w150 mr-2 align-self-center text-nowrap text-right">消費税率<span class="label label-danger ml-1">必須</span></div>
                        <div class="w150 mr-2 align-self-center">
                            @component('layouts.inputs.num')
                                @slot('accesskey', 'tax_basic_rate')
                                @slot('loading', $data['results'])
                                @slot('options', ['class'=> $errors->has("tax_basic_rate") ? 'form-control is-invalid text-right': 'form-control text-right'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('tax_basic_rate')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w150 mr-2 align-self-center text-nowrap text-right">軽減税率<span class="label label-danger ml-1">必須</span></div>
                        <div class="w150 mr-2 align-self-center">
                            @component('layouts.inputs.num')
                                @slot('accesskey', 'reduction_tax_rate')
                                @slot('loading', $data['results'])
                                @slot('options', ['class'=> $errors->has("reduction_tax_rate") ? 'form-control is-invalid text-right': 'form-control text-right'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('reduction_tax_rate')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w150 mr-2 align-self-center text-nowrap text-right">税区分<span class="label label-danger ml-1">必須</span></div>
                        <div class="w150 mr-2 align-self-center">
                            @component('layouts.inputs.select')
                                @slot('accesskey', 'tax_flag')
                                @slot('loading', $data['results'])
                                @slot('pulldown', Config::get('const.tax_flag_list'))
                                @slot('options', ['class'=> $errors->has('tax_flag') ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('tax_flag')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w150 mr-2 align-self-center text-nowrap text-right">税計算区分<span class="label label-danger ml-1">必須</span></div>
                        <div class="w150 mr-2 align-self-center">
                            @component('layouts.inputs.select')
                                @slot('accesskey', 'tax_calc_flag')
                                @slot('loading', $data['results'])
                                @slot('pulldown', Config::get('const.tax_calc_flag_list'))
                                @slot('options', ['class'=> $errors->has('tax_calc_flag') ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('tax_calc_flag')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w150 mr-2 align-self-center text-nowrap text-right">税端数計算区分<span class="label label-danger ml-1">必須</span></div>
                        <div class="w150 mr-2 align-self-center">
                            @component('layouts.inputs.select')
                                @slot('accesskey', 'tax_fraction_type')
                                @slot('loading', $data['results'])
                                @slot('pulldown', Config::get('const.tax_fraction_type_list'))
                                @slot('options', ['class'=> $errors->has('tax_fraction_type') ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('tax_fraction_type')}}</span>
                        </div>
                    </div><!--/.row-->

                    <div class="row mt-3">
                        <div class="col">
                            @if( !empty( Funcs::rq('id', $data['results']) ) )
                                <!-- 編集の時のみ動作する -->
                                    <button type="button" id="btn_delete" class="btn btn-danger" data-toggle="modal" data-target="#{{$modalid_delete}}">削除</button>
                            @endif
                            {{ Form::button('登録', ['id'=>'btn_save', 'class' => 'btn btn-primary', 'data-toggle'=>'tooltip', 'title'=>'入力データを保存する']) }}
                            <a href="{{url('/tax')}}" class="btn btn-success" >戻る</a>
                        </div>
                    </div>
                </div>
                <!-- / card-body -->

                {{ Form::hidden('id', Funcs::rq('id',$data['results'])) }}
            </div>
        </div>
    </div>
    @csrf
{{ Form::close() }}
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')
<!-- イベント -->
<script type="text/javascript">
$(function(){
    $('#btn_save').on('click', function(){
        $('#input-form').submit();
    });
});
</script>
<!-- /イベント -->

<!-- 削除 -->
@if( !empty( Funcs::rq('id', $data['results']) ) )
    @component('layouts.modal')
        @slot('modalid', $modalid_delete)
        @slot('modaltitle', '削除')
        @slot('modalcontent')
            削除してよろしいですか？
        @endslot
        @slot('modalfooter')
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="dodelete();return false;">はい</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">いいえ</button>
            <script type="text/javascript">
                function dodelete(){
                    $('#form-delete').submit();
                }
            </script>
            <form id="form-delete" method="post" action="{{url('tax/delete').'/'.Funcs::rq('id',$data['results'])}}">
                {!! Form::hidden('id', Funcs::rq('id',$data['results'])) !!}
                @csrf
            </form>
        @endslot
    @endcomponent
@endif

@endsection
