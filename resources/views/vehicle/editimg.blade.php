<?php
//    // ページタイトル
    if (!empty( Funcs::rq('vehicle_file_name', $results[0]))) {
        // ページタイトル
        $title = 'パーツ画像 編集';
    } else {
        // ページタイトル
        $title = 'パーツ画像 登録';
    }
    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';

    // コントローラ
    $subnavs = [
        ['text'=>'一覧','href'=>url('vehicle')],
        ['text'=>'車両 編集','href'=>url('/vehicle/edit/'.Funcs::rq('id', $results[0]))],
        ['text'=>'取扱いパーツ','href'=>url('/vehicle/parts/'.Funcs::rq('id', $results[0]))],
    ];

?>
@extends('layouts.app')


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
    <!--baguetteBox-->
    <link rel="stylesheet" href="{{ asset('asset/dist/baguetteBox.min.css') }}">
    <script src="{{ asset('asset/dist/baguetteBox.min.js') }}" async></script>

    <!--dropzone-->
    <script src="{{ asset('asset/dropzone/js/dropzone.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('asset/dropzone/css/dropzone.css') }}">

    <script type="text/javascript">
        // baguetteBox
        window.onload = function() {
            baguetteBox.run('.entry-content');
        };

        // dropzone
        Dropzone.options.imageUpload = {
            dictDefaultMessage: 'アップロードするファイルをここへドロップしてください',
            acceptedFiles: '.jpg, .jpeg, .png',
            maxFilesize: 5,  //1つのファイルの最大サイズ( MB)
            dictFileTooBig: "ファイルが大きすぎます。(@{{filesize}}MB). 最大サイズ: @{{maxFilesize}}MB.",
            dictInvalidFileType: "画像ファイルのみアップロードが可能です。",
            maxFiles: 10,
            dictMaxFilesExceeded: "ファイルは10ファイルまで追加が可能です。",
            addRemoveLinks: true, // 削除リンクを表示する
            dictRemoveFile:'削除', // 削除リンクのラベルを設定
            dictCancelUpload:'キャンセル',
            init: function () {

                // アップロードがすべて完了したら、画面を更新してアップロードした画像を表示
                this.on('success', function(){
                    if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                        location.reload();
                    }
                });

                // 登録ファイル画像をDBから取得し一覧表示
                var myDropzone = this;
                var mockFile = "";
                var js_array = JSON.parse('<?php echo $php_results; ?>');
                $.each(js_array, function(index, value){
                    console.log(index + ':' + value.vehicle_file_name);
                    if ( value.vehicle_file_name != null ) {
                        mockFile = { name: value.vehicle_file_name};
                        myDropzone.options.addedfile.call(myDropzone, mockFile);
                        myDropzone.options.thumbnail.call(myDropzone, mockFile, "{{ asset('storage/items/100-') }}"+value.vehicle_file_name);
                        // プログレスバー非表示
                        myDropzone.emit("complete", mockFile);
                    }
                });
            },
            successmultiple: function() {
                console.log("test");
            },
            /**
             * 削除リンクを押下したときの処理を実行する
             * @override
             */
            removedfile: function(file) {
                // 車両ID取得
                var id = $('input[name=id]').val();
                var name = file.name;

                // サーバのファイルを削除
                $.ajax({
                    url: "{{ url('vehicle/imagedelete') }}",
                    type: "post",
                    data: {id: id, name: name},
                    cache: false,
                    // 成功したらサムネイルを削除
                    complete: function() {
                        var _ref;
                        if (file.previewElement) {
                            if ((_ref = file.previewElement) != null) {
                                _ref.parentNode.removeChild(file.previewElement);
                            }
                        }
                        //return this._updateMaxFilesReachedClass();
                        return;
                    },
                    error: function() {
                        alert("画像の削除に失敗しました。");
                    }
                });
            },
            thumbnail: function(file, dataUrl) {
              //イメージ
              var thumbnailElement, _i, _len, _ref;
              //アンカー
              var anchorElement, _i, _len, _ref;

              if (file.previewElement) {
                file.previewElement.classList.remove("dz-file-preview");
                _ref = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                _ref2 = file.previewElement.querySelectorAll("[gvg-vgv]");

                //イメージ
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                  thumbnailElement = _ref[_i];
                  thumbnailElement.alt = file.name;
                  thumbnailElement.src = dataUrl;
                }
                //アンカー
                for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
                  var str = dataUrl;
                  var strM = str.replace('100-', '');
                  anchorElement = _ref2[_i];
                  anchorElement.alt = file.name;
                  //画像URL
                  anchorElement.href = strM;
                  //lightbox用
//                  anchorElement.rel = 'lightbox[example]';
                  //画像名
                  anchorElement.title = file.name;
                }
              }
            },
        };
    </script>

	<style>
/*        dropzone .dz-preview .dz-image { width: 250px; height: 250px; }
        Dropzone.options.myAwesomeDropzone = { thumbnailWidth: 250, thumbnailHeight: 250; }*/
	</style>
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')
<!-- イベント -->

<!-- /イベント -->

@section('content')
    <div class="row justify-content-center w1024">
        <div class="col-12">
            <div class="card">
                <div id="no" class="card-header" style="font-size:11px;">
                    <div class="float-left">
                        <div class="row pl-3">
                            <span class="w15 align-self-center"><i class="far fa-list-alt text-info"></i></span>
                            <span class="w50 align-self-center">車両ID</span>
                            <span class="w100 align-self-center">:{{$results[0]->id}}</i></span>
                        </div>
                        <div class="row pl-3">
                            <span class="w15 align-self-center"><i class="fas fa-industry text-info"></i></span>
                            <span class="w50 align-self-center">メーカー</span>
                            <span class="w100 align-self-center">:{{$results[0]->manufacturer_name}}</i></span>
                        </div>
                        <div class="row pl-3">
                            <span class="w15 align-self-center"><i class="fas fa-car text-info"></i></span>
                            <span class="w50 align-self-center">車種</span>
                            <span class="w100 align-self-center">:{{$results[0]->model_name}}</i></span>
                        </div>
                    </div>
                    <div class="float-right">
                        <div class="align-self-center text-nowrap">
                            <i class="far fa-clock text-info mr-1"></i>更新日：{{ date_format( date_create($results[0]->updated_at), 'Y/m/d H:i:s') }}
                        </div>
                        <div class="align-self-center text-nowrap">
                            <i class="fas fa-user text-info mr-1"></i>作成者：{{ $results[0]->add_user_name }}
                        </div>
                        <div class="align-self-center text-nowrap">
                            <i class="fas fa-user text-info mr-1"></i>更新者：{{ $results[0]->upd_user_name }}
                        </div>
                    </div>
                </div>
                <!-- card-body -->
                <div class="card-body">
                    <div id="dropzones"  class="form-group{{ $errors->has('file') ? ' has-error' : '' }}"class="row mt-3">
                        <!-- Change /upload-target to your upload address -->
                        {{ Form::open(['id'=>'imageUpload', 'method' => 'post', 'url' => 'vehicle/saveimages', 'enctype'=>'multipart/form-data', 'files' => true, 'class'=>'dropzone needsclick dz-clickable entry-content']) }}
                            <div class="dz-message needsclick">
                                画像をここにドラッグするか、クリックして画像を選択して下さい。
                                <div class="dz-message needsclick entry-content">
                            </div>

                            {{ Form::hidden('id',               $results[0]->id) }}
                            {{ Form::hidden('vehicle_number',   $results[0]->vehicle_number) }}
                            @csrf
                        {{ Form::close() }}
                    </div>
                </div>
                <!-- / card-body -->
            </div>
            <!-- / card -->
        </div>
    </div>
    <br>
    <br>
@endsection


@endsection
