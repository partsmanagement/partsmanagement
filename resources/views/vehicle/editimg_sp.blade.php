<?php
    // ページタイトル
    $title = '車両画像編集登録';

//    // ページタイトル
//    if (!empty( Funcs::rq('id', $data['results']))) {
//        // ページタイトル
//        $title = '画像 編集';
//    } else {
//        // ページタイトル
//        $title = '画像 登録';
//    }
    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';

    // コントローラ
    $subnavs = [
        ['text'=>'一覧','href'=>url('vehicle')],
    ];

    // ログイン情報
    $charge = new partsmanagement\Libs\ChargeInfo;
    $user_type = $charge->user_type;

?>
@extends('layouts.app_sp_type1')

{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
    <!--baguetteBox-->
    <link rel="stylesheet" href="{{ asset('asset/dist/baguetteBox.min.css') }}">
    <script src="{{ asset('asset/dist/baguetteBox.min.js') }}"></script>

    <!--dropzone-->
    <script src="{{ asset('asset/dropzone/js/dropzone.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('asset/dropzone/css/dropzone.css') }}">

    <script type="text/javascript">
        // baguetteBox
        window.onload = function() {
            baguetteBox.run('.entry-content');
        };

        // dropzone
        Dropzone.options.imageUpload = {
            dictDefaultMessage: 'アップロードするファイルをここへドロップしてください',
            acceptedFiles: '.jpg, .jpeg, .png',
            maxFilesize: 5,  //1つのファイルの最大サイズ( MB)
            dictFileTooBig: "ファイルが大きすぎます。(@{{filesize}}MB). 最大サイズ: @{{maxFilesize}}MB.",
            dictInvalidFileType: "画像ファイルのみアップロードが可能です。",
            maxFiles: 10,
            dictMaxFilesExceeded: "ファイルは10ファイルまで追加が可能です。",
            addRemoveLinks: true, // 削除リンクを表示する
            dictRemoveFile:'削除', // 削除リンクのラベルを設定
            dictCancelUpload:'キャンセル',
            init: function () {

                // アップロードがすべて完了したら、画面を更新してアップロードした画像を表示
                this.on('success', function(){
                    if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                        location.reload();
                    }
                });

                // 登録ファイル画像をDBから取得し一覧表示
                var myDropzone = this;
                var mockFile = "";
                var js_array = JSON.parse('<?php echo $php_results; ?>');
                $.each(js_array, function(index, value){
                    if ( value.vehicle_file_name != null ) {
                        mockFile = { name: value.vehicle_file_name};
                        myDropzone.options.addedfile.call(myDropzone, mockFile);
                        myDropzone.options.thumbnail.call(myDropzone, mockFile, "{{ asset('storage/items/100-') }}"+value.vehicle_file_name);
                        // プログレスバー非表示
                        myDropzone.emit("complete", mockFile);
                    }
                });
            },
            successmultiple: function() {
                console.log("test");
            },
            /**
             * 削除リンクを押下したときの処理を実行する
             * @override
             */
            removedfile: function(file) {
                // 車両ID取得
                var id = $('input[name=id]').val();
                var name = file.name;

                // サーバのファイルを削除
                $.ajax({
                    url: "{{ url('vehicle/imagedelete') }}",
                    type: "post",
                    data: {id: id, name: name},
                    cache: false,
                    beforeSend: function(request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    // 成功したらサムネイルを削除
                    complete: function() {
                        var _ref;
                        if (file.previewElement) {
                            if ((_ref = file.previewElement) != null) {
                                _ref.parentNode.removeChild(file.previewElement);
                            }
                        }
                        //return this._updateMaxFilesReachedClass();
                        return;
                    },
                    error: function() {
                        alert("画像の削除に失敗しました。");
                    }
                });
            },
            thumbnail: function(file, dataUrl) {
              //イメージ
              var thumbnailElement, _i, _len, _ref;
              //アンカー
              var anchorElement, _i, _len, _ref;

              if (file.previewElement) {
                file.previewElement.classList.remove("dz-file-preview");
                _ref = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                _ref2 = file.previewElement.querySelectorAll("[gvg-vgv]");

                //イメージ
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                  thumbnailElement = _ref[_i];
                  thumbnailElement.alt = file.name;
                  thumbnailElement.src = dataUrl;
                }
                //アンカー
                for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
                  var str = dataUrl;
                  var strM = str.replace('100-', '' );

                  anchorElement = _ref2[_i];
                  anchorElement.alt = file.name;
                  //画像URL
                  anchorElement.href = strM;
                  //lightbox用
//                  anchorElement.rel = 'lightbox[example]';
                  //画像名
                  anchorElement.title = file.name;
                }
              }
            },
        };
    </script>

    <style>
        /*baguetteBox操作時、背景のスクロールをさせない*/
        html, body {
          overflow: auto;
          height: 100%;
        }
        body.open {
          overflow: hidden;
        }

        /*baguetteBox表示時、背景色変更*/
        #baguetteBox-overlay {
            background-color: #000!important;
        }
	</style>
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')
<!-- イベント -->

<!-- /イベント -->

@section('content')
    <h5 id="return" class="my-3 text-center text-nowrap"><p class="far fa-list-alt mr-1" style="color:#1e90ff;"></p>{{ $title }}</h5>
    <div class="row justify-content-center w1024">
        <div class="col-12">
            <div class="card">
                <div id="no" class="card-header">
                    <div class="align-self-center">
                        <div class="label bg-warning mr-1">{{$results[0]->manufacturer_name}}</div>
                        <div class="label bg-danger mr-1">{{Config::get('const.transmission_list')[$results[0]->transmission] }}</div>
                        <div class="label bg-secondary mr-1">{{Config::get('const.vehicle_color_list')[$results[0]->vehicle_color]}}</div><br>
                        <i class="fas fa-car-side pt-3" style="color:#228b22;"></i>&nbsp&nbsp<span class="h4 font-weight-bold">{{ $results[0]->model_name }}</span><br>
                        <div class="float-left">
                            <i class="w15 far fa-calendar-alt text-info mr-1"></i>年　式：{{ date_format( date_create($results[0]->first_registration_date."-01"), 'Y/m') }}<br>
                            <i class="w15 fas fa-warehouse mr-1 fa-xs" style="color:#8b008b;"></i>ヤード：{{ $results[0]->yard }}<br>
                            <i class="w15 fas fa-user text-info mr-1"></i>作成者：{{ $results[0]->add_user_name }}<br>
                            <i class="w15 fas fa-user text-info mr-1"></i>更新者：{{ $results[0]->upd_user_name }}<br>
                            <i class="w15 far fa-clock text-info mr-1"></i>更新日：{{ date_format( date_create($results[0]->updated_at), 'Y/m/d H:i:s') }}
                        </div>
                    </div>
                </div>
                <!-- card-body -->
                <div class="card-body">
                    <div id="dropzones"  class="form-group{{ $errors->has('file') ? ' has-error' : '' }}"class="row mt-3">
                        <!-- Change /upload-target to your upload address -->
                        {{ Form::open(['id'=>'imageUpload', 'method' => 'post', 'url' => 'vehicle/saveimages', 'enctype'=>'multipart/form-data', 'files' => true, 'class'=>'dropzone needsclick dz-clickable entry-content']) }}
                            <div class="dz-message needsclick">
                                画像をここにドラッグするか、クリックして画像を選択して下さい。
                            </div>

                            {{ Form::hidden('id',               $results[0]->id) }}
                            {{ Form::hidden('vehicle_number',   $results[0]->vehicle_number) }}
                            @csrf
                        {{ Form::close() }}
                    </div>
                </div>
                <!-- / card-body -->
            </div>
            <!-- / card -->
        </div>
        <div class="row mt-3">
            <div class="col">
                <a href="{{url('/vehicle')}}" class="btn btn-success" >戻る</a>
            </div>
        </div>
    </div>
    <br>
    <br>
@endsection


@endsection
