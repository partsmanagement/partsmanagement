<?php
    // ページタイトル
    $title = '車両一覧';

    // ページタイトル
    $title_en = 'Vehicle List';

    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';

    // コントローラ
    $subnavs = [
//        ['text'=>'新規登録','href'=>url('vehicle/edit')],
    ];

    // モーダルウィンドウ
    $modalid_clear         = 'alert_clear';
    $modalid_detail_search = 'detail_search';

    // ログイン情報
    $charge = new partsmanagement\Libs\ChargeInfo;
    $user_type = $charge->user_type;
    $funcs = new partsmanagement\Libs\Funcs;

?>
@extends('layouts.app_sp_type1')

@section('content')
<h5 id="return" class="my-3 text-center text-nowrap"><p class="far fa-list-alt mr-1" style="color:#1e90ff;"></p>{{ $title }}</h5>

<div class="row">
    <div class="col-12">
        <form id="form-search" method="get" action="{{ url('vehicle/search') }}" class="form-horizontal form-label-left">
            <div class="row pb-2 mr-1">
                <div class="col-6 pr-0 align-self-center text-nowrap">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#{{$modalid_detail_search}}">詳細検索</button>
                    <a class="btn btn-primary" href="{{url('vehicle/edit')}}">新規登録</a>
                </div>
            </div>
        </form>
    </div>
</div>

<?php $j=0; ?>
@foreach($results as $row)
<div class="row">
    <div class="col-12">
        <div class="card border-secondary mb-3" style="">

            <div class="card-header pl-1 py-1">
                <span class="label bg-warning mr-1">{{$row->manufacturer_name }}</span>
                <span class="font-weight-bold" style="vertical-align:middle;">{{ $row->model_name }}</span>
            </div> <!-- card-header-->

            {{-- スマホサイズ400px以下表示 --}}
            <div class="disp400 card-body px-1 py-1 css-fade1">
                <table border="0" width="100%">
                    <tr>
                        <td rowspan="5" class="border" width="30%">
                            <ol class="list">
                                <li class="item" id="item1">
                                    <div class="thumbnail">
                                        <div class="inner">
                                            @if(!empty($row->main_file_names))
                                                <img src="{{ asset('storage/items/100-' . $row->main_file_names) }}" alt="item-image" class="image" />
                                            @else
                                                <img src="{{ asset('storage/items/100-nophoto.png') }}" alt="item-image" class="image" />
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            </ol>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;" width="40%">
                            <div class="pl-2 pb-1" style="font-size:14px;"><div class="label bg-secondary mr-1">年式</div>{{ date_format( date_create($row->first_registration_date."-01"), 'Y年') }}</div>
                        </td>
                        <td style="vertical-align:top;" width="30%">
                            <div class="pl-2 pb-1" style="font-size:12px;"><div class="label bg-secondary mr-1">原動機型式</div>{{ $row->prime_mover_model }}</div>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;" width="30%">
                            <div class="pl-2 pb-1" style="font-size:12px;"><div class="label bg-secondary mr-1">ﾐｯｼｮﾝ</div>{{Config::get('const.transmission_list')[$row->transmission] }}</div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="vertical-align:top;" width="30%">
                            <div class="pl-2 pb-1" style="font-size:12px;"><div class="label bg-secondary mr-1">カラー</div>{{Config::get('const.vehicle_color_list')[$row->vehicle_color]}}</div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="vertical-align:top;" width="30%">
                            <div class="pl-2 pb-1" style="font-size:12px;"><div class="label bg-secondary mr-1">ヤード</div>{{ $row->yard }}</div>
                        </td>
                    </tr>
                </table>
                <span class="float-right" style="font-size:10px;vertical-align:bottom;">
                    最終更新日：{{ date_format( date_create($row->updated_at), 'Y年m月d日') }}
                </span>
            </div> <!-- card-body-->

            {{-- スマホサイズ600px以上表示 --}}
            <div class="disp600 card-body px-1 py-1 css-fade1">
                <table border="0" width="100%">
                    <tr>
                        <td class="border">
                            <ol class="list" style="width: 204px;">
                                <li class="item" id="item1" style="width: 200px;">
                                    <div class="thumbnail" style="height: 200px;">
                                        <div class="inner">
                                            @if(!empty($row->main_file_names))
                                                <img src="{{ asset('storage/items/200-' . $row->main_file_names) }}" alt="item-image" class="image" style="max-height: 200px;" />
                                            @else
                                                <img src="{{ asset('storage/items/200-nophoto.png') }}" alt="item-image" class="image" style="max-height: 200px;" />
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            </ol>
                        </td>
                        <td style="vertical-align:top;" width="80%">
                            {{-- 上段 --}}
                            <div class="card-group px-1 py-1" style="font-size:11px">
                                <div class="card">
                                    <div class="card-header px-1 py-1 text-center font-weight-bold">年式</div>
                                    <div class="card-body text-center align-middle">
                                        <p class="card-text text-danger h6 font-weight-bold">{{ date_format( date_create($row->first_registration_date."-01"), 'Y年') }}</p>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header px-1 py-1 text-center font-weight-bold">ミッション</div>
                                    <div class="card-body text-center align-middle">
                                        <p class="card-text h6">{{ Config::get('const.transmission_list')[$row->transmission] }}</p>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header px-1 py-1 text-center font-weight-bold">カラー</div>
                                    <span class="card-body px-1 py-1 text-center align-middle color{{$row->vehicle_color}}">
                                        <p class="card-text">{{ Config::get('const.vehicle_color_list')[$row->vehicle_color] }}</p>
                                    </span>
                                </div>
                                <div class="card">
                                    <div class="card-header px-1 py-1 text-center font-weight-bold">色識別コード</div>
                                    <div class="card-body px-1 text-center align-middle">
                                        <p class="card-text">{{ $row->color_code }}</p>
                                    </div>
                                </div>
                            </div>
                            {{-- 下段 --}}
                            <div class="card-group px-1 py-1" style="font-size:11px">
                                <div class="card">
                                    <div class="card-header px-1 py-1 text-center font-weight-bold">車体番号</div>
                                    <div class="card-body px-1 py-1 text-center align-middle">
                                        <p class="card-text">{{ $row->vehicle_number }}</p>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header px-1 py-1 text-center font-weight-bold">型式</div>
                                    <div class="card-body px-1 py-1 text-center align-middle">
                                        <p class="card-text">{{ $row->model_number }}</p>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header px-1 py-1 text-center font-weight-bold">型式指定-類別区分番号</div>
                                    <div class="card-body px-1 py-1 text-center align-middle">
                                        <p class="card-text">{{ $row->model_designation_number."-".$row->category_classification_number }}</p>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <span class="float-right pr-1" style="font-size:10px;vertical-align:bottom;">
                    最終更新者：{{ $row->upd_user_name }}<br>
                    最終更新日：{{ date_format( date_create($row->updated_at), 'Y年m月d日') }}
                </span>
            </div> <!-- card-body-->

            <div class="card-footer px-1 py-1" style="text-align: center;">
                <table border="0" width="100%">
                    <tr>
                        <td class="mx-3"><a style="width:100%;color:#FFF" class="btn btn-warning py-2 px-2" href="{{url('vehicle/parts/'.$row->id)}}">取扱パーツ</a></td>
                        <td class="mx-3"><a style="width:100%" class="btn btn-danger py-2 px-2" href="{{url('vehicle/images/'.$row->id)}}">画像　<i class="fa fa-image" aria-hidden="true"></i><span style="vertical-align:top;font-size:15px;"> {{$row->cnt_images}}枚</span></a></td>
                        <td class="mx-3"><a style="width:100%" class="btn btn-success py-2 px-3" href="{{url('vehicle/edit/'.$row->id)}}">車両編集</a></td>
                    </tr>
                </table>
            </div> <!-- card-footer-->

        </div>
    </div>
</div>
<?php $j++; ?>

@endforeach

@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
<!--カラーカテゴリー用CSS-->
<link href="{{ asset('css/vehicles_color.css') }}"  rel="stylesheet">
<!--画像縦横用CSS-->
<link href="{{ asset('css/vehicles_images.css') }}" rel="stylesheet">

<!-- CSS -->
<!--アニメーション-->
<style>
.css-fade1{
  /*アニメーション設定*/
  animation-name:fade-in1;
  animation-duration:1s; /*アニメーション時間*/
  animation-timing-function: ease-out; /*アニメーションさせるイージング*/
  animation-delay:0s; /*アニメーション開始させる時間*/
  animation-iteration-count:1; /*繰り返し回数*/
  animation-direction:normal; /*往復処理をするかどうか*/
  animation-fill-mode: forwards; /*アニメーション後のスタイルをどうするか*/
}
/* アニメーション*/
@keyframes fade-in1 {
  0% {opacity: 0}
  100% {opacity: 1}
}
</style>

<!-- イベント -->
<script type="text/javascript">
$(function(){
    //
    $('#btn_exec').on('click', function(){
        $('#form-search').submit();
    });

    /*
     * イベントハンドラの設定
     * load や resize も入れておかないと android で
     * うまく動作しないことがあるかも。問題なさそうなら外したほうが吉。
     */
    $(window).on("load orientationchange resize", function() {

        /*
         * 現在の回転角度を取得して縦横の判定を行う
         * 90 と -90 の場合は横向きであると判断できる
         */
        var sWidth = window.innerWidth;
        if(Math.abs(window.orientation) === 90) {
            if ( sWidth > 450) {
                console.log("横で見ている"+sWidth);
                $('.disp400').hide();
                $('.disp600').show();
            }else{
                $('.disp400').show();
                $('.disp600').hide();
            }
        }else{
            if ( sWidth < 450) {
                console.log("縦で見ている"+sWidth);
                $('.disp400').show();
                $('.disp600').hide();
            }else{
                $('.disp400').hide();
                $('.disp600').show();
            }
        }

    });

});
</script>
<!-- /イベント -->
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')
<!--
    ***********************************************
    * 詳細検索ウインドウ
    *
    ***********************************************
-->
@component('layouts.modal')
    @slot('modalid', $modalid_detail_search)
    @slot('modaltitle', '車両一覧 詳細検索')
    @slot('modalcontent')
        <form id="form-detail-search" method="get" action="{{ url('vehicle/search') }}" class="form-horizontal form-label-left">
            <div class="row pb-2 pl-1">
                <div class="w100 ml-3 mr-1 align-self-center text-nowrap text-left">メーカー名</div>
                <div class="w200 ml-1 align-self-center text-nowrap">
                    @component('layouts.inputs.select')
                        @slot('accesskey', 'manufacturer_id')
                        @slot('pulldown', $manufacturer_list)
                        @slot('loading', $prms)
                    @endcomponent
                </div>
            </div><!-- .row -->
            <div class="row pb-2 pl-1">
                <div class="w100 ml-3 mr-1 align-self-center text-nowrap text-left">車両名</div>
                <div class="w200 ml-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'model_name')
                        @slot('loading', $prms)
                    @endcomponent
                </div>
            </div><!-- .row -->
            <div class="row pb-2 pl-1">
                <div class="w100 ml-3 mr-1 align-self-center text-nowrap text-left">車体番号</div>
                <div class="w200 ml-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'vehicle_number')
                        @slot('loading', $prms)
                    @endcomponent
                </div>
            </div><!-- .row -->
            <div class="row pb-2 pl-1">
                <div class="w100 ml-3 mr-1 align-self-center text-nowrap text-left">色</div>
                <div class="w200 ml-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'vehicle_color')
                        @slot('loading', $prms)
                    @endcomponent
                </div>
            </div><!-- .row -->
            <div class="row pb-2 pl-1">
                <div class="w100 ml-3 mr-1 align-self-center text-nowrap text-left">型式指定番号</div>
                <div class="w200 ml-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'model_designation_number')
                        @slot('loading', $prms)
                    @endcomponent
                </div>
            </div><!-- .row -->
            <div class="row pb-2 pl-1">
                <div class="w100 ml-3 mr-1 align-self-center text-nowrap text-left">類別区分番号</div>
                <div class="w200 ml-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'category_classification_number')
                        @slot('loading', $prms)
                    @endcomponent
                </div>
            </div><!-- .row -->
<!--            <div class="row pb-2 pl-1">
                <div class="w100 ml-3 mr-1 align-self-center text-nowrap text-left">型式</div>
                <div class="w200 ml-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'model_number')
                        @slot('loading', $prms)
                    @endcomponent
                </div>
            </div> .row -->
            <div class="row pb-2 pl-1">
                <div class="w100 ml-3 mr-1 align-self-center text-nowrap text-left">ヤード</div>
                <div class="w200 ml-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'yard')
                        @slot('loading', $prms)
                    @endcomponent
                </div>
            </div><!-- .row -->
            <div class="row pb-2 pl-1">
                <div class="w100 ml-3 mr-1 align-self-center text-nowrap text-left">キーワード</div>
                <div class="w200 ml-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'search_keyword')
                        @slot('loading', $prms)
                    @endcomponent
                </div>
            </div><!-- .row -->
        </form>
    @endslot
    @slot('modalfooter')
        <div class="center-block">
            <button type="button" class="btn btn-secondary"     data-dismiss="modal" onclick="doclear();return false;"     >クリア</button>
            <button type="button" class="btn btn-primary w150"  data-dismiss="modal" onclick="do_dtlsearch();return false;">検索</button>
            <button type="button" class="btn btn-secondary"     data-dismiss="modal">閉じる</button>
            <script type="text/javascript">
                function doclear(){
                    window.location.href='{{ url('vehicle/search') }}';
                }
                function do_dtlsearch(){
                    $('#form-detail-search').submit();
                }
            </script>
        </div>
    @endslot
@endcomponent

@component('layouts.modal')
    @slot('modalid', $modalid_clear)
    @slot('modaltitle', 'クリア')
    @slot('modalcontent')
        クリアします
    @endslot
    @slot('modalfooter')
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doclear();return false;">はい</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">いいえ</button>
        <script type="text/javascript">
            function doclear(){
                window.location.href='{{ url('vehicle/search') }}';
            }
        </script>
    @endslot
@endcomponent

@endsection
