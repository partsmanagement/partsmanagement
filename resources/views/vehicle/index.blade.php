<?php
    // ページタイトル
    $title = '車両一覧';
    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';
    // コントローラ
    $subnavs = [
        ['text'=>'新規登録','href'=>url('vehicle/edit')],
    ];

    // モーダルウィンドウ
    $modalid_clear = 'catalog_alert_clear';
?>
@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form id="form-search" method="get" action="{{ url('vehicle/search') }}" class="form-horizontal form-label-left">
                    <div class="row pb-2 pl-1">
                        <div class="w70 mx-3 align-self-center text-nowrap">メーカー名</div>
                        <div class="w180 mx-3 align-self-center">
                            @component('layouts.inputs.select')
                                @slot('accesskey', 'manufacturer_id')
                                @slot('pulldown', $manufacturer_list)
                            @endcomponent
                        </div>
                        <div class="w70 mx-3 align-self-center text-nowrap">車両名</div>
                        <div class="w180 mx-3 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'model_name')
                            @endcomponent
                        </div>
                        <div class="w80 mx-3 align-self-center text-nowrap">車体番号</div>
                        <div class="w180 mx-3 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'vehicle_number')
                            @endcomponent
                        </div>
                    </div><!-- .row -->
                    <div class="row pb-2 pl-1">
                        <div class="w70 mx-3 align-self-center text-nowrap">カラー</div>
                        <div class="w180 mx-3 align-self-center">
                            @component('layouts.inputs.select')
                                @slot('accesskey', 'vehicle_color')
                                @slot('pulldown', Config::get('const.vehicle_color_list'))
                            @endcomponent
                        </div>
                        <div class="w70 mx-3 align-self-center text-nowrap">型式指定番号</div>
                        <div class="w180 mx-3 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'model_designation_number')
                            @endcomponent
                        </div>
                        <div class="w80 mx-3 align-self-center text-nowrap">類別区分番号</div>
                        <div class="w180 mx-3 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'category_classification_number')
                            @endcomponent
                        </div>
                    </div><!-- .row -->
                    <div class="row pb-2 pl-1">
                        <div class="w70 mx-3 align-self-center text-nowrap">型式</div>
                        <div class="w180 mx-3 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'model_number')
                            @endcomponent
                        </div>
                        <div class="w70 mx-3 align-self-center text-nowrap">ヤード</div>
                        <div class="w180 mx-3 align-self-center">
                            @component('layouts.inputs.select')
                                @slot('accesskey', 'yard_id')
                                @slot('pulldown', $yard_list)
                            @endcomponent
                        </div>
                        <div class="w80 mx-3 align-self-center text-nowrap">検索キーワード</div>
                        <div class="w180 mx-3 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'search_keyword')
                            @endcomponent
                        </div>
                    </div><!-- .row -->
                    <div class="col">
                        <div class="row justify-content-end">
                            <div class="mx-3">
                                <button type="button" id="btn_exec" class="btn btn-primary">検索</button>
                            </div>
                            <div class="mx-3">
                                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#{{$modalid_clear}}">クリア</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-12">
        <div class="card">
            <!--div class="card-header"></div-->
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <!-- ページャ -->
            			<nav  class="nav_style_height">
            			    {{ $results->links() }}
            			</nav>
            			<!-- /ページャ -->
                    </div>
                </div>
                <div class="number_style">
                    @if ( $results->Total() )
                        該当件数 <span>{{ $results->Total() }}</span> 件のうち <span>{{ $results->firstItem() }} ～ {{ $results->lastItem() }}</span> 件目を表示
                    @else
                        該当件数 <span>{{ $results->Total() }}</span> 件
                    @endif
                </div>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="thead-light">
                                <tr>
                                    <!--<th scope="col" width="3%">ID</th>-->
                                    <th scope="col" width="10%">メイン画像</th>
                                    <th scope="col" width="10%">メーカー名</th>
                                    <th scope="col" width="10%">車両名</th>
                                    <th scope="col" width="7%">車体番号</th>
                                    <th scope="col" width="10%">初年度<br>検査年月</th>
                                    <th scope="col" width="7%">型式</th>
                                    <th scope="col" width="7%">原動機<br>型式</th>
                                    <th scope="col" width="5%">カラー</th>
                                    <th scope="col" width="7%">型式指定番号</th>
                                    <th scope="col" width="7%">類別区分番号</th>
                                    <th scope="col" width="7%">ヤード</th>
                                    <th scope="col" width="7%">備考</th>
                                    <th scope="col" width="10%">登録日時</th>
                                    <th scope="col" width="7%">パーツ</th>
                                    <th scope="col" width="7%">画像</th>
                                    <th scope="col" width="7%">編集</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($results as $row)
                                <tr>
                                    <!--<td class="text-right">{{ $row->id }}</td>-->
                                    <td>
                                        <ol class="list">
                                            <li class="item" id="item1">
                                                <div class="thumbnail">
                                                    <div class="inner">
                                                        @if(!empty($row->main_file_names))
                                                            <img src="{{ asset('storage/items/100-' . $row->main_file_names) }}" alt="item-image" class="image" />
                                                        @else
                                                            <img src="{{ asset('storage/items/100-nophoto.png') }}" alt="item-image" class="image" />
                                                        @endif
                                                    </div>
                                                </div>
                                            </li>
                                        </ol>
                                    </td>
<!--                                    <td class="text-left">
                                        @if(!empty($row->main_file_names))
                                            <img id="itemimg" src="{{ asset('storage/items/100-' . $row->main_file_names) }}" alt="item-image" style=""/>
                                        @else
                                            <img id="itemimg" src="{{ asset('storage/items/100-nophoto.png') }}" alt="item-image" style=""/>
                                        @endif
                                    </td>-->
                                    <td class="text-left">{{ $row->manufacturer_name }}</td>
                                    <td class="text-left">{{ $row->model_name }}</td>
                                    <td class="text-left">{{ $row->vehicle_number }}</td>
                                    <td class="text-left">
                                        {{ str_replace('-', '/', $row->first_registration_date) }}<br>
                                        {{ $row->first_registration_date_wareki }}
                                    </td>
                                    <td class="text-left">{{ $row->model_number }}</td>
                                    <td class="text-left">{{ $row->prime_mover_model }}</td>
                                    <td class="text-left"><p class="radio color{{ $row->vehicle_color }}" style="position: relative;left:10px"></p></td>
                                    <td class="text-left">{{ $row->model_designation_number }}</td>
                                    <td class="text-left">{{ $row->category_classification_number }}</td>
                                    <td class="text-left">{{ $row->yard_name }}</td>
                                    <td class="text-left">{{ $row->remarks }}</td>
                                    <td class="text-left">{{ $row->upd_user_name }}<br>{{ date_format( date_create($row->updated_at), 'Y/m/d h:i') }}</td>
                                    <td class="text-center">
                                        <a class="btn btn-warning" href="{{url('vehicle/parts')}}/{{$row->id}}">パーツ</a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-info" href="{{url('vehicle/images')}}/{{$row->id}}">画像</a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-primary" href="{{url('vehicle/edit')}}/{{$row->id}}">編集</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-12">
                        <!-- ページャ -->
            			<nav  class="nav_style_height">
            			    {{ $results->links() }}
            			</nav>
            			<!-- /ページャ -->
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
    </div>
</div>
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')

<!--カラーカテゴリー用CSS-->
<link href="{{ asset('css/vehicles_color.css') }}"  rel="stylesheet">
<!--画像縦横用CSS-->
<link href="{{ asset('css/vehicles_images.css') }}" rel="stylesheet">

<!-- イベント -->
<script type="text/javascript">
$(function(){
    //
    $('#btn_exec').on('click', function(){
        $('#form-search').submit();
    });
});
</script>
<!-- /イベント -->
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')

@component('layouts.modal')
    @slot('modalid', $modalid_clear)
    @slot('modaltitle', 'クリア')
    @slot('modalcontent')
        クリアします
    @endslot
    @slot('modalfooter')
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doclear();return false;">はい</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">いいえ</button>
        <script type="text/javascript">
            function doclear(){
                window.location.href='{{ url('vehicle/search') }}';
            }
        </script>
    @endslot
@endcomponent

@endsection
