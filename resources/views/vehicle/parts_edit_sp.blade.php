<?php
    // ページタイトル
    $title = '取扱いパーツ';
    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';
    // コントローラ
    $subnavs = [
        ['text'=>'一覧','href'=>url('vehicle')],
        ['text'=>'パーツ画像登録','href'=>url('/vehicle/images/'.$vehicles_id)],
        ['text'=>'車両 編集','href'=>url('/vehicle/edit/'.$vehicles_id)],
    ];

    // モーダルウィンドウ
    $modalid_clear = 'catalog_alert_clear';
?>
@extends('layouts.app_sp_type1')

@section('content')
<h5 id="return" class="my-3 text-center text-nowrap"><p class="far fa-list-alt mr-1" style="color:#1e90ff;"></p>{{ $title }}</h5>

<div class="row mt-3">
    <div class="col-12">
        {{ Form::open(['id'=>'input-form', 'method' => 'post', 'url' => 'vehicle/saveparts', 'files' => true, 'class'=>'form-horizontal form-label-left']) }}
        <div class="row">
            <div class="col-12">
                <?php $i=1; ?>
                <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach($data as $row)
                    <div class="card border-secondary">
                        @if( $row->parts_status == 1 || $row->parts_status == '' )
                            <div class="card-header" style="" role="tab" id="heading{{$i}}">
                        @elseif( $row->parts_status == 2 )
                            <div class="card-header" style="background-color:#87ceeb" role="tab" id="heading{{$i}}">
                        @else( $row->parts_status == 3 )
                            <div class="card-header" style="background-color:#ffe4e1" role="tab" id="heading{{$i}}">
                        @endif
                            <h6 class="mb-0">
                                <a class="collapsed text-body" data-toggle="collapse" href="#collapse{{$i}}" role="button" aria-expanded="false" aria-controls="collapse{{$i}}">
                                    {!! Form::hidden('id['.$i.']', Funcs::rq('id', $row)) !!}
                                    {{$i}}.
                                    {!! Form::hidden('no['.$i.']', $i) !!}
                                    {{ $row->parts_name }}
                                    {!! Form::hidden('parts_id['.$i.']', $row->parts_id) !!}
                                    @if (!empty($row['remarks']))
                                        <i class="fas fa-edit pull-right" style="color:#1e90ff;"></i>
                                    @endif
                                </a>
                            </h6>
                        </div><!-- /.card-header -->
                        <div id="collapse{{$i}}" class="collapse" role="tabpanel" aria-labelledby="heading{{$i}}" data-parent="#accordion">
                            <div class="odd pointer itm" id="id{{$i}}">
                                <div class="align-middle text-left m-1">
                                    <div style="width:100%" id="div_price_change_flag" class="btn-group btn-group-toggle" data-toggle="buttons">
                                        @if( $row->parts_status == 1 || $row->parts_status == '' )
                                            <label id="lbl_cancel_setting_division_select" class="btn btn-outline-primary">
                                                {{ Form::radio('parts_status['.$i.']', '2', false ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['2'] }}
                                            </label>
                                            <label id="lbl_cancel_setting_division_auto" class="btn btn-outline-primary">
                                                {{ Form::radio('parts_status['.$i.']', '3', false ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['3'] }}
                                            </label>
                                            <label id="lbl_cancel_setting_division_clean" class="btn btn-outline-primary active">
                                                {{ Form::radio('parts_status['.$i.']', '1', true ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['1'] }}
                                            </label>
                                        @elseif( $row->parts_status == 2 )
                                            <label id="lbl_cancel_setting_division_select" class="btn btn-outline-primary active">
                                                {{ Form::radio('parts_status['.$i.']', '2', true ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['2'] }}
                                            </label>
                                            <label id="lbl_cancel_setting_division_auto" class="btn btn-outline-primary">
                                                {{ Form::radio('parts_status['.$i.']', '3', false,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['3'] }}
                                            </label>
                                            <label id="lbl_cancel_setting_division_clean" class="btn btn-outline-primary">
                                                {{ Form::radio('parts_status['.$i.']', '1', false ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['1'] }}
                                            </label>
                                        @else( $row->parts_status == 3 )
                                            <label id="lbl_cancel_setting_division_select" class="btn btn-outline-primary">
                                                {{ Form::radio('parts_status['.$i.']', '2', false ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['2'] }}
                                            </label>
                                            <label id="lbl_cancel_setting_division_auto" class="btn btn-outline-primary active">
                                                {{ Form::radio('parts_status['.$i.']', '3', true ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['3'] }}
                                            </label>
                                            <label id="lbl_cancel_setting_division_clean" class="btn btn-outline-primary">
                                                {{ Form::radio('parts_status['.$i.']', '1', false ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['1'] }}
                                            </label>
                                        @endif
                                    </div>
                                </div>
                                <div class="align-middle text-left m-1">
                                    {!! Form::textarea('remarks['.$i.']', $row['remarks'], ['style' => 'color:#000','class' => 'form-control ja input-sm margin_gap', 'rows' => '2','cols' => '30', 'placeholder' => '']) !!}
                                    <span style="color:#dc3545;">{{$errors->first('remarks')}}</span>
                                </div>
                            </div>
                        </div><!-- / .collapse -->
                    </div><!-- /.card -->
                    <?php $i++; ?>
                @endforeach
                </div><!-- /#accordion -->
                <br>
                <div class="row mt-3">
                    <div class="col text-center">
                        {{ Form::button('登録', ['id'=>'btn_save', 'class' => 'btn btn-primary', 'data-toggle'=>'tooltip', 'title'=>'入力データを保存する']) }}
                        <a href="{{url('/vehicle')}}" class="btn btn-success" >戻る</a>
                    </div>
                </div>

                {{ Form::hidden('vehicles_id', $vehicles_id) }}
            </div>
        </div>
        <br>
        @csrf
        {{ Form::close() }}
    </div>
    <br>
    <br>
</div>
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')

<!-- イベント -->
<script type="text/javascript">

$(function(){

    /* ******************************************
     * 更新ボタンクリック
     * *******************************************/
    $('#btn_save').on('click', function(){
        $('#input-form').submit();
    });

});
</script>
<!-- /イベント -->
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')

@component('layouts.modal')
    @slot('modalid', $modalid_clear)
    @slot('modaltitle', 'クリア')
    @slot('modalcontent')
        クリアします
    @endslot
    @slot('modalfooter')
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doclear();return false;">はい</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">いいえ</button>
        <script type="text/javascript">
            function doclear(){
                window.location.href='{{ url('vehicle/search') }}';
            }
        </script>
    @endslot
@endcomponent

@endsection
