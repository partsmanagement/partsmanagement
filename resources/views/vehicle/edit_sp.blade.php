<?php
    // ページタイトル
    if (!empty( Funcs::rq('id', $data['results']))) {
        // ページタイトル
        $title = '車両 編集';
    } else {
        // ページタイトル
        $title = '車両 登録';
    }
    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';

    // コントローラ
    $subnavs = [
        ['text'=>'一覧','href'=>url('vehicle')],
    ];

    // モーダルウィンドウ
    $modalid_customer       = 'findcustomer';

    // ダイアログ
    $modalid_delete = 'dialogdelete';

?>
@extends('layouts.app_sp_type1')

@section('content')
<h5 id="return" class="my-3 text-center text-nowrap"><p class="far fa-list-alt mr-1" style="color:#1e90ff;"></p>{{ $title }}</h5>

{{ Form::open(['id'=>'input-form', 'method' => 'post', 'url' => 'vehicle/save', 'files' => true, 'class'=>'form-horizontal form-label-left']) }}
    <div class="row justify-content-center w1024">
        <div class="col-12">
            @if( !empty( Funcs::rq('id', $data['results']) ) )
                <div class="row pb-2 pl-2">
                    <div class="w150 mr-2 align-self-center text-nowrap text-left">ID</div>
                    <div class="w400 mr-2 align-self-center">
                        {{ Funcs::rq('id', $data['results']) }}
                    </div>
                </div><!--/.row-->
            @endif
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">車体番号</div>
                <div class="w400 mr-2 my-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'vehicle_number')
                        @slot('loading', $data['results'])
                        @slot('placeholder', '')
                        @slot('maxlength', '40')
                        @slot('options', ['class'=> $errors->has("vehicle_number") ? 'form-control is-invalid text-left': 'form-control text-left'])
                    @endcomponent
                    <span style="color:#dc3545;">{{$errors->first('vehicle_number')}}</span>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">初年度検査年月<span class="label label-danger ml-1">必須</span></div>
                <div class="mr-2">
                    <div class="w150 mr-2 my-1 align-self-center">
                        <?php
                            $first_registration_date = Funcs::rq('first_registration_date', $data['results']);
                            if ( empty($first_registration_date) ) {
                                $first_registration_date = \Carbon\Carbon::now()->format("Y/m/d");
                            }else{
                                $first_registration_date = $first_registration_date."-01";
                            }
                        ?>
                        <input type="month" id="first_registration_date" name="first_registration_date" value="{{ date_format( date_create($first_registration_date), 'Y-m') }}" min="" max="" class="form-control ja input-sm">
                    </div>
                    <div class="w400 mr-2 align-self-center">
                        <span style="color:#dc3545;">{{$errors->first('first_registration_date')}}</span>
                    </div>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">メーカー<span class="label label-danger ml-1">必須</span></div>
                <div class="w400 mr-2 my-1 align-self-center">
                    @component('layouts.inputs.select')
                        @slot('accesskey', 'manufacturer_id')
                        @slot('loading', $data['results'])
                        @slot('pulldown', $manufacturer_list)
                        @slot('options', ['class'=> $errors->has("manufacturer_id") ? 'form-control is-invalid ': 'form-control'])
                    @endcomponent
                    <span style="color:#dc3545;">{{$errors->first('manufacturer_id')}}</span>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">車種<span class="label label-danger ml-1">必須</span></div>
                <div class="w400 mr-2 my-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'model_name')
                        @slot('loading', $data['results'])
                        @slot('placeholder', '')
                        @slot('maxlength', '60')
                        @slot('options', ['class'=> $errors->has("model_name") ? 'form-control is-invalid text-left': 'form-control text-left'])
                    @endcomponent
                    <span style="color:#dc3545;">{{$errors->first('model_name')}}</span>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">メイン車両画像</div>
                <div class="w400 mr-2 my-2 align-self-center">
                    <div class="form-group">
                        <ol class="list" style="width: 204px;">
                            <li class="item" id="item1" style="width: 200px;">
                                <div class="thumbnail" style="height: 200px;">
                                    <div class="inner">
                                        <div class="pb-1">
                                            @if(!empty(Funcs::rq('main_file_names', $data['results'])))
                                                <img id="itemimg" src="{{ asset('storage/items/200-' . Funcs::rq('main_file_names', $data['results'])) }}" alt="item-image" class="image" style="max-height: 200px;" />
                                            @else
                                                <img id="itemimg" src="{{ asset('storage/items/200-nophoto.png') }}" alt="item-image" class="image" style="max-height: 200px;" />
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ol>
                        <div class="mt-2">
                            <div class="btn-group pb-1">
                                <div class="pr-1">
                                    @if(!empty(Funcs::rq('main_file_names', $data['results'])))
                                        <button id="delimage" type="button" class="btn btn-danger btn-sm" style="display: block;">画像を削除</button>
                                    @else
                                        <button id="delimage" type="button" class="btn btn-danger btn-sm" style="display: none;">画像を削除</button>
                                    @endif
                                </div>
                                <div class="pr-1">
                                    {!! Form::file('main_file_names', ['id' => 'main_file_names']) !!}
                                </div>
                            </div>
                        </div>
                        {{ Form::hidden('nophoto_file', asset('storage/items/200-nophoto.png')) }}
                        {{ Form::hidden('item_db_filename',   Funcs::rq('main_file_names', $data['results'])) }}
                        {{ Form::hidden('item_del_filename',  '') }}
                    </div>
                    <span style="color:#dc3545;">{{$errors->first('main_file_names')}}</span>
                </div>
            </div>
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">色カテゴリー</div>
                <div class="w450 mr-2 my-1 align-self-center">
                    <ul class="wrap" style='width:120%'>
                        <?php
                            $cnt = count( Config::get('const.vehicle_color_list') );
                            $vec = Funcs::rq('vehicle_color', $data['results']);
                            if (empty($vec)) {
                                $vec = "1";
                            }
                        ?>
                        @for ( $i=1; $i<=$cnt; $i++ )
                            <li>
                                <input style="display: none;" type="radio" name="vehicle_color" value="{{ $i }}" id="radio{{$i}}" @if ( $vec==$i) checked @endif />
                                <label for="radio{{$i}}" class="radio color{{$i}}"></label>
                                <p class="arrow_box">{{ Config::get('const.vehicle_color_list')[$i] }}</p>
                            </li>
                        @endfor
                    </ul>
                    <span style="color:#dc3545;">{{$errors->first('vehicle_color')}}</span>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">カラー識別コード</div>
                <div class="w400 mr-2 my-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'color_code')
                        @slot('loading', $data['results'])
                        @slot('placeholder', '')
                        @slot('maxlength', '40')
                        @slot('options', ['class'=> $errors->has("vehicle_color") ? 'form-control is-invalid text-left': 'form-control text-left'])
                    @endcomponent
                    <span style="color:#dc3545;">{{$errors->first('vehicle_color')}}</span>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">AT/MT</div>
                <div class="w400 mr-2 my-1 align-self-center">
                    @component('layouts.inputs.select')
                        @slot('accesskey', 'transmission')
                        @slot('loading', $data['results'])
                        @slot('pulldown', Config::get('const.transmission_list'))
                        @slot('options', ['class'=> $errors->has('transmission') ? 'form-control is-invalid ': 'form-control'])
                    @endcomponent
                    <span style="color:#dc3545;">{{$errors->first('transmission')}}</span>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">型式</div>
                <div class="w400 mr-2 my-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'model_number')
                        @slot('loading', $data['results'])
                        @slot('placeholder', '')
                        @slot('maxlength', '40')
                        @slot('options', ['class'=> $errors->has("model_number") ? 'form-control is-invalid text-left': 'form-control text-left'])
                    @endcomponent
                    <span style="color:#dc3545;">{{$errors->first('model_number')}}</span>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">原動機型式</div>
                <div class="w400 mr-2 my-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'prime_mover_model')
                        @slot('loading', $data['results'])
                        @slot('placeholder', '')
                        @slot('maxlength', '40')
                        @slot('options', ['class'=> $errors->has("prime_mover_model") ? 'form-control is-invalid text-left': 'form-control text-left'])
                    @endcomponent
                    <span style="color:#dc3545;">{{$errors->first('prime_mover_model')}}</span>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">型式指定番号</div>
                <div class="w400 mr-2 my-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'model_designation_number')
                        @slot('loading', $data['results'])
                        @slot('placeholder', '')
                        @slot('maxlength', '5')
                        @slot('options', ['class'=> $errors->has("model_designation_number") ? 'form-control is-invalid text-left': 'form-control text-left'])
                    @endcomponent
                </div>
                <div class="w400 mr-2 align-self-center">
                    <span style="color:#dc3545;">{{$errors->first('model_designation_number')}}</span>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">類別区分番号</div>
                <div class="w400 mr-2 my-1 my-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'category_classification_number')
                        @slot('loading', $data['results'])
                        @slot('placeholder', '')
                        @slot('maxlength', '4')
                        @slot('options', ['class'=> $errors->has("category_classification_number") ? 'form-control is-invalid text-left': 'form-control text-left'])
                    @endcomponent
                </div>
                <div class="w400 mr-2 align-self-center">
                    <span style="color:#dc3545;">{{$errors->first('category_classification_number')}}</span>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">ヤード</div>
                <div class="w400 mr-2 my-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'yard')
                        @slot('loading', $data['results'])
                        @slot('placeholder', '')
                        @slot('maxlength', '80')
                        @slot('options', ['class'=> $errors->has("yard") ? 'form-control is-invalid text-left': 'form-control text-left'])
                    @endcomponent
                    <span style="color:#dc3545;">{{$errors->first('yard')}}</span>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">提供者名</div>
                <div class="w400 mr-2 my-1 align-self-center">
                    @component('layouts.inputs.text')
                        @slot('accesskey', 'provider_name')
                        @slot('loading', $data['results'])
                        @slot('placeholder', '')
                        @slot('maxlength', '20')
                        @slot('options', ['class'=> $errors->has("provider_name") ? 'form-control is-invalid text-left': 'form-control text-left'])
                    @endcomponent
                    <span style="color:#dc3545;">{{$errors->first('provider_name')}}</span>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">取得金額</div>
                <div class="w400 mr-2 my-1 align-self-center">
                    @component('layouts.inputs.num')
                        @slot('default', 0)
                        @slot('accesskey', 'acquisition_amount')
                        @slot('loading', $data['results'])
                        @slot('placeholder', '')
                        @slot('maxlength', '10')
                        @slot('options', ['class'=> $errors->has("acquisition_amount") ? 'form-control ja input-sm text-right decimal_point_1 is-invalid ': 'form-control ja input-sm text-right decimal_point_1', 'maxlength' => '8'])
                    @endcomponent
                </div>
                <div class="w400 mr-2 align-self-center">
                    <span style="color:#dc3545;">{{$errors->first('acquisition_amount')}}</span>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">パーツ</div>
                <div class="w500 mr-2 my-1 align-self-center">
                    <div class="mt-2 mb-2">
                        <a href="{{url('/vehicle/parts/'.Funcs::rq('id', $data['results']))}}" class="btn btn-warning" >取扱いパーツ画面へ</a>
                    </div>
                    @if ( !$v_parts_list->isEmpty() )
                        <table class="table table-striped table-hover table-bordered">
<!--                            <thead class="thead-light">
                                <tr class="text-center">
                                    <th scope="col" width="3%">ID</th>
                                    <th scope="col" width="15%">ステータス</th>
                                    <th scope="col" width="30%">パーツ名</th>
                                    <th scope="col" width="50%">備考</th>
                                </tr>
                            </thead>-->
                            <tbody>
                                @foreach($v_parts_list as $row)
                                <tr>
                                    @if ( $row->parts_status == Config::get('const.parts_status_cd')['OK'] )
                                        <td width="3%" class="align-middle text-center" style="color:#32cd32">
                                            <i class="fas fa-check-circle"></i>
                                        </td>
                                    @elseif ( $row->parts_status == Config::get('const.parts_status_cd')['NG'] )
                                        <td width="3%" class="align-middle text-center" style="color:#FF0000">
                                            <i class="fa fa-times-circle" aria-hidden="true"></i>
                                        </td>
                                    @endif
                                    <td width="45%" class="align-middle text-left">{{ $row->parts_name }}</td>
                                    <td width="45%" class="align-middle text-left">{{ $row->remarks }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <p class="text-info h5 w500 mt-3">取扱いパーツが登録されていません。<p>
                    @endif
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">特記事項</div>
                <div class="w400 mr-2 align-self-center text-nowrap">
                    @component('layouts.inputs.textarea')
                        @slot('accesskey', 'remarks')
                        @slot('loading', $data['results'])
                        @slot('options', ['rows' => 2, 'class'=> $errors->has("remarks") ? 'form-control is-invalid ': 'form-control'])
                    @endcomponent
                    <span style="color:#dc3545;">{{$errors->first('remarks')}}</span>
                </div>
            </div><!--/.row-->
            <div class="row pb-2 pl-2">
                <div class="w150 mr-2 align-self-center text-nowrap text-left">検索キーワード</div>
                <div class="w400 mr-2 align-self-center text-nowrap">
                    @component('layouts.inputs.textarea')
                        @slot('accesskey', 'search_keyword')
                        @slot('loading', $data['results'])
                        @slot('placeholder', '例）○○○、○○○○、○○○○○')
                        @slot('options', ['rows' => 2, 'class'=> $errors->has("search_keyword") ? 'form-control is-invalid ': 'form-control'])
                    @endcomponent
                    <span style="color:#dc3545;">{{$errors->first('search_keyword')}}</span>
                </div>
            </div><!--/.row-->
            <br>
            <br>
            <div class="row mt-3 text-center">
                <div class="col">
                    @if( !empty( Funcs::rq('id', $data['results']) ) )
                        <!-- 編集の時のみ動作する -->
                            <button type="button" id="btn_delete" class="btn btn-danger" data-toggle="modal" data-target="#{{$modalid_delete}}">削除</button>
                    @endif
                    {{ Form::button('登録', ['id'=>'btn_save', 'class' => 'btn btn-primary', 'data-toggle'=>'tooltip', 'title'=>'入力データを保存する']) }}
                    <a href="{{url('/vehicle')}}" class="btn btn-success" >戻る</a>
                </div>
            </div>

        {{ Form::hidden('id', Funcs::rq('id',$data['results'])) }}
        </div><!--/.col-12-->
    </div>
    <br>
    <br>

    @csrf
{{ Form::close() }}
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
<!--カラーカテゴリー用CSS-->
<link href="{{ asset('css/vehicles_color.css') }}" rel="stylesheet">
<!--画像縦横用CSS-->
<link href="{{ asset('css/vehicles_images.css') }}" rel="stylesheet">

@endsection

{{-- 追加文末部分 --}}
@section('postdocument')

<style>
    /* Base for label styling */
    [type="checkbox"]:not(:checked),
    [type="checkbox"]:checked {
      position: absolute;
      left: -9999px;
    }
    [type="checkbox"]:not(:checked) + label,
    [type="checkbox"]:checked + label {
      position: relative;
      padding-left: 1.95em;
      cursor: pointer;
    }

    /* checkbox aspect */
    [type="checkbox"]:not(:checked) + label:before,
    [type="checkbox"]:checked + label:before {
      content: '';
      position: absolute;
      left: 0; top: 0;
      width: 1.25em; height: 1.25em;
      border: 2px solid #ccc;
      background: #fff;
      border-radius: 4px;
      box-shadow: inset 0 1px 3px rgba(0,0,0,.1);
    }
    /* checked mark aspect */
    [type="checkbox"]:not(:checked) + label:after,
    [type="checkbox"]:checked + label:after {
      content: '\2713\0020';
      position: absolute;
      top: .15em; left: .22em;
      font-size: 1.3em;
      line-height: 0.8;
      color: #09ad7e;
      transition: all .2s;
      font-family: 'Lucida Sans Unicode', 'Arial Unicode MS', Arial;
    }
    /* checked mark aspect changes */
    [type="checkbox"]:not(:checked) + label:after {
      opacity: 0;
      transform: scale(0);
    }
    [type="checkbox"]:checked + label:after {
      opacity: 1;
      transform: scale(1);
    }
    /* disabled checkbox */
    [type="checkbox"]:disabled:not(:checked) + label:before,
    [type="checkbox"]:disabled:checked + label:before {
      box-shadow: none;
      border-color: #bbb;
      background-color: #ddd;
    }
    [type="checkbox"]:disabled:checked + label:after {
      color: #999;
    }
    [type="checkbox"]:disabled + label {
      color: #aaa;
    }
    /* accessibility */
    [type="checkbox"]:checked:focus + label:before,
    [type="checkbox"]:not(:checked):focus + label:before {
      border: 2px dotted blue;
    }

    /* hover style just for information */
    label:hover:before {
      border: 2px solid #4778d9!important;
    }

</style>

<!-- イベント -->
<script type="text/javascript">

    $(function(){

        /* ******************************************
         * パーツ全選択チェックボックス
         * *******************************************/
        $('#all').on('change', function() {
            $('input[name^=parts]').prop('checked', this.checked);
        });

        /* ******************************************
         * 更新ボタンクリック
         * *******************************************/
        $('#btn_save').on('click', function(){
            $('#input-form').submit();
        });

        /* ******************************************
         * メーカーセレクト；選択イベント
         * *******************************************/
        $(document).on('change', "select[name=manufacturer_id]", function() {
            $('#model_id option').remove();
            var manufacturer_id = $('select[name=manufacturer_id]').val();

            // API呼び出し
            $.ajax({
                url: "{{ url('getmanufacturer/models/') }}/"+manufacturer_id,
                type: "get",
                dataType: 'json',
                fail: function() {
                    //DANGER
                },
                success: function(response) {
                    console.log(response);
                    $('#model_id').val(response.customer_name);

                    $.each(response, function(id, value){
                        console.log(value.id);
                        $('#model_id').append($('<option>').text(value.model_name).attr('value', value.id));
                    });
                }
            });

            return ;
        });

        /* ******************************************
         * メイン車両画像：変更イベント
         * *******************************************/
        $('#main_file_names').change(function(e){
            var file = e.target.files[0];
            var reader = new FileReader();

            if(file.type.indexOf("image") < 0){
              alert("画像ファイルを指定してください。");
              return false;
            }

            //選択した画像ファイルのプレビュー表示
            reader.onload = (function(file){
              return function(e){
                $("#itemimg").attr("src", e.target.result);
                $("#itemimg").attr("title", file.name);
              };
            })(file);

            // 画像ファイル削除ボタンを表示
            $("#delimage").css("display", "block");

            //削除する画像ファイル名（hidden）をクリア
            $("[name=item_del_filename]").val("");

            reader.readAsDataURL(file);
        });

        /* ******************************************
         * メイン車両画像：削除イベント
         * *******************************************/
        $('#delimage').click(function(e){
            //プレビュー表示の画像をNo Photo画像に差し替る
            var nophoto_file = $("[name=nophoto_file]").val();
            $("#itemimg").attr("src", nophoto_file);

            //削除する画像ファイル名をhiddenにセット
            $("[name=item_del_filename]").val( $("[name=item_db_filename]").val());

            //選択した画像ファイル名の表示クリア
            $("[name=main_file_names]").val("");

            // 画像ファイル削除ボタンを非表示
            $("#delimage").css("display", "none");
        });

    });

</script>
<!-- /イベント -->

<style>
.originalFileBtn {
    display: inline-block;
    position: relative;
}
.originalFileBtn input[type="file"] {
    height: 100%;
    left: 0;
    opacity: 0;
    position: absolute;
    top: 0;
    width: 100%;
}
</style>


<!-- 削除 -->
@if( !empty( Funcs::rq('id', $data['results']) ) )
    @component('layouts.modal')
        @slot('modalid', $modalid_delete)
        @slot('modaltitle', '削除')
        @slot('modalcontent')
            削除してよろしいですか？
        @endslot
        @slot('modalfooter')
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="dodelete();return false;">はい</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">いいえ</button>
            <script type="text/javascript">
                function dodelete(){
                    $('#form-delete').submit();
                }
            </script>
            <form id="form-delete" method="post" action="{{url('vehicle/delete').'/'.Funcs::rq('id',$data['results'])}}">
                {!! Form::hidden('id', Funcs::rq('id',$data['results'])) !!}
                @csrf
            </form>
        @endslot
    @endcomponent
@endif

@endsection
