<?php
    // ページタイトル
    $title = '取扱いパーツ';
    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';
    // コントローラ
    $subnavs = [
        ['text'=>'一覧','href'=>url('vehicle')],
        ['text'=>'パーツ画像登録','href'=>url('/vehicle/images/'.$vehicles_id)],
        ['text'=>'車両 編集','href'=>url('/vehicle/edit/'.$vehicles_id)],
    ];

    // モーダルウィンドウ
    $modalid_clear = 'catalog_alert_clear';
?>
@extends('layouts.app')

@section('content')

    <div class="row mt-3">
        <div class="col-12">
            <div class="card">
                {{ Form::open(['id'=>'input-form', 'method' => 'post', 'url' => 'vehicle/saveparts', 'files' => true, 'class'=>'form-horizontal form-label-left']) }}
                <!--div class="card-header"></div-->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <table id="tbl" class="table table-responsive table-striped table-condensed jambo_table bulk_action table_frame table-hove table-bordered">
                                <thead class="thead-light">
                                    <tr class="text-center ">
                                        <!--<th scope="col" width="3%">ID</th>-->
                                        <th class="align-middle" scope="col" width="5%">表示<br>順番</th>
                                        <th class="align-middle" scope="col" width="10%">ステータス</th>
                                        <th class="align-middle" scope="col" width="20%">パーツ名</th>
                                        <th class="align-middle" scope="col" width="60%">備考</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                    @foreach($data as $row)
                                        <tr class="odd pointer itm" id="id{{$i}}">
                                            <td class="align-middle text-center handle" width="20px">
                                                {!! Form::hidden('id['.$i.']', Funcs::rq('id', $row)) !!}
                                                {{$i}}
                                                {!! Form::hidden('no['.$i.']', $i) !!}
                                            </td>
                                            <td class="align-middle text-left">
                                                <div id="div_price_change_flag" class="btn-group btn-group-toggle" data-toggle="buttons">
                                                    @if( $row->parts_status == 1 || $row->parts_status == '' )
                                                        <label id="lbl_cancel_setting_division_select" class="btn btn-outline-primary">
                                                            {{ Form::radio('parts_status['.$i.']', '2', false ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['2'] }}
                                                        </label>
                                                        <label id="lbl_cancel_setting_division_auto" class="btn btn-outline-primary">
                                                            {{ Form::radio('parts_status['.$i.']', '3', false ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['3'] }}
                                                        </label>
                                                        <label id="lbl_cancel_setting_division_clean" class="btn btn-outline-primary active">
                                                            {{ Form::radio('parts_status['.$i.']', '1', true ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['1'] }}
                                                        </label>
                                                    @elseif( $row->parts_status == 2 )
                                                        <label id="lbl_cancel_setting_division_select" class="btn btn-outline-primary active">
                                                            {{ Form::radio('parts_status['.$i.']', '2', true ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['2'] }}
                                                        </label>
                                                        <label id="lbl_cancel_setting_division_auto" class="btn btn-outline-primary">
                                                            {{ Form::radio('parts_status['.$i.']', '3', false,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['3'] }}
                                                        </label>
                                                        <label id="lbl_cancel_setting_division_clean" class="btn btn-outline-primary">
                                                            {{ Form::radio('parts_status['.$i.']', '1', false ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['1'] }}
                                                        </label>
                                                    @else( $row->parts_status == 3 )
                                                        <label id="lbl_cancel_setting_division_select" class="btn btn-outline-primary">
                                                            {{ Form::radio('parts_status['.$i.']', '2', false ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['2'] }}
                                                        </label>
                                                        <label id="lbl_cancel_setting_division_auto" class="btn btn-outline-primary active">
                                                            {{ Form::radio('parts_status['.$i.']', '3', true ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['3'] }}
                                                        </label>
                                                        <label id="lbl_cancel_setting_division_clean" class="btn btn-outline-primary">
                                                            {{ Form::radio('parts_status['.$i.']', '1', false ,array('id'=>'parts_status['.$i.']', 'autocomplete'=>'off')) }}{{ Config::get('const.parts_status_list')['1'] }}
                                                        </label>
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="align-middle text-left">
                                                {{ $row->parts_name }}
                                                {!! Form::hidden('parts_id['.$i.']', $row->parts_id) !!}
                                            </td>
                                            <td class="align-middle text-left">
                                                {!! Form::textarea('remarks['.$i.']', $row['remarks'], ['style' => 'color:#000','class' => 'form-control ja input-sm margin_gap', 'rows' => '2','cols' => '30', 'placeholder' => '']) !!}
                                                <span style="color:#dc3545;">{{$errors->first('remarks')}}</span>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                </tbody>
                            </table>
                            <br>
                            <div class="row mt-3">
                                <div class="col">
                                    {{ Form::button('登録', ['id'=>'btn_save', 'class' => 'btn btn-primary', 'data-toggle'=>'tooltip', 'title'=>'入力データを保存する']) }}
                                    <a href="{{url('/vehicle')}}" class="btn btn-success" >戻る</a>
                                </div>
                            </div>

                            {{ Form::hidden('vehicles_id', $vehicles_id) }}
                        </div>
                    </div>
                    <br>
                </div>
            @csrf
            {{ Form::close() }}
            </div>
            <br>
            <br>
        </div>
    </div>
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')

<!-- イベント -->
<script type="text/javascript">

$(function(){

    /* ******************************************
     * 更新ボタンクリック
     * *******************************************/
    $('#btn_save').on('click', function(){
        $('#input-form').submit();
    });

});
</script>
<!-- /イベント -->
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')

@component('layouts.modal')
    @slot('modalid', $modalid_clear)
    @slot('modaltitle', 'クリア')
    @slot('modalcontent')
        クリアします
    @endslot
    @slot('modalfooter')
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doclear();return false;">はい</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">いいえ</button>
        <script type="text/javascript">
            function doclear(){
                window.location.href='{{ url('vehicle/search') }}';
            }
        </script>
    @endslot
@endcomponent

@endsection
