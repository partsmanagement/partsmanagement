// ------------------------------
// -- 共通関数
// ------------------------------
function tonum(value){
    // var array2 = String(value).match(/-?[0-9]+\.?[0-9]*/g);
    // for(var i = 0; i < array2.length; i++) {
    //     return parseFloat(array2[i]);
    // }
    // return 0;
    return Number(value);//エイリアス
}
function tomoney(value){
    // // // // num = num.replace(/(\d)(?=(\d\d\d)+$)/g, '$1,');
    // // // return tonum(value).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    // // var a = tonum(value)
    // // return a.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    // return value.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');

    var n = tonum(value);
    return n.toString(10).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}
function isempty(value){
    if ( value == null ) {
      // typeof null -> object : for hack a bug of ECMAScript
      return true;
    }
    switch( typeof value ) {
      case 'object':
        if ( Array.isArray( value ) ) {
          // When object is array:
          return ( value.length === 0 );
        } else {
          // When object is not array:
          if ( Object.keys( value ).length > 0 || Object.getOwnPropertySymbols(value).length > 0 ) {
            return false;
          } else
          if ( value.valueOf().length !== undefined ) {
            return ( value.valueOf().length === 0 );
          } else
          if ( typeof value.valueOf() !== 'object' ) {
            return is_empty( value.valueOf() );
          } else {
            return true;
          }
        }
        break;
      case 'string':
        return ( value === '' );
        break;
      case 'number':
        return ( value == 0 );
        break;
      case 'boolean':
        return ! value;
        break;
      case 'undefined':
      case 'null':
        return true;
        break;
      case 'symbol':
        // Since ECMAScript6
      case 'function':
      default:
        return false;
        break;
    }
    // デフォルト：True/引数は空である
    return true;
}

// ------------------------------
// -- ドラッグ＆ドロップでソートする
// ------------------------------
$(function () {
    $('.sortable').sortable();
});

// ------------------------------
// -- カレンダー
// ------------------------------
$(function () {
    $('.datepicker').datetimepicker({
        dayViewHeaderFormat: 'YYYY年 MMMM',
        tooltips: {
            close: '閉じる',
            selectMonth: '月を選択',
            prevMonth: '前月',
            nextMonth: '次月',
            selectYear: '年を選択',
            prevYear: '前年',
            nextYear: '次年',
            selectTime: '時間を選択'
        },
        format: 'YYYY/MM/DD',
        locale: 'ja',
        showClose: true
    });
    $('.monthpicker').datetimepicker({
        dayViewHeaderFormat: 'YYYY年 MMMM',
        tooltips: {
            close: '閉じる',
            selectMonth: '月を選択',
            prevMonth: '前月',
            nextMonth: '次月',
            selectYear: '年を選択',
            prevYear: '前年',
            nextYear: '次年',
            selectTime: '時間を選択'
        },
        format: 'YYYY/MM',
        locale: 'ja',
        showClose: true
    });
    $('.yearpicker').datetimepicker({
        dayViewHeaderFormat: 'YYYY年 MMMM',
        tooltips: {
            close: '閉じる',
            selectMonth: '月を選択',
            prevMonth: '前月',
            nextMonth: '次月',
            selectYear: '年を選択',
            prevYear: '前年',
            nextYear: '次年',
            selectTime: '時間を選択'
        },
        format: 'YYYY',
        locale: 'ja',
        showClose: true
    });
    $('.timepicker').datetimepicker({
        tooltips: {
            close: '閉じる',
            pickHour: '時間を取得',
            incrementHour: '時間を増加',
            decrementHour: '時間を減少',
            pickMinute: '分を取得',
            incrementMinute: '分を増加',
            decrementMinute: '分を減少',
            pickSecond: '秒を取得',
            incrementSecond: '秒を増加',
            decrementSecond: '秒を減少',
            togglePeriod: '午前/午後切替',
            selectTime: '時間を選択'
        },
        format: 'HH:mm',
        locale: 'ja',
        showClose: true
    });
});

// ------------------------------
// -- ENTERキーのTAB移動
// ------------------------------
// input項目をEnter キー、Shift+Enterキーでtab移動 without(button, hidden)
function fEnterChangeTab(){
    // ② input要素の選択。但し、ボタンとhidden型は除く。
    //var oObject = "#Div要素のid :input:not(:button):not(:hidden)";
    var oObject = "input:not(:button):not(:hidden)";
    $(oObject).keypress(function(e) {
        var c = e.which ? e.which : e.keyCode; // クロスブラウザ対応
        if (c == 13) {
            var index = $(oObject).index(this); // indexは0～
            var cNext = "";
            var nLength = $(oObject).length;
            for(i=index;i<nLength;i++){
                cNext = e.shiftKey ? ":lt(" + index + "):last" : ":gt(" + index + "):first";
                // ③ 止まってはいけいない属性 readonly
                if ($(oObject + cNext).attr("readonly") == "readonly") {
                    if (e.shiftKey) index--; // １つ前
                    else index++;            // 次へ
                }
                // ③ 止まってはいけいない属性 disabled
                else if ($(oObject + cNext).prop("disabled") == true) {
                    if (e.shiftKey) index--; // １つ前
                    else index++;            // 次へ
                }
                // ③ 止まってはいけいないクラス noentered
                else if ($(oObject + cNext).hasClass("noentered") == true) {
                    if (e.shiftKey) index--; // １つ前
                    else index++;            // 次へ
                }
                else break;
            }
            if (index == nLength - 1) {
                if (! e.shiftKey){
                    // 最後の項目なら、最初に移動。
                    cNext = ":eq(1)";
                }
            }
            if (index == 0) {
                if (e.shiftKey) {
                    // 最初の項目なら、最後に移動。
                    cNext = ":eq(" + (nLength - 1) + ")";
                }
            }
            $(oObject + cNext).focus();
            e.defaultPrevented;
        }
    });
}
// function setEvent_enteredTab(){
//     // ④onloadのタイミングでこの関数を実行
//     if(window.attachEvent){
//         // IE用
//         window.detachEvent('onload',fEnterChangeTab);
//         window.attachEvent('onload',fEnterChangeTab);
//     }
//     else if (window.opera){
//         // opera用
//         window.removeEventListener('load',fEnterChangeTab,false);
//         window.addEventListener('load',fEnterChangeTab,false);
//     }
//     else {
//         // Mozilla用
//         window.removeEventListener('load',fEnterChangeTab,false);
//         window.addEventListener('load',fEnterChangeTab,false);
//     }
// }
// setEvent_enteredTab();
// ENTERキーでのカーソル移動
$(function(){
    $('body').on('keypress', function(){
        fEnterChangeTab();
    });
});

// ------------------------------
// --
// ------------------------------
