$(function(){
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //------------------------
    // 初期値設定
    //------------------------
    $('#sales_cal_date').val(sales_date);                   //売上日
    $('.tab-contents > li').eq(0).addClass('active');       //得意先選択タブの表示

    if(sale_arrival_type == '1') {
        // 港止め
        $('#sale_arrival_type').val('1');
        $("#delivery_move").hide();                             //配送先入力エリア非表示
    } else {
        // 配送
        $('#sale_arrival_type').val('2');
        $("#delivery_move").show();                             //配送先入力エリア非表示
    }
    

    var clickEventType=((window.ontouchstart!==null)?'click':'touchstart');
//    var clickEventType=((window.ontouchstart!==null)?'click':'touchend');

    /* 得意先～商品選択切替タブ */
    $('.tab-title > li').on(clickEventType, function(e){
        var th = $(this).index();  // 何番目のタブがクリックされたか

        $('.tab-title > li').removeClass('active');
        $('.tab-title > li').eq(th).addClass('active');
        
        $('.tab-contents > li').removeClass('active');
        $('.tab-contents > li').eq(th).addClass('active');
        
        /* 五十音検索のactive化 */
        if(th == 0) {
            var cnt = $('.cust > .kanapager > .kanapager-contents > li').length;
            for (var i = 0; i < cnt; i++) {
                $('.cust > .kanapager > .kanapager-contents > li').eq(i).addClass('active');
            }
        } else if(th == 2) {
            var cnt = $('.place > .kanapager > .kanapager-contents > li').length;
            for (var i = 0; i < cnt; i++) {
                $('.place > .kanapager > .kanapager-contents > li').eq(i).addClass('active');
            }
        } else if(th == 3) {
            var cnt = $('.landing > .kanapager > .kanapager-contents > li').length;
            for (var i = 0; i < cnt; i++) {
                $('.landing > .kanapager > .kanapager-contents > li').eq(i).addClass('active');
            }
        } else if(th == 4) {
            var cnt = $('.item > .kanapager > .kanapager-contents > li').length;
            for (var i = 0; i < cnt; i++) {
                $('.item > .kanapager > .kanapager-contents > li').eq(i).addClass('active');
            }
        }

        /* 波紋エフェクト */
        $('.tab .ripple').remove();  // 前の波紋要素を消す

        var tab_width = $(this).width();
        var tab_height = $(this).height();
        var tab_x = $(this).offset().left;
        var tab_y = $(this).offset().top;

        // 大きい方の幅に合わせる
        var size = tab_width >= tab_height ? tab_width : tab_height;

        // 波紋要素を追加
        $(this).prepend('<div class="ripple"></div>');

        // 中心座標を算出
        var x = e.pageX - tab_x - size / 2;
        var y = e.pageY - tab_y - size / 2;

        $('.ripple').css({
            width: size,
            height: size,
            top: y + 'px',
            left: x + 'px'
        }).addClass('ripple-animation');
    });

    /* 五十音検索切替 */
    $('.cust > .kanapager > .kanapager-contents > li').on(clickEventType, function(e) {
        var th = $(this).index();   // 何番目のカナがクリックされたか
        var kana = $('.cust > .kanapager > .kanapager-contents > li').children('a');
        var first_kana =  kana[1].firstChild.nodeValue;
        var click_kana =  kana[th].firstChild.nodeValue;
        var hidden_name = 'cust_kana_search';
        
        kanapager(th, kana, click_kana, first_kana, hidden_name);
        
        if(click_kana == '・') {
            return;
        }
        
        if (th >= 1 && th <=5) { 
            if(kana[th].className == 'current') {
                var search_kana = click_kana;
            } else {
                var search_kana = '*';
            }

            //得意先カナ検索実行（Loading開始）
            showLoading();
            
            // Ajax呼び出し
            $.ajax({
//                url : location.protocol + '/accept/search/cust/' + search_kana ,
                url : search_cust_path  + '/' + search_kana ,
                async : true,
                type: "get",
                dataType: 'json',
                timeout: 120000,    // 120秒(2分)
                success: function(data, textStatus, jqXHR){
                    //全行を削除
                    $('#cust-info-tbl tr').remove();

                    for (var idx in data) {
                        //行データ作成
                        var tbl_tr = '<tr><td>' 
                                     + data[idx]['customer_name'] + '<br/>'
                                     + data[idx]['prefname'] + data[idx]['cityname'] + data[idx]['address1'] + data[idx]['address2']
                                     + '<input name="customer_name" type="hidden" value="' + data[idx]['customer_name'] + '">'
                                     + '<input name="cust_id" type="hidden" value="' + data[idx]['id'] + '">'
                                     + '</td></tr>';
                        //行データ追加
                        $('#cust-info-tbl').append(tbl_tr);
                    }
                    
                    if(data.length > 0) {
                        // 選択行の背景色を元に戻す
                        var sale_cust_id = $('#sale_cust_id').val();
                        if(sale_cust_id !== '') {
                            $.each($("#cust-info-tbl tr").find('input:hidden[name="cust_id"]'), function() {
                                var cust_id = $(this).val();
                                if(sale_cust_id == cust_id ) {
                                    $(this).parent('tr').children('td').eq(0).addClass('select-row');
                                    return true;
                                }
                            });
                        }
                    }
                    
                    //得意先カナ検索実行（Loading解除）
                    hideLoading();
                    return;
                },
                error: function(jqXHR, textStatus, errorThrown){
                    // 失敗時の処理
                    console.log("ajax error:" + textStatus);
                    //得意先カナ検索実行（Loading解除）
                    hideLoading();
                },
                complete:function(){
                    //得意先カナ検索実行（Loading解除）
                    hideLoading();
                },
            });
            return;
        }
    });
    
    $('.place > .kanapager > .kanapager-contents > li').on(clickEventType, function(e){
        var th = $(this).index();   // 何番目のカナがクリックされたか
        var kana = $('.place > .kanapager > .kanapager-contents > li').children('a');
        var first_kana =  kana[1].firstChild.nodeValue;
        var click_kana =  kana[th].firstChild.nodeValue;
        var hidden_name = 'place_kana_search';
        
        kanapager(th, kana, click_kana, first_kana, hidden_name);
        
        if(click_kana == '・') {
            return;
        }
        
        if (th >= 1 && th <=5) { 
            if(kana[th].className == 'current') {
                var search_kana = click_kana;
            } else {
                var search_kana = '*';
            }

            //発地カナ検索実行（Loading開始）
            showLoading();
        
            // Ajax呼び出し
            $.ajax({
//                url : location.protocol +   '/accept/search/area/' + search_kana ,
                url : search_area_path  + '/' + search_kana ,
                async : true,
                type: "get",
                dataType: 'json',
                timeout: 120000,    // 120秒(2分)
                success: function(data, textStatus, jqXHR){
                    //全行を削除
                    $('#place-info-tbl tr').remove();

                    for (var idx in data) {
                        //行データ作成
                        var tbl_tr = '<tr><td>' 
                                     + data[idx]['area_name'] 
                                     + '<input name="place_name" type="hidden" value="' + data[idx]['area_name_name'] + '">'
                                     + '<input name="place_id" type="hidden" value="' + data[idx]['id'] + '">'
                                     + '</td></tr>';
                        //行データ追加
                        $('#place-info-tbl').append(tbl_tr);
                    }
                    
                    if(data.length > 0) {
                        // 選択行の背景色を元に戻す
                        var sale_place_id = $('#sale_place_id').val();
                        if(sale_place_id !== '') {
                            $.each($("#place-info-tbl tr").find('input:hidden[name="place_id"]'), function() {
                                var place_id = $(this).val();
                                if(sale_place_id == place_id ) {
                                    $(this).parent('tr').children('td').eq(0).addClass('select-row');
                                    return true;
                                }
                            });
                        }
                    }
                    
                    //発地カナ検索実行（Loading解除）
                    hideLoading();
                    return;
                },
                error: function(jqXHR, textStatus, errorThrown){
                    // 失敗時の処理
                    console.log("ajax error:" + textStatus);
                    //発地カナ検索実行（Loading解除）
                    hideLoading();
                },
                complete:function(){
                    //発地カナ検索実行（Loading解除）
                    hideLoading();
                },
            });
            return;
        }
    });

    $('.landing > .kanapager > .kanapager-contents > li').on(clickEventType, function(e){
        var th = $(this).index();   // 何番目のカナがクリックされたか
        var kana = $('.landing > .kanapager > .kanapager-contents > li').children('a');
        var first_kana =  kana[1].firstChild.nodeValue;
        var click_kana =  kana[th].firstChild.nodeValue;
        var hidden_name = 'landing_kana_search';

        kanapager(th, kana, click_kana, first_kana, hidden_name);
        
        if(click_kana == '・') {
            return;
        }

        if (th >= 1 && th <=5) { 
            if(kana[th].className == 'current') {
                var search_kana = click_kana;
            } else {
                var search_kana = '*';
            }

            //着地カナ検索実行（Loading開始）
            showLoading();
        
            // Ajax呼び出し
            $.ajax({
//                url : location.protocol +   '/accept/search/area/' + search_kana ,
                url : search_area_path  + '/' + search_kana ,
                async : true,
                type: "get",
                dataType: 'json',
                timeout: 120000,    // 120秒(2分)
                success: function(data, textStatus, jqXHR){
                    // console.log(data.length);
                    //全行を削除
                    $('#landing-info-tbl tr').remove();

                    for (var idx in data) {
                        //行データ作成
                        var tbl_tr = '<tr><td>' 
                                     + data[idx]['area_name'] 
                                     + '<input name="landing_name" type="hidden" value="' + data[idx]['area_name_name'] + '">'
                                     + '<input name="landing_id" type="hidden" value="' + data[idx]['id'] + '">'
                                     + '</td></tr>';
                        //行データ追加
                        $('#landing-info-tbl').append(tbl_tr);
                    }
                    
                    if(data.length > 0) {
                        // 選択行の背景色を元に戻す
                        var sale_landing_id = $('#sale_landing_id').val();
                        if(sale_landing_id !== '') {
                            $.each($("#landing-info-tbl tr").find('input:hidden[name="landing_id"]'), function() {
                                var landing_id = $(this).val();
                                if(sale_landing_id == landing_id ) {
                                    $(this).parent('tr').children('td').eq(0).addClass('select-row');
                                    return true;
                                }
                            });
                        }
                    }
                    
                    //着地カナ検索実行（Loading解除）
                    hideLoading();
                    return;
                },
                error: function(jqXHR, textStatus, errorThrown){
                    // 失敗時の処理
                    console.log("ajax error:" + textStatus);
                    //着地カナ検索実行（Loading解除）
                    hideLoading();
                },
                complete:function(){
                    //着地カナ検索実行（Loading解除）
                    hideLoading();
                },
            });
            return;
        }
    });

    $('.item > .kanapager > .kanapager-contents > li').on(clickEventType, function(e){
        var th = $(this).index();   // 何番目のカナがクリックされたか
        var kana = $('.item > .kanapager > .kanapager-contents > li').children('a');
        var first_kana =  kana[1].firstChild.nodeValue;
        var click_kana =  kana[th].firstChild.nodeValue;
        var hidden_name = 'item_kana_search';
        
        kanapager(th, kana, click_kana, first_kana, hidden_name);
        
        if(click_kana == '・') {
            return;
        }

        if (th >= 1 && th <=5) { 
            if(kana[th].className == 'current') {
                var search_kana = click_kana;
            } else {
                var search_kana = '*';
            }
        
            //商品カナ検索実行（Loading開始）
            showLoading();

            // Ajax呼び出し
            $.ajax({
//                url : location.protocol +   '/accept/search/item/' + search_kana ,
                url : search_item_path  + '/' + search_kana ,
                async : true,
                type: "get",
                dataType: 'json',
                timeout: 120000,    // 120秒(2分)
                success: function(data, textStatus, jqXHR){
                    // console.log(data.length);
                    //全行を削除
                    $('#item-info-tbl tr').remove();

                    for (var idx in data) {
                        //行データ作成
                        if(data[idx]['unit'] == null || data[idx]['unit'] == 'null') {
                            var unit = "";
                        } else {
                            var unit = data[idx]['unit'];
                        }
                        var tbl_tr = '<tr><td class="row_item_name">' 
                                     + data[idx]['item_name'] 
                                     + '<input name="item_name" type="hidden" value="' + data[idx]['item_name'] + '">'
                                     + '</td><td class="row_item_unit">'
                                     + unit 
                                     + '<input name="item_unit" type="hidden" value="' + unit + '">'
                                     + '</td><td class="row_sea_price">'
                                     + data[idx]['sea_price'] 
                                     + '<input name="item_sea_price" type="hidden" value="' + data[idx]['sea_price'] + '">'
                                     + '</td><td class="row_land_price">'
                                     + data[idx]['land_price'] 
                                     + '<input name="item_land_price" type="hidden" value="' + data[idx]['land_price'] + '">'
                                     + '</td>'
                                     + '<input name="item_id" type="hidden" value="' + data[idx]['id'] + '">'
                                     + '<input name="item_category_id" type="hidden" value="' + data[idx]['category_id'] + '">'
                                     + '</tr>';
                        //行データ追加
                        $('#item-info-tbl').append(tbl_tr);
                    }
                    
                    if(data.length > 0) {
                        // 選択行の背景色を元に戻す
                        var num = $('#sale_items_group').find('div.sale_items').length;
                        if (num >= 1) {
                            for (i = 0; i < num; i++) {
                                var sale_item_id = $('div.sale_items').eq(i).find('input:hidden[name="sale_item_id[]"]').val();
                                $.each($("#item-info-tbl tr").find('input:hidden[name="item_id"]'), function() {
                                    var item_id = $(this).val();
                                    if (item_id == sale_item_id) {
                                        var col =$(this).parent('tr').children('td').length;
                                        for (j = 0; j < col; j++) {
                                            $(this).parent('tr').children('td').eq(j).addClass('select-row');
                                        }
                                        return true;
                                    }
                                });
                            }
                        }
                    }
                    
                    //商品カナ検索実行（Loading解除）
                    hideLoading();
                    return;
                },
                error: function(jqXHR, textStatus, errorThrown){
                    // 失敗時の処理
                    console.log("ajax error:" + textStatus);
                    //商品カナ検索実行（Loading解除）
                    hideLoading();
                },
                complete:function(){
                    //商品カナ検索実行（Loading解除）
                    hideLoading();
                },
            });
            return;
        }
    });
    
    function kanapager(th, kana, click_kana, first_kana, hidden_name) {
        var kana_a = ['ア', 'イ', 'ウ', 'エ', 'オ'];
        var kana_k = ['カ', 'キ', 'ク', 'ケ', 'コ'];
        var kana_s = ['サ', 'シ', 'ス', 'セ', 'ソ'];
        var kana_t = ['タ', 'チ', 'ツ', 'テ', 'ト'];
        var kana_n = ['ナ', 'ニ', 'ヌ', 'ネ', 'ノ'];
        var kana_h = ['ハ', 'ヒ', 'フ', 'ヘ', 'ホ'];
        var kana_m = ['マ', 'ミ', 'ム', 'メ', 'モ'];
        var kana_y = ['ヤ', '・', 'ユ', '・', 'ヨ'];
        var kana_r = ['ラ', 'リ', 'ル', 'レ', 'ロ'];
        var kana_w = ['ワ', '・', 'ヲ', '・', 'ン'];
        
        var hidden_select_kana_search = 'input:hidden[name="' + hidden_name + '"]';
        var hidden_select_kana_search_id = 'input:hidden[name="' + hidden_name + '_id"]';
        var select_kana_search = $(hidden_select_kana_search).val();
        var select_kana_search_id = $(hidden_select_kana_search_id).val();

        if (th == 0) {
            if (first_kana == kana_k['0']) {
                for (var i = 1; i < 6; i++) {
                    // ア行
                     kana[i].firstChild.nodeValue = kana_a[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_a[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_s['0']) {
                for (var i = 1; i < 6; i++) {
                    // カ行
                     kana[i].firstChild.nodeValue = kana_k[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_k[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_t['0']) {
                for (var i = 1; i < 6; i++) {
                    // サ行
                     kana[i].firstChild.nodeValue = kana_s[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_s[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_n['0']) {
                for (var i = 1; i < 6; i++) {
                    // タ行
                     kana[i].firstChild.nodeValue = kana_t[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_t[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_h['0']) {
                for (var i = 1; i < 6; i++) {
                    // ナ行
                     kana[i].firstChild.nodeValue = kana_n[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_n[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_m['0']) {
                for (var i = 1; i < 6; i++) {
                    // ハ行
                     kana[i].firstChild.nodeValue = kana_h[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_h[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_y['0']) {
                for (var i = 1; i < 6; i++) {
                    // マ行
                     kana[i].firstChild.nodeValue = kana_m[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_m[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_r['0']) {
                for (var i = 1; i < 6; i++) {
                    // ヤ行
                     kana[i].firstChild.nodeValue = kana_y[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_y[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_w['0']) {
                for (var i = 1; i < 6; i++) {
                    // ラ行
                     kana[i].firstChild.nodeValue = kana_r[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_r[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            }
        } else if(th == 6) {
            if(first_kana == kana_a['0']) {
                for (var i = 1; i < 6; i++) {
                    // カ行
                     kana[i].firstChild.nodeValue = kana_k[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_k[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_k['0']) {
                for (var i = 1; i < 6; i++) {
                    // サ行
                     kana[i].firstChild.nodeValue = kana_s[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_s[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_s['0']) {
                for (var i = 1; i < 6; i++) {
                    // タ行
                     kana[i].firstChild.nodeValue = kana_t[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_t[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_t['0']) {
                for (var i = 1; i < 6; i++) {
                    // ナ行
                     kana[i].firstChild.nodeValue = kana_n[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_n[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_n['0']) {
                for (var i = 1; i < 6; i++) {
                    // ハ行
                     kana[i].firstChild.nodeValue = kana_h[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_h[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_h['0']) {
                for (var i = 1; i < 6; i++) {
                    // マ行
                     kana[i].firstChild.nodeValue = kana_m[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_m[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_m['0']) {
                for (var i = 1; i < 6; i++) {
                    // ヤ行
                     kana[i].firstChild.nodeValue = kana_y[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_y[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_y['0']) {
                for (var i = 1; i < 6; i++) {
                    // ラ行
                     kana[i].firstChild.nodeValue = kana_r[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_r[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            } else if (first_kana == kana_r['0']) {
                for (var i = 1; i < 6; i++) {
                    // ワ行
                     kana[i].firstChild.nodeValue = kana_w[i-1];
                     if (select_kana_search_id == i) {
                        if (select_kana_search ==  kana_w[i-1]) {
                            kana.eq(i).addClass('current');
                        } else {
                            kana.eq(i).removeClass('current');
                        }
                     }
                }
            }
        } else {
            if(select_kana_search == click_kana) {
                for (i = 0; i < 7; i++) {
                    kana.eq(i).removeClass('current');
//                    kana.eq(i).css('background-color','#eee');
                    kana.eq(i).off('mouseenter mouseleave');
                    kana.eq(i).blur();
                }
                $(hidden_select_kana_search).val("");
                $(hidden_select_kana_search_id).val("");
            } else {
                for (i = 0; i < 7; i++) {
                    kana.eq(i).removeClass('current');
                }
                kana.eq(th).addClass('current');
                $(hidden_select_kana_search).val(click_kana);
                $(hidden_select_kana_search_id).val(th);
            }
        }
    }
    
    // 得意先データ行の選択
    $(document).on(clickEventType, "#cust-info-tbl tbody tr td", function () {
        var customer_id = $(this).find('input:hidden[name="cust_id"]').val();
        var customer_name = $(this).find('input:hidden[name="customer_name"]').val();
        var sale_cust_id = $('input:hidden[name="sale_cust_id"]').val();

        $('input:hidden[name="select_cust_id"]').val(customer_id);
        $('input:hidden[name="select_customer_name"]').val(customer_name);
        
        $('#cust-info-tbl td').removeClass('select-row');
        $(this).addClass('select-row');

        if(customer_id != sale_cust_id) {
            $('#cust-dept-tbl td').removeClass('select-row');
            $('#sale_cust_dept_name').text('');
            $('input:hidden[name="sale_cust_dept_id"]').val('');
            $('input:hidden[name="sale_cust_dept_nm"]').val('');

            // 得意先部門タブ情報の取得（Ajax呼び出し）
            $.ajax({
//                url : location.protocol + '/accept/custdpt/' + customer_id ,
                url : tab_custdpt_path  + '/' + customer_id ,
                async : true,
                type: "get",
                dataType: 'json',
                timeout: 120000,    // 120秒(2分)
                success: function(data, textStatus, jqXHR){
                    //全行を削除
                    $('#cust-dept-tbl tr').remove();

                    for (var idx in data) {
                        //行データ作成
                        var tbl_tr = '<tr><td>' 
                                     + data[idx]['cust_dept_name']
                                     + '<input name="cust_dept_id" type="hidden" value="' + data[idx]['id'] + '">'
                                     + '</td></tr>';
                        //行データ追加
                        $('#cust-dept-tbl').append(tbl_tr);
                    }
                    
                    if(data.length > 0) {
                        // 選択行の背景色を元に戻す
                        var sale_cust_dept_id = $('#sale_cust_dept_id').val();
                        if(sale_cust_dept_id !== '') {
                            $.each($("#cust-dept-tbl tr").find('input:hidden[name="cust_dept_id"]'), function() {
                                var cust_dept_id = $(this).val();
                                if(sale_cust_dept_id == cust_dept_id) {
                                    $(this).parent('tr').children('td').eq(0).addClass('select-row');
                                    return true;
                                }
                            });
                        }
                    }
                    return;
                },
                error: function(jqXHR, textStatus, errorThrown){
                    // 失敗時の処理
                    console.log("ajax error:" + textStatus);
                }
            });
            
            // 商品タブ情報の取得
            $.ajax({
//                url : location.protocol + '/accept/item/' + customer_id ,
                url : tab_item_path + '/' + customer_id ,
                async : true,
                type: "get",
                dataType: 'json',
                timeout: 120000,    // 120秒(2分)
                success: function(data, textStatus, jqXHR){
                    //全行を削除
                    $('#item-info-tbl tr').remove();

                    for (var idx in data) {
                        var sea_price = data[idx]['sea_price'];
                        var land_price = data[idx]['land_price'];
                        var str_sea_price = sea_price.toString().replace(/(\d)(?=(\d{3})+$)/g , '$1,');
                        var str_land_price = land_price.toString().replace(/(\d)(?=(\d{3})+$)/g , '$1,');
                    
                        //行データ作成
                        var tbl_tr = '<tr>'
                                     + '<input name="item_name" type="hidden" value="' + data[idx]['item_name'] + '">'
                                     + '<input name="item_unit" type="hidden" value="' + data[idx]['unit'] + '">'
                                     + '<input name="item_sea_price" type="hidden" value="' + data[idx]['sea_price'] + '">'
                                     + '<input name="item_land_price" type="hidden" value="' + data[idx]['land_price'] + '">'
                                     + '<input name="item_category_id" type="hidden" value="' + data[idx]['category_id'] + '">'
                                     + '<input name="item_id" type="hidden" value="' + data[idx]['id'] + '">'
                                     + '<td class="row_item_name">' + data[idx]['item_name'] + '</td>'
                                     + '<td class="row_item_unit">' + data[idx]['unit'] + '</td>'
                                     + '<td class="row_sea_price">' + '\xA5' + str_sea_price + '</td>'
                                     + '<td class="row_land_price">' + '\xA5' + str_land_price + '</td>'
                                     + '</tr>';
                                     
                        //行データ追加
                        $('#item-info-tbl').append(tbl_tr);
                    }
                    
                    if(data.length > 0) {
                        // 選択行の背景色を元に戻す
                        $.each($("div.sale_items").find('input:hidden[name="sale_item_id[]"]'), function() {
                            var item_id = $(this).val();
                            $.each($("#item-info-tbl tr").find('input:hidden[name="item_id"]'), function() {
                                var target_id = $(this).val();
                                if (item_id == target_id) {
                                    var col =$(this).parent('tr').children('td').length;
                                    for (i = 0; i < col; i++) {
                                        $(this).parent('tr').children('td').eq(i).removeClass('select-row');
                                    }
                                    return true;
                                }
                            });
                        });
                    }
                    return;
                },
                error: function(jqXHR, textStatus, errorThrown){
                    // 失敗時の処理
                    console.log("ajax error:" + textStatus);
                }
            });
            
        }
        
    });

    // 得意先の選択ボタン押下
    $('#cust_select').on(clickEventType, function(){
        var id = $('input:hidden[name="select_cust_id"]').val();
        var nm = $('input:hidden[name="select_customer_name"]').val();

        if(id === '' || id === null) {
            $('#errmsgModal').on('show.bs.modal', function (event) {
                var msg = '<ul><li>得意先が未選択です。</li><li>選択してください。</li></ul>';
                $('#modal-errmsg').html(msg);
            });
            $('#errmsgModal').modal();
        } else {
            $('#sale_cust_name').text(nm);
            $('input:hidden[name="sale_cust_id"]').val(id);
            $('input:hidden[name="sale_cust_nm"]').val(nm);
            var cnt = $("#cust-dept-tbl tbody").children().length;
            if(cnt > 0) {
                // 得意先部門タブに切り替え
                $('.tab-title > li').eq(1).trigger(clickEventType);
            } else {
                // 発地タブに切り替え
                $('.tab-title > li').eq(2).trigger(clickEventType);
            }
        }
    })

    // 得意先部門データ行の選択
    $(document).on(clickEventType, "#cust-dept-tbl tbody td", function () {
        var cust_dept_id = $(this).find('input:hidden[name="cust_dept_id"]').val();
        var cust_dept_name = $(this).text();

        $('input:hidden[name="select_cust_dept_id"]').val(cust_dept_id);
        $('input:hidden[name="select_cust_dept_nm"]').val(cust_dept_name);
        
        $('#cust-dept-tbl td').removeClass('select-row');
        $(this).addClass('select-row');
    });

    // 得意先部門 新規登録 得意先SelectBoxの初期選択
    $('#custDeptAddModal').on('show.bs.modal', function (event) {
        // 得意先部門新規登録の得意先SelectBoxの初期選択を設定
        var id = $('input:hidden[name="select_cust_id"]').val();
        $('#m_cust_dept_customer_name').val(id);
    });

    // 得意先部門の選択ボタン押下
    $('#cust_dept_select').on(clickEventType, function(){
        var id = $('input:hidden[name="select_cust_dept_id"]').val();
        var nm = $('input:hidden[name="select_cust_dept_nm"]').val();

        if(id === '' || id === null) {
            $('#errmsgModal').on('show.bs.modal', function (event) {
                var msg = '<ul><li>得意先部門が未選択です。</li><li>選択してください。</li></ul>';
                $('#modal-errmsg').html(msg);
            });
            $('#errmsgModal').modal();
        } else {
            $('#sale_cust_dept_name').text(nm);
            $('input:hidden[name="sale_cust_dept_id"]').val(id);
            $('input:hidden[name="sale_cust_dept_nm"]').val(nm);
            $('.tab-title > li').eq(2).trigger(clickEventType);
        }
    })

    // 発地データ行の選択
    $(document).on(clickEventType, "#place-info-tbl tbody td", function () {
        var place_id = $(this).find('input:hidden[name="place_id"]').val();
        var place_name = $(this).text();
        var sale_landing_id = $('#sale_landing_id').val();
        
        if(sale_landing_id !== place_id) {
            $('input:hidden[name="select_place_id"]').val(place_id);
            $('input:hidden[name="select_place_name"]').val(place_name);
            $('#place-info-tbl td').removeClass('select-row');
            $(this).addClass('select-row');
        }
    });

    // 発地の選択ボタン押下
    $('#place_select').on(clickEventType, function(){
        var id = $('input:hidden[name="select_place_id"]').val();
        var nm = $('input:hidden[name="select_place_name"]').val();

        if(id === '' || id === null) {
            $('#errmsgModal').on('show.bs.modal', function (event) {
                var msg = '<ul><li>発地が未選択です。</li><li>選択してください。</li></ul>';
                $('#modal-errmsg').html(msg);
            });
            $('#errmsgModal').modal();
        } else {
            $('#sale_place_name').text(nm);
            $('input:hidden[name="sale_place_id"]').val(id);
            $('input:hidden[name="sale_place_nm"]').val(nm);
            $('.tab-title > li').eq(3).trigger(clickEventType);
        }
    })

    //着地 配送ルール 港止め選択
    $('#lbl_arrival_type_stop').on(clickEventType, function(){
        var delivery_rule = $(this).text();
        var delivery_type = $('arrival_type_stop').val();
        
        //$('#sale_delivery_rule').text(delivery_rule);
        $('input:hidden[name="sale_arrival_type"]').val(delivery_type);
        
        // 配送先入力エリア 非表示
        $("#delivery_move").hide();
    })

    //着地 配送ルール 配送選択
    $('#lbl_arrival_type_move').on(clickEventType, function(){
        var delivery_rule = $(this).text();
        var delivery_type = $('#arrival_type_move').val();
        
        //$('#sale_delivery_rule').text(delivery_rule);
        $('input:hidden[name="sale_arrival_type"]').val(delivery_type);
        
        // 配送先入力エリア 表示
        $("#delivery_move").show();
    })

    // 着地データ行の選択
    $(document).on(clickEventType, "#landing-info-tbl tbody td", function () {
        var landing_id = $(this).find('input:hidden[name="landing_id"]').val();
        var landing_name = $(this).text();
        var sale_place_id = $('#sale_place_id').val();
        
        if(sale_place_id !== landing_id) {
            $('input:hidden[name="select_landing_id"]').val(landing_id);
            $('input:hidden[name="select_landing_name"]').val(landing_name);
            
            $('#landing-info-tbl td').removeClass('select-row');
            $(this).addClass('select-row');
        }
    });

    // 着地の選択ボタン押下
    $('#landing_select').on(clickEventType, function(){
        var id = $('input:hidden[name="select_landing_id"]').val();
        var nm = $('input:hidden[name="select_landing_name"]').val();

        if(id === '' || id === null) {
            $('#errmsgModal').on('show.bs.modal', function (event) {
                var msg = '<ul><li>着地が未選択です。</li><li>選択してください。</li></ul>';
                $('#modal-errmsg').html(msg);
            });
            $('#errmsgModal').modal();
        } else {
            $('#sale_landing_name').text(nm);
            $('input:hidden[name="sale_landing_id"]').val(id);
            $('input:hidden[name="sale_landing_nm"]').val(nm);
            $('.tab-title > li').eq(4).trigger(clickEventType);
        }
    })

    // 商品行の選択
    $(document).on(clickEventType, "#item-info-tbl tbody td", function () {
        if(this.cellIndex > 0) {
            return;
        }
        var i;
        var item_id = $(this).closest('tr').find('input:hidden[name="item_id"]').val();
        var item_name = $(this).closest('tr').find('input:hidden[name="item_name"]').val();
        var item_unit = $(this).closest('tr').find('input:hidden[name="item_unit"]').val();
        var item_sea_price = $(this).closest('tr').find('input:hidden[name="item_sea_price"]').val();
        var item_land_price = $(this).closest('tr').find('input:hidden[name="item_land_price"]').val();
        var item_category_id = $(this).closest('tr').find('input:hidden[name="item_category_id"]').val();

        var select_item_id = $('input:hidden[name="select_item_id"]').val();
        var select_item_name = $('input:hidden[name="select_item_name"]').val();
        var select_item_unit = $('input:hidden[name="select_item_unit"]').val();
        var select_item_sea_price = $('input:hidden[name="select_item_sea_price"]').val();
        var select_item_land_price = $('input:hidden[name="select_item_land_price"]').val();
        var select_item_category_id = $('input:hidden[name="select_item_category_id"]').val();
        
        if($(this).hasClass('select-row')) {
            var array_item_id = select_item_id.split(',');
            var array_item_name = select_item_name.split(',');
            var array_unit = select_item_unit.split(',');
            var array_sea_price = select_item_sea_price.split(',');
            var array_land_price = select_item_land_price.split(',');
            var array_category_id = select_item_category_id.split(',');
            
            var num = $('#sale_items_group').find('div.sale_items').length;
            var select_item_flg = true;
            
            if (num >= 1) {
                for (i = 0; i < num; i++) {
                    sale_item_id = $('div.sale_items').eq(i).find('input:hidden[name="sale_item_id[]"]').val();
                    if(sale_item_id == item_id) {
                        // 商品選択欄に同一商品が存在する場合は、選択行の背景色はクリアしない
                        select_item_flg = false;
                        return;
                    }
                }
            }
            
            if(select_item_flg) {
                var del_idx = $.inArray(item_id, array_item_id);
                if(del_idx > -1) {
                    array_item_id.splice(del_idx, 1);
                    $('input:hidden[name="select_item_id"]').val(array_item_id.join(','));
                }

                var del_idx = $.inArray(item_name, array_item_name);
                if(del_idx > -1) {
                    array_item_name.splice(del_idx, 1);
                    $('input:hidden[name="select_item_name"]').val(array_item_name.join(','));
                }

                var del_idx = $.inArray(item_unit, array_unit);
                if(del_idx > -1) {
                    array_unit.splice(del_idx, 1);
                    $('input:hidden[name="select_item_unit"]').val(array_unit.join(','));
                }

                var del_idx = $.inArray(item_sea_price, array_sea_price);
                if(del_idx > -1) {
                    array_sea_price.splice(del_idx, 1);
                    $('input:hidden[name="select_item_sea_price"]').val(array_sea_price.join(','));
                }

                var del_idx = $.inArray(item_land_price, array_land_price);
                if(del_idx > -1) {
                    array_land_price.splice(del_idx, 1);
                    $('input:hidden[name="select_item_land_price"]').val(array_land_price.join(','));
                }

                var del_idx = $.inArray(item_category_id, array_category_id);
                if(del_idx > -1) {
                    array_category_id.splice(del_idx, 1);
                    $('input:hidden[name="select_item_category_id"]').val(array_category_id.join(','));
                }
            
                // 商品選択欄に同一商品が存在しない場合は、選択行の背景色をクリア
                var col =$(this).closest('tr').children('td').length;
                for (i = 0; i < col; i++) {
                    $(this).closest('tr').children('td').eq(i).removeClass('select-row');
                }
            }
        } else {
            if(select_item_id) {
                $('input:hidden[name="select_item_id"]').val(select_item_id + ',' + item_id);
            } else {
                $('input:hidden[name="select_item_id"]').val(item_id);
            }

            if(select_item_name) {
                $('input:hidden[name="select_item_name"]').val(select_item_name + ',' + item_name);
            } else {
                $('input:hidden[name="select_item_name"]').val(item_name);
            }

            if(select_item_unit) {
                $('input:hidden[name="select_item_unit"]').val(select_item_unit + ',' + item_unit);
            } else {
                $('input:hidden[name="select_item_unit"]').val(item_unit);
            }

            if(select_item_sea_price) {
                $('input:hidden[name="select_item_sea_price"]').val(select_item_sea_price + ',' + item_sea_price);
            } else {
                $('input:hidden[name="select_item_sea_price"]').val(item_sea_price);
            }

            if(select_item_land_price) {
                $('input:hidden[name="select_item_land_price"]').val(select_item_land_price + ',' + item_land_price);
            } else {
                $('input:hidden[name="select_item_land_price"]').val(item_land_price);
            }

            if(select_item_category_id) {
                $('input:hidden[name="select_item_category_id"]').val(select_item_category_id + ',' + item_category_id);
            } else {
                $('input:hidden[name="select_item_category_id"]').val(item_category_id);
            }

            // 選択行の背景色を変更
            var col =$(this).closest('tr').children('td').length;
            for (i = 0; i < col; i++) {
                $(this).closest('tr').children('td').eq(i).addClass('select-row');
            }
        }
    })

    // 商品の選択ボタン押下
    $('#item_select').on(clickEventType, function(){
        var i;
        var sale_item_id;

        var select_item_id = $('input:hidden[name="select_item_id"]').val();
        var select_item_name = $('input:hidden[name="select_item_name"]').val();
        var select_item_unit = $('input:hidden[name="select_item_unit"]').val();
        var select_item_sea_price = $('input:hidden[name="select_item_sea_price"]').val();
        var select_item_land_price = $('input:hidden[name="select_item_land_price"]').val();
        var select_item_category_id = $('input:hidden[name="select_item_category_id"]').val();
        
        var array_item_id = select_item_id.split(',');
        var array_item_name = select_item_name.split(',');
        var array_unit = select_item_unit.split(',');
        var array_sea_price = select_item_sea_price.split(',');
        var array_land_price = select_item_land_price.split(',');
        var array_category_id = select_item_category_id.split(',');

        if (array_item_id[0] == '') {
            $('#errmsgModal').on('show.bs.modal', function (event) {
                var msg = '<ul><li>商品が未選択です。</li><li>選択してください。</li></ul>';
                $('#modal-errmsg').html(msg);
            });
            $('#errmsgModal').modal();
            return;
        }

        $.each(array_item_id, function(idx, val){
            var num = $('#sale_items_group').find('div.sale_items').length;
            var add_item_flg = true;
            
            if (num >= 0) {
                for (i = 0; i < num; i++) {
                    sale_item_id = $('div.sale_items').eq(i).find('input:hidden[name="sale_item_id[]"]').val();
                    if(sale_item_id == val) {
                        // 商品欄に同一商品が存在する場合は、追加しない
                        add_item_flg = false;
                    }
                }
            }
            
            if(add_item_flg) {
                // 商品欄に同一商品が存在しない場合は、追加する
                var item_id = array_item_id[idx];
                var item_name = array_item_name[idx];

                if(array_unit.length > idx) {
                    var item_unit = array_unit[idx];
                } else {
                    var item_unit = '';
                }
                
                var item_sea_price = array_sea_price[idx];
                var item_land_price = array_land_price[idx];

                if(array_category_id.length > idx) {
                    var item_category_id = array_category_id[idx];
                } else {
                    var item_category_id = '';
                }

                // 商品欄の作成
                var div_item = '<div class="sale_item_block form-group row">' 
                             + '<div class="sale_items">'
                             
                             + '<div class="input-group sales_card_form_group">'
                             + '<div class="col-md-3">'
                             + '<label class="text-head">商品</label>'
                             + '</div>'
                             + '<div class="col-md-8">'
                             + '<p name="sale_item_name[]">' + item_name + '</p>'
                             + '</div>'
                             + '<div class="item-close">'
                             + '<a href="#"><i class="fa fa-times item-close-icon" aria-hidden="true"></i></a>'
                             + '</div>'
                             + '</div>'
                             
                             + '<div class="input-group sales_card_form_group">'
                             + '<div class="col-md-3">'
                             + '<label class="text-head">数量</label>'
                             + '</div>'
                             + '<div class="col-md-3">'
                             + '<input type="number" class="form-control input-sm item_quantity" name="item_quantity[]" maxlength="10" value=1 onfocus="this.select();">'
                             + '</div>'
                             + '<div class="col-md-3">' + select_unit + '</div>'
                             + '</div>'
                             
                             + '<div class="input-group sales_card_form_group">'
                             + '<div class="col-md-3">'
                             + '<label class="text-head">海上運賃</label>'
                             + '</div>'
                             + '<div class="col-md-4 col-md-offset-5 ">'
                             + '<input type="number" class="form-control input-sm sea_price" name="sea_price[]" maxlength="10" value=' + item_sea_price + ' onfocus="this.select();">'
                             + '</div>'
                             + '</div>'
                             
                             + '<div class="input-group sales_card_form_group">'
                             + '<div class="col-md-3">'
                             + '<label class="text-head">陸上運賃</label>'
                             + '</div>'
                             + '<div class="col-md-4 col-md-offset-5 ">'
                             + '<input type="number" class="form-control input-sm land_price" name="land_price[]" maxlength="10" value=' + item_land_price + ' onfocus="this.select();">'
                             + '</div>'
                             + '</div>'
                             
                             + '<input name="sale_item_id[]" type="hidden" value="' + item_id + '">'
                             + '<input name="sale_item_name[]" type="hidden" value="' + item_name + '">'
                             + '<input name="sale_item_category_id[]" type="hidden" value="' + item_category_id + '">'
                             + '</div>'
                             + '</div>';
                             
                // 商品欄の追加
                $('#sale_items_group').append(div_item);

                // 単位の初期選択
                $.each($('div.sale_items').eq(num).find('[name="sale_item_unit[]"]'), function() {
                    var sel = $(this).children();
                    for (var i = 0; i < sel.length; i++) {
                        var unit_item = sel.eq(i).val();
                        if(sel.eq(i).val() == item_unit) {
                            $(this).val(item_unit);
                            return true;
                        }
                    }
                });
                
                // 商品点数
                var item_num = num + 1;
                $('#sales_items').text(item_num + '点');
                $('#sales_item_cnt').val(item_num);

                // 合計額＆消費税計算
                calcTotal();
            }
        });

        return;
    });

    // 商品削除
    $(document).on(clickEventType, "div.item-close", function () {
        var target_id = $(this).parent('div.sales_card_form_group').parent('div.sale_items').find('input:hidden[name="sale_item_id[]"]').val();
        var target_nm = $(this).parent('div.sales_card_form_group').parent('div.sale_items').find('input:hidden[name="sale_item_name[]"]').val();
        var target_unit = $(this).parent('div.sales_card_form_group').parent('div.sale_items').find('input:hidden[name="sale_item_unit[]"]').val();
        var target_sea_price = $(this).parent('div.sales_card_form_group').parent('div.sale_items').find('input:hidden[name="sea_price[]"]').val();
        var target_land_price = $(this).parent('div.sales_card_form_group').parent('div.sale_items').find('input:hidden[name="land_price[]"]').val();
        var target_category_id = $(this).parent('div.sales_card_form_group').parent('div.sale_items').find('input:hidden[name="sale_item_category_id[]"]').val();
        
        // 選択行の背景色を元に戻す
        $.each($("#item-info-tbl tr").find('input:hidden[name="item_id"]'), function() {
            var item_id = $(this).val();
            if (item_id == target_id) {
                var col =$(this).parent('tr').children('td').length;
                for (i = 0; i < col; i++) {
                    $(this).parent('tr').children('td').eq(i).removeClass('select-row');
                }
                return true;
            }
        });
        
        // 選択行のhidden配列から該当値を削除する
        var del_idx;
        
        var select_item_id = $('input:hidden[name="select_item_id"]').val();
        var select_item_name = $('input:hidden[name="select_item_name"]').val();
        var select_item_unit = $('input:hidden[name="select_item_unit"]').val();
        var select_item_sea_price = $('input:hidden[name="select_item_sea_price"]').val();
        var select_item_land_price = $('input:hidden[name="select_item_land_price"]').val();
        var select_item_category_id = $('input:hidden[name="select_item_category_id"]').val();
        
        var array_item_id = select_item_id.split(',');
        var array_item_name = select_item_name.split(',');
        var array_unit = select_item_unit.split(',');
        var array_sea_price = select_item_sea_price.split(',');
        var array_land_price = select_item_land_price.split(',');
        var array_category_id = select_item_category_id.split(',');
        
        del_idx = $.inArray(target_id, array_item_id);
        if(del_idx >= 0) {
            array_item_id.splice(del_idx, 1);
        }

        del_idx = $.inArray(target_nm, array_item_name);
        if(del_idx >= 0) {
            array_item_name.splice(del_idx, 1);
        }

        del_idx = $.inArray(target_unit, array_unit);
        if(del_idx >= 0) {
            array_unit.splice(del_idx, 1);
        }

        del_idx = $.inArray(target_sea_price, array_sea_price);
        if(del_idx >= 0) {
            array_sea_price.splice(del_idx, 1);
        }

        del_idx = $.inArray(target_land_price, array_land_price);
        if(del_idx >= 0) {
            array_land_price.splice(del_idx, 1);
        }

        del_idx = $.inArray(target_category_id, array_category_id);
        if(del_idx >= 0) {
            array_category_id.splice(del_idx, 1);
        }

        $('input:hidden[name="select_item_id"]').val(array_item_id.join(','));
        $('input:hidden[name="select_item_name"]').val(array_item_name.join(','));
        $('input:hidden[name="select_item_unit"]').val(array_unit.join(','));
        $('input:hidden[name="select_item_sea_price"]').val(array_sea_price.join(','));
        $('input:hidden[name="select_item_land_price"]').val(array_land_price.join(','));
        $('input:hidden[name="select_item_category_id"]').val(array_category_id.join(','));

        // 選択商品を削除
        $(this).parent('div.sales_card_form_group').parent('div.sale_items').parent('div.sale_item_block').remove();
        
        // 商品点数
        var num = $('#sale_items_group').find('div.sale_items').length;
        $('#sales_items').text(num + '点');
        $('#sales_item_cnt').val(num);
        
        // 合計額＆消費税計算
        calcTotal();
        
        return;
    });

    // 商品数量 入力制限
    $(document).on('input', 'input[name="item_quantity[]"]', function () {
        // 半角変換
        var halfVal = $(this).val().replace(/[！-～]/g,
            function (tmpStr) {
                // 文字コードをシフト
                return String.fromCharCode(tmpStr.charCodeAt(0) - 0xFEE0);
            }
        );
        // 数値（0～9、マイナス、小数点）以外の不要な文字を削除
        $(this).val(halfVal.replace(/[^\d-.]/g, ''));
    });

    // 海上運賃 & 陸上運賃 入力制限
    $(document).on('input', 'input[name="sea_price[]"]', 'input[name="land_price[]"]', function () {
        // 半角変換
        var halfVal = $(this).val().replace(/[！-～]/g,
            function (tmpStr) {
                // 文字コードをシフト
                return String.fromCharCode(tmpStr.charCodeAt(0) - 0xFEE0);
            }
        );
        // 数値以外の不要な文字を削除
        $(this).val(halfVal.replace(/[^0-9]/g, ''));
    });

    // 数量入力
    $(document).on('keyup', 'input[name="item_quantity[]"]', function () {
        calcTotal();
	});

    // 数量のfocusが外れたら
    $(document).on('blur', 'input[name="item_quantity[]"]', function () {
        if($(this).val() == "") {
            $(this).val(0.00);
        }
	});

    // 海上運賃入力
    $(document).on('keyup', 'input[name="sea_price[]"]', function () {
        calcTotal();
	});
	
    // 海上運賃のfocusが外れたら
    $(document).on('blur', 'input[name="sea_price[]"]', function () {
        if($(this).val() == "") {
            $(this).val(0);
        }
	});

    // 海上運賃 or 陸上運賃入力
    $(document).on('keyup', 'input[name="land_price[]"]', function () {
        calcTotal();
	});

    // 陸上運賃のfocusが外れたら
    $(document).on('blur', 'input[name="land_price[]"]', function () {
        if($(this).val() == "") {
            $(this).val(0);
        }
	});

    /***
    *
    *   海上運賃 = 商品毎の（数量×単価（海上運賃））の集計
    *   陸上運賃 = 商品毎の（数量×単価（陸上運賃））の集計
    *   小計（税抜合計）= 陸上運賃＋海上運賃
    *   消費税 = 小計×税率（四捨五入）
    *   合計金額（税込合計）= 小計（税抜合計）＋消費税
    *
    ****/
	function calcTotal() {
	    var item_quantity = 0;
	    var sea_price = 0;
	    var land_price = 0;
	    var subSeaTotal = 0;
	    var subLandTotal = 0;

        var num = $('#sale_items_group').find('div.sale_item_block').length;

        if (num >= 1) {
            for (i = 0; i < num; i++) {
                item_quantity = $('input[name="item_quantity[]"]').eq(i).val();
                sea_price = $('input[name="sea_price[]"]').eq(i).val();
                land_price = $('input[name="land_price[]"]').eq(i).val();
                
                if(item_quantity == "") {
                    item_quantity = 0;
                } else {
                    item_quantity = parseFloat(item_quantity);
                }

                if(sea_price == "") {
                    sea_price = 0;
                } else {
                    sea_price = parseFloat(sea_price);
                }
                
                if(land_price == "") {
                    land_price = 0;
                } else {
                    land_price = parseFloat(land_price);
                }

                // 海上運賃 = 商品毎の（数量×単価（海上運賃））の集計
                subSeaTotal = subSeaTotal + Math.round((sea_price * item_quantity));
                // 陸上運賃 = 商品毎の（数量×単価（陸上運賃））の集計
                subLandTotal = subLandTotal + Math.round((land_price * item_quantity));
            }
        }

        // 海上運賃の小計（税抜合計）
        $('#sales_sea_amount_total').text('\xA5' + subSeaTotal.toString().replace(/(\d)(?=(\d{3})+$)/g , '$1,'));
        $('#sea_amount').val(subSeaTotal);
        
        // 陸上運賃の小計（税抜合計）
        $('#sales_land_amount_total').text('\xA5' + subLandTotal.toString().replace(/(\d)(?=(\d{3})+$)/g , '$1,'));
        $('#land_amount').val(subLandTotal);
        
        // 小計（税抜合計 = 陸上運賃＋海上運賃
        var total =  parseFloat(subSeaTotal) +  parseFloat(subLandTotal);
        $('#net_amount').val(total);

        // 消費税
//        var total_tax = Math.round( parseFloat(total) *  parseFloat(tax_rate));   //（四捨五入）
        var total_tax = Math.round(calctax(total));       //DBの税率を参照して税額を求める
        var str_sales_tax = total_tax.toString().replace(/(\d)(?=(\d{3})+$)/g , '$1,');
        $('#sales_tax').text('\xA5' + str_sales_tax);
        $('#tax_amount').val(total_tax);
        
        // 合計金額（税込合計）= 小計（税抜合計）＋消費税
//        var sales_total = parseFloat(total) + parseFloat(total_tax);
        var sales_total = Math.round(calcamount(total));  //税込額を求める
        var str_sales_total = sales_total.toString().replace(/(\d)(?=(\d{3})+$)/g , '$1,');
        $('#sales_total').text('\xA5' + str_sales_total);
        $('#total_amount').val(sales_total);
        
    }

});