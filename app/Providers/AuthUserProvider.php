<?php

namespace partsmanagement\Providers;

use Illuminate\Auth\EloquentUserProvider;

class AuthUserProvider extends EloquentUserProvider
{

    public function retrieveById($identifier) {
        $result = $this->createModel()->newQuery()
//            ->leftJoin('customer_details', 'users.customer_detail_id', '=', 'customer_details.id')
//            ->select([
//                        'users.*'
//                        ,'customer_details.customer_detail_name'
////                        ,'customer_details.customer_detail_name_kanae'
//                    ])
            ->find($identifier);
        return $result;
    }

}
