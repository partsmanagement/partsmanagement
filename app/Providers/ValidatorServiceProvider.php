<?php

namespace partsmanagement\Providers;

use Illuminate\Support\ServiceProvider;
use partsmanagement\Validation\CustomValidator;

class ValidatorServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \Validator::resolver(function($translator,$data,$rules,$messages,$attributes){
            return new CustomValidator($translator,$data,$rules,$messages,$attributes);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
