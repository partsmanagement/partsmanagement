<?php

namespace partsmanagement\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use partsmanagement\Providers\AuthUserProvider;
use Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'partsmanagement\Model' => 'partsmanagement\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }

    public function register() {

        Auth::provider('auth_ex', function($app) {
            $model = $app['config']['auth.providers.users.model'];
            return new AuthUserProvider($app['hash'], $model);
        });

    }

}
