<?php

namespace partsmanagement\Validation;
use Auth;
use Hash;
use Request;

class CustomValidator extends \Illuminate\Validation\Validator
{
    /**
     * 数字以上チェック
     *
     * @param string $value
     * @return bool
     */
    public function validateNumAbove($attribute, $value, $parameters)
    {
        $key      = substr($attribute, -1);
        $data     = $this->getValue($parameters[0]);
        $condNum  = $data[$key];

        if (!empty($condNum)) {
            if ($value > $condNum) {
                return false;
            }
        }
        return true;

    }

    /**
     * 半角数字チェック
     *
     * @param string $value
     * @return bool
     */
    public function validateNumCheck($attribute, $value, $parameters)
    {
        return !is_null($value)?preg_match('/^[\d]+$/', $value):true;
    }

    /**
     * 半角英字チェック
     *
     * @param string $value
     * @return bool
     */
    public function validateAlphaCheck($attribute, $value, $parameters)
    {
        return preg_match('/^[A-Za-z]+$/', $value);
    }

    /**
     * 半角英数字チェック
     *
     * @param string $value
     * @return bool
     */
    public function validateAlphaNumCheck($attribute, $value, $parameters)
    {
        return !is_null($value)?preg_match('/^[A-Za-z\d]+$/', $value):true;
    }

    /**
     * 半角英数字チェック(_ and -)
     *
     * @param string $value
     * @return bool
     */
    public function validateAlphaDashCheck($attribute, $value, $parameters)
    {
        return preg_match('/^[A-Za-z\d_-]+$/', $value);
    }

    /**
     * 郵便番号ハイフン付き（半角数字3桁-半角数字4桁）かチェックする
     *
     * @param string $value
     * @return bool
     */
     public function validateZipHyphen($attribute, $value, $parameters)
     {
        return preg_match('/^\d{3}\-\d{4}$/', $value);
     }

    /**
     * 電話番号ハイフンなし（半角数字）かチェックする
     *
     * @param string $value
     * @return bool
     */
     public function validateTelHankakuNum($attribute, $value, $parameters)
     {
        return !is_null($value)?preg_match('/^[0-9]+$/', $value):true;
     }

    /**
     * 電話番号・携帯番号ハイフン付き（日本仕様）かチェックする
     *
     * 0から始まる市外局番込の10桁の番号
     * [2-9]から始まる市外局番を含まない5~8桁の番号
     * 市外局番と市内局番は各1~4桁、合わせると5桁 になる
     * 携帯電話とPHSは「070」、「080」又は「090」から始まる11桁の番号
     * 市外局番、市内局番の境目で区切りが入る可能性がある 03-1234-1234
     * 区切りが市内局番を()で囲む可能性がある 03(1234)1234
     * フリーダイヤルは0120から始まる4桁3桁3桁
     *
     * @param string $value
     * @return bool
     */
     public function validateTelMobileHyphenHankakuNum($attribute, $value, $parameters)
     {
//        $pattern = '/\A(((0(\d{1}[-(]?\d{4}|\d{2}[-(]?\d{3}|\d{3}[-(]?\d{2}|\d{4}[-(]?\d{1}|[5789]0[-(]?\d{4})[-)]?)|\d{1,4}\-?)\d{4}|0120[-(]?\d{3}[-)]?\d{3})\z/';
        // 国内プレフィックス(0)と市外局番(1～4) – 市内局番(1～4) – 加入者番号(4)
        // ハイフンは入れても入れなくても可
        $pattern = '/^(0{1}\d{1,4}-{0,1}\d{1,4}-{0,1}\d{4})$/';
        return !is_null($value)?preg_match($pattern, $value):true;
     }

    /**
     * 電話番号ハイフン付き（日本仕様）かチェックする
     *
     * 国内プレフィックス(0)と市外局番(1～4) – 市内局番(1～4) – 加入者番号(4)
     *
     * @param string $value
     * @return bool
     */
     public function validateTelHyphenHankakuNum($attribute, $value, $parameters)
     {
        $pattern = '/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/';
        return !is_null($value)?preg_match($pattern, $value):true;
     }

    /**
     * 電話番号ハイフンなしorありどちらでもOK（日本仕様）かチェックする
     *
     * 国内プレフィックス(0)と市外局番(1～4) – 市内局番(1～4) – 加入者番号(4)
     *
     * @param string $value
     * @return bool
     */
     public function validateTelHyphenNonHyphenHankakuNum($attribute, $value, $parameters)
     {
        $pattern = '/^(0{1}\d{1,4}-{0,1}\d{1,4}-{0,1}\d{4})$/';
        return !is_null($value)?preg_match($pattern, $value):true;
     }

    /**
     * 全角カタカナ数字かチェックする
     *
     * @param string $value
     * @return bool
     */
     public function validateEmNum($attribute,$value,$parameters)
     {
        //全角カタカナ
        if (preg_match('/^[０-９]+$/u', $value)) {
            return true;
        } else {
            return false;
        }
     }

     /**
     * すべて全角カタカナ、英数字、記号（・’”～．）かチェックする
     *
     * @param string $value
     * @return bool
     */
     public function validateAllEmKana($attribute,$value,$parameters)
     {
        //全角カタカナ
        if (preg_match('/^[ァ-ヾ－ー\s　０-９Ａ-Ｚａ-ｚ・’”～．]+$/u', $value)) {
            return true;
        } else {
            return false;
        }
     }

    /**
     * すべて半角カタカナかチェックする
     *
     * @param string $value
     * @return bool
     */
     public function validateAllEnKana($attribute, $value)
     {
        //半角カタカナ
        if (preg_match('/^[ｦ-ﾟ\s ]+$/u', $value)) {
            return true;
        } else {
            return false;
        }
     }

    /**
     * すべて全角半角カタカナ、英数字、記号かチェックするかチェックする
     *
     * @param string $value
     * @return bool
     */
     public function validateAllKana($attribute, $value)
     {
        //半角カタカナ
        if (preg_match('/^[ァ-ヾ－ーｦ-ﾟー\s 　０-９0-9Ａ-Ｚａ-ｚA-za-z・’”～．|[[:graph:]|[:space:]]+$/u', $value)) {
            return true;
        } else {
            return false;
        }
     }

    /**
     * すべて全角半角カタカナかチェックする
     *
     * @param string $value
     * @return bool
     */
     public function validateAllKanaAll($attribute, $value)
     {
        //半角カタカナ
        if (preg_match('/^[ｦ-ﾟ\s ァ-ヾ－ー\s]+$/u', $value)) {
            return true;
        } else {
            return false;
        }
     }

    /**
     * 入力されたパスワードとDBのパスワードが合致しているかチェック
     *
     * @param string $value
     * @return bool
     */
     public function validatePassConfirmHash($attribute, $value)
     {
        $userInfo = Auth::user();

        //現在のユーザーパスワードをDBから取得
        $hashedPassword = $userInfo->password;

        //入力された現在パスワードとDBのパスワードが合致しているかチェック
        if (Hash::check($value, $hashedPassword)){
            return true;
        } else {
            return false;
        }
     }

    /**
     * 指定したパラメータの日付より過去日付かチェックする
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  mixed   $parameters
     * @return bool
     */
     public function validateDateAfter($attribute, $value, $parameters)
     {

        $this->requireParameterCount(2, $parameters, 'date_after');

        $data = $this->getValue($parameters[0]);
        $ymd_if = $parameters[1];

        if($ymd_if == "Y") {
            $dataYMD = $data."/01/01";
            $valueYMD = $value."/01/01";
        } elseif($ymd_if == "YM") {
            $dataYMD = $data."/01";
            $valueYMD = $value."/01";
        } elseif($ymd_if == "YMD") {
            $dataYMD = $data;
            $valueYMD = $value;
        }

        if(strtotime($dataYMD) > strtotime($valueYMD)){
            return false;
        }
        return true;

     }

    /**
     * date_after エラーメッセージを作成する
     *
     * @param  string  $message
     * @param  string  $attribute
     * @param  string  $rule
     * @param  array   $parameters
     * @return string
     */
    public function replaceDateAfter($message, $attribute, $rule, $parameters)
    {
        $parameters[1] = $this->getDisplayableValue($parameters[0], $this->getValue($parameters[0]));
        $parameters[0] = $this->getAttribute($parameters[0]);
        return str_replace([':other', ':value'], $parameters, $message);
    }

    /**
     * 指定したパラメータの日付より未来日付かチェックする
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  mixed   $parameters
     * @return bool
     */
     public function validateDateBefore($attribute, $value, $parameters)
     {

        $this->requireParameterCount(2, $parameters, 'date_before');

        $data = $this->getValue($parameters[0]);
        $ymd_if = $parameters[1];

        if($ymd_if == "Y") {
            $dataYMD = $data."/01/01";
            $valueYMD = $value."/01/01";
        } elseif($ymd_if == "YM") {
            $dataYMD = $data."/01";
            $valueYMD = $value."/01";
        } elseif($ymd_if == "YMD") {
            $dataYMD = $data;
            $valueYMD = $value;
        }

        if(strtotime($dataYMD) < strtotime($valueYMD)){
            return false;
        }
        return true;

     }

    /**
     * date_before エラーメッセージを作成する
     *
     * @param  string  $message
     * @param  string  $attribute
     * @param  string  $rule
     * @param  array   $parameters
     * @return string
     */
    public function replaceDateBefore($message, $attribute, $rule, $parameters)
    {
        $parameters[1] = $this->getDisplayableValue($parameters[0], $this->getValue($parameters[0]));
        $parameters[0] = $this->getAttribute($parameters[0]);
        return str_replace([':other', ':value'], $parameters, $message);
    }

    /**
     * 指定パラメータの入力値が指定したパラメータ比較条件の時、指定したパラメータの必須入力チェックをする
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  mixed   $parameters
     * @return bool
     */
     public function validateRequiredIfValue($attribute, $value, $parameters)
     {

        $this->requireParameterCount(3, $parameters, 'required_if_value');

        $attribute_data = $parameters[0];
        $required_data = $this->getValue($parameters[0]);
        $chk_value = $parameters[1];
        $value_if = $parameters[2];

        if($value_if == "=") {
            if((int)$value == (int)$chk_value) {
                if (!$this->validateRequired($attribute_data, $required_data)) {
                    return false;
                }
            }
        } elseif($value_if == ">") {
            if((int)$value > (int)$chk_value) {
                if (!$this->validateRequired($attribute_data, $required_data)) {
                    return false;
                }
            }
        } elseif($value_if == ">=") {
            if((int)$value >= (int)$chk_value) {
                if (!$this->validateRequired($attribute_data, $required_data)) {
                    return false;
                }
            }
        } elseif($value_if == "<") {
            if((int)$value < (int)$chk_value) {
                if (!$this->validateRequired($attribute_data, $required_data)) {
                    return false;
                }
            }
        } elseif($value_if == "<=") {
            if((int)$value <= (int)$chk_value) {
                if (!$this->validateRequired($attribute_data, $required_data)) {
                    return false;
                }
            }
        }

        return true;

     }

    /**
     * date_before エラーメッセージを作成する
     *
     * @param  string  $message
     * @param  string  $attribute
     * @param  string  $rule
     * @param  array   $parameters
     * @return string
     */
    public function replaceRequiredIfValue($message, $attribute, $rule, $parameters)
    {
        $parameters[1] = $this->getValue($attribute);
        $parameters[0] = $this->getAttribute($parameters[0]);
        return str_replace([':other', ':value'], $parameters, $message);
    }
}

