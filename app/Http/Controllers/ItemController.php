<?php

namespace partsmanagement\Http\Controllers;

//use Illuminate\Http\Request;
use partsmanagement\Http\Requests\FormItemRequest;
use partsmanagement\Libs\Funcs;
use partsmanagement\Libs\ChargeInfo;
use partsmanagement\Models\Items;
use partsmanagement\Models\Customers;
use Symfony\Component\HttpFoundation\StreamedResponse;

use Config;
use Carbon\Carbon;
use DB;
use Input;
use Request;
use Response;
use Session;
use Validator;
use File;
use Image;
use Storage;

class ItemController extends Controller
{
    /**
     * コンストラクタ
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 初期表示
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->search();
    }

    /**
     * 検索
     *
     * @return Response
     */
    public function search()
    {
        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */

        /* ------------------------------------------------------
         * データ取得
         *------------------------------------------------------ */
        //リクエスト取得
        $prms = Request::all();

        //データ取得
        $results = $this->getData($prms);

        /* ---------------------------
         * VIEW
         *--------------------------- */
        return view('item.index', compact('results', 'prms'));
    }
    /**
     * SQLの生成
     * 一覧データの取得
     * @return Response
     */
	public function getData($prms)
	{
        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */
        // リクエスト取得
        $prms = Request::all();
		$data['results'] = null;

        /* ------------------------------------------------------
         * 一覧データの取得
         *------------------------------------------------------ */
        // <editor-fold defaultstate="collapsed" desc=" クエリ">
        $query = Items::select(
                                'id'                       // 商品ID
                               ,'item_code'                // 商品コード
                               ,'item_name'                // 商品名
                               ,'item_name_kana'           // 商品名カナ
                               ,'item_file_name'           // 商品画像ファイル名（格納パスはConst定義）
                               ,'item_cost_price'          // 商品原価
                               ,'item_method'              // 商品規格
                               ,'item_quantity'            // 商品入数
                               ,'item_quantity_unit'       // 商品入数単位
                               ,'item_unit'                // 商品単位
                               ,'wholesale_basic_price'    // 卸売通常価格
                               ,'retail_basic_price'       // 小売通常価格
                               ,'wholesale_sale_price'     // 卸売特売価格
                               ,'retail_sale_price'        // 小売特売価格
                    );
//                ->leftjoin('category','item.category_id','=','category.id');

        //商品名
        if(!empty(Funcs::vl('item_name', $prms))){
            $query->where(function($q)use($prms){
                $q->where('items.item_name','LIKE',sprintf('%%%s%%',Funcs::vl('item_name', $prms)))
                ->orWhere('items.id',Funcs::vl('item_name', $prms))
                ;
            });
        }
        //商品カナ
        if(!empty(Funcs::vl('item_name_kana', $prms))){
            $query->where('items.item_name_kana','LIKE','%'.Funcs::vl('item_name_kana', $prms).'%');
        }
        $query->orderBy('id','asc');

        $data = $query->paginate(Config::get('const.default_maxrowsize'))->appends($prms);
        // </editor-fold>

		return $data;
    }

    /**
     * 登録更新画面へ遷移
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id=null)
    {
        /*-----------------------------------------
         * 1. 初期化
         *----------------------------------------- */
        $data = [];
        $data['results'] = [];

        /*-----------------------------------------
         * 2. 登録更新データの取得
         *----------------------------------------- */
        // <editor-fold defaultstate="collapsed" desc=" 登録更新データの取得">
        $hasold = !empty(Input::old());
        if(!$hasold){
            if (empty($id))
            {
                /*---------------------------
                 * 2.1 新規
                 *---------------------------*/

            }
            else
            {
                /*---------------------------
                 * 2.2 修正
                 *---------------------------*/
                /* ===========================
                 * 2.2.1 初期表示データの取得
                 * =========================== */
                $data['results'] = Items::find( $id );
                if(empty( $data['results'] )) {
                    // 該当がない場合、エラーにして一覧へ戻す
                    Session::flash('flash_danger', '商品情報が存在しませんでした。');
                    return redirect('item'); //一覧へ戻す
                }
            }

        }
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return view('item.edit', compact('data'));

    }

    /**
     * 登録更新処理
     *
     * @return Response
     */
    public function save()
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge  = new ChargeInfo;

        // リクエスト
        $request = Request::all();
        $id      = Funcs::rq('id', $request);

        // 結果格納用
        $data['results'] = array();

        /* ----------------------------------------------
         * 2. バリデーション
         * ----------------------------------------------*/
        $validator = Validator::make( $request, FormItemRequest::rules(), FormItemRequest::messages() );

        // バリデーションエラーだった場合
        if ( $validator->fails() ) {

            return view('item.edit', compact('data'))->withErrors($validator);
        }

        /* ----------------------------------------------
         * 3. DB登録
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3. DB登録">
        //存在チェック
        $isnew = empty(Items::find($id));
        if( $isnew ){
            /* ----------------------------------------
             * 4.1 新規
             * ----------------------------------------*/
            $table = new Items();
            $table->add_user_name = $charge->user_name;    // 登録者
            $edit_message = "登録";
        }else{
            /* ----------------------------------------
             * 4.2 更新
             * ----------------------------------------*/
            $table = Items::find($id);
            $edit_message = "更新";
        }

        if(!empty($request['item_file_name'])) {
            // アップロードしたファイルのオリジナル名の取得
            //$item_file_name = $request['item_file_name']->getClientOriginalName();
            $item_file_name = $request['item_code'].'.'.$request['item_file_name']->getClientOriginalExtension();

            // アップロードしたファイルのパスを取得
            $image = Image::make($request['item_file_name']->getRealPath());

            // imagesフォルダがあるかどうかを確認し、ない場合は新規作成する
            File::exists(storage_path() . '/app/public/items') or File::makeDirectory(storage_path() . '/app/public/items');

            //既に同一画像ファイル名がアップロードされている場合は既存の画像ファイルを削除
            File::delete(storage_path() . '/app/public/items/'.$request['item_db_filename']);
            File::delete(storage_path() . '/app/public/items/100-'.$request['item_db_filename']);
            File::delete(storage_path() . '/app/public/items/200-'.$request['item_db_filename']);

            // Original Image → Sumnail Image 200px を作成保存する
            $image->save(storage_path() . '/app/public/items/' . $item_file_name)
                  ->resize(200, null, function ($constraint) {$constraint->aspectRatio();})
                  ->save(storage_path() . '/app/public/items/200-' . $item_file_name);

            // Original Image → Sumnail Image 100px を作成保存する
            $image->save(storage_path() . '/app/public/items/' . $item_file_name)
                  ->resize(100, null, function ($constraint) {$constraint->aspectRatio();})
                  ->save(storage_path() . '/app/public/items/100-' . $item_file_name);
        } else {
            if(!empty($request['item_del_filename'])) {
                $item_file_name = null;
                //アップロードされている画像ファイルを削除
                File::delete(storage_path() . '/app/public/items/'.$request['item_del_filename']);
                File::delete(storage_path() . '/app/public/items/100-'.$request['item_del_filename']);
                File::delete(storage_path() . '/app/public/items/200-'.$request['item_del_filename']);
            } else {
                $item_file_name = $request['item_db_filename'];
            }
        }

        //入力項目のセット
        $table->item_code               = $request['item_code'];                // 商品コード
        $table->item_name               = $request['item_name'];                // 商品名
        $table->item_name_kana          = $request['item_name_kana'];           // 商品名カナ
        $table->item_short_name         = $request['item_short_name'];          // 商品簡略名
        $table->item_file_name          = $item_file_name;                      // 商品画像ファイル名（格納パスはConst定義）
        $table->item_cost_price         = $request['item_cost_price'];          // 商品原価
        $table->item_method             = $request['item_method'];              // 商品規格
        $table->item_quantity           = $request['item_quantity'];            // 商品入数
        $table->item_quantity_unit      = $request['item_quantity_unit'];       // 商品入数単位
        $table->item_unit               = $request['item_unit'];                // 商品単位
        $table->wholesale_basic_price   = $request['wholesale_basic_price'];    // 卸売通常価格
        $table->retail_basic_price      = $request['retail_basic_price'];       // 小売通常価格
        $table->wholesale_sale_price    = $request['wholesale_sale_price'];     // 卸売特売価格
        $table->retail_sale_price       = $request['retail_sale_price'];        // 小売特売価格
        $table->wholesale_rank_price1   = $request['wholesale_rank_price1'];    // 卸売ランク価格１
        $table->retail_rank_price1      = $request['retail_rank_price1'];       // 小売ランク価格１
        $table->wholesale_rank_price2   = $request['wholesale_rank_price2'];    // 卸売ランク価格２
        $table->retail_rank_price2      = $request['retail_rank_price2'];       // 小売ランク価格２
        $table->wholesale_rank_price3   = $request['wholesale_rank_price3'];    // 卸売ランク価格３
        $table->retail_rank_price3      = $request['retail_rank_price3'];       // 小売ランク価格３
        $table->wholesale_rank_price4   = $request['wholesale_rank_price4'];    // 卸売ランク価格４
        $table->retail_rank_price4      = $request['retail_rank_price4'];       // 小売ランク価格４
        $table->wholesale_rank_price5   = $request['wholesale_rank_price5'];    // 卸売ランク価格５
        $table->retail_rank_price5      = $request['retail_rank_price5'];       // 小売ランク価格５
        $table->item_disp_type          = $request['item_disp_type'];           // 商品表示区分（1：表示、99：非表示）
        $table->upd_user_name           = $charge->user_name;                   // 更新者

        // 登録更新処理
        $ret = $table->save();

        // 正常終了メッセージ
        Session::flash('flash_success', $edit_message."が完了しました。");
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return redirect('item');
    }

    /**
     * 論理削除処理
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // ログイン情報
        $charge = new ChargeInfo;

        if(!empty($id)){
            $data = Items::find($id);
            if(empty($data)) {
                Session::flash('flash_danger', '削除情報が存在しませんでした。');
            }else{
                $data->delete();                            // 論理削除処理
                $data->upd_user_name = $charge->user_name;  // 更新者
                $data->save();

                // 正常終了メッセージ
                Session::flash('flash_success', "削除が完了しました。");
            }
		}

        // 一覧に戻す
        return redirect('item');
    }

    /**
     * CSVエクスポート
     * ItemsList.csv
     *
     * @return Response
     */
    public function csvput()
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=ItemsList.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $callback = function() {
            $prms = Request::all();
        
            $handle = fopen('php://output', 'w');

            $columns = [
                '商品ID'
                ,'商品コード'
                ,'商品名'
                ,'商品カナ名'
                ,'商品簡略名'
                ,'商品画像ファイル名'
                ,'商品原価'
                ,'商品規格'
                ,'商品入数'
                ,'商品入数単位'
                ,'商品単位'
                ,'卸売通常価格'
                ,'小売通常価格'
                ,'卸売特売価格'
                ,'小売特売価格'
                ,'商品表示区分'
            ];

            mb_convert_variables('SJIS-win', 'UTF-8', $columns);
            fputcsv($handle, $columns);

            $query = Items::select(
                        'id'                       // 商品ID
                       ,'item_code'                // 商品コード
                       ,'item_name'                // 商品名
                       ,'item_name_kana'           // 商品名カナ
                       ,'item_short_name'          // 商品簡略名
                       ,'item_file_name'           // 商品画像ファイル名（格納パスはConst定義）
                       ,'item_cost_price'          // 商品原価
                       ,'item_method'              // 商品規格
                       ,'item_quantity'            // 商品入数
                       ,'item_quantity_unit'       // 商品入数単位
                       ,'item_unit'                // 商品単位
                       ,'wholesale_basic_price'    // 卸売通常価格
                       ,'retail_basic_price'       // 小売通常価格
                       ,'wholesale_sale_price'     // 卸売特売価格
                       ,'retail_sale_price'        // 小売特売価格
                       ,'item_disp_type'            // 商品表示区分（1：表示、99：非表示）
                    );

            //商品名
            if(!empty(Funcs::vl('item_name', $prms))){
                $query->where('items.item_name','LIKE','%'.Funcs::vl('item_name', $prms).'%');
            }
            //商品カナ
            if(!empty(Funcs::vl('item_name_kana', $prms))){
                $query->where('items.item_name_kana','LIKE','%'.Funcs::vl('item_name_kana', $prms).'%');
            }
            $query->orderBy('id','asc');
            $datas = $query->get();

            foreach ($datas as $row) {
                $csv = [
                    $row->id
                    ,$row->item_code
                    ,$row->item_name
                    ,$row->item_name_kana
                    ,$row->item_short_name
                    ,$row->item_file_name
                    ,str_replace(',', '', number_format($row->item_cost_price))
                    ,$row->item_method
                    ,$row->item_quantity
                    ,$row->item_quantity_unit
                    ,$row->item_unit
                    ,str_replace(',', '', number_format($row->wholesale_basic_price))
                    ,str_replace(',', '', number_format($row->retail_basic_price))
                    ,str_replace(',', '', number_format($row->wholesale_sale_price))
                    ,str_replace(',', '', number_format($row->retail_sale_price))
                    ,$row->item_disp_type
                ];
                mb_convert_variables('SJIS-win', 'UTF-8', $csv);
                fputcsv($handle, $csv);
            }
            fclose($handle);
        };

      return response()->stream($callback, 200, $headers);
    }
}
