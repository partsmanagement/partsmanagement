<?php

namespace partsmanagement\Http\Controllers;

//use Illuminate\Http\Request;
use partsmanagement\Http\Requests\FormImagesRequest;
use partsmanagement\Http\Requests\FormVehicles;
use partsmanagement\Libs\Funcs;
use partsmanagement\Libs\ChargeInfo;
use partsmanagement\Models\Images;
use partsmanagement\Models\Manufacturers;
use partsmanagement\Models\Models;
use partsmanagement\Models\Parts;
use partsmanagement\Models\Vehicles;
use partsmanagement\Models\VehiclesParts;
use partsmanagement\Models\Yards;

use Symfony\Component\HttpFoundation\StreamedResponse;

use Config;
use Carbon\Carbon;
use DB;
use File;
use Image;
use Input;
use Request;
use Response;
use Session;
use Storage;
use Validator;


class VehiclesController extends Controller
{
    /**
     * コンストラクタ
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 初期表示
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->search();
    }

    /**
     * 検索
     *
     * @return Response
     */
    public function search()
    {
        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */
        //リクエスト取得
        $prms = Request::all();

        /* ------------------------------------------------------
         * データ取得
         *------------------------------------------------------ */
        // 1.1 メーカー：プルダウン
        $query = Manufacturers::orderBy('manufacturer_name','asc');
        $manufacturer_list = $query->pluck('manufacturer_name', 'id');

        // 1.2 ヤード：プルダウン
        $query = Yards::orderBy('yard_name','asc');
        $yard_list = $query->pluck('yard_name', 'id');

        //データ取得
        $results = $this->getData($prms);
        foreach( $results as $row ) {
            // パーツ画像数取得
            $row->cnt_images = Images::where('vehicles_id', '=', $row->id)->count();
            $regdate = str_replace('-', '/', $row->first_registration_date);
            $regdate = $this->convGtJDate( $regdate.'/01' ).substr($regdate, 5, 2)."月";
            $row->first_registration_date_wareki = $regdate;
        }

        /* ---------------------------
         * VIEW
         *--------------------------- */
        return view('vehicle.index', compact('results', 'prms', 'manufacturer_list', 'yard_list'));
    }

    /**
     * SQLの生成
     * 一覧データの取得
     * @return Response
     */
	public function getData($prms)
	{
        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */
        // リクエスト取得
        $prms = Request::all();
		$data['results'] = null;

        /* ------------------------------------------------------
         * 一覧データの取得
         *------------------------------------------------------ */
        // <editor-fold defaultstate="collapsed" desc=" クエリ">
        $query = Vehicles::select('vehicles.*'
                                  ,'manufacturers.manufacturer_name'
//                                  ,'models.model_name'
                                  ,'yards.yard_name'
                            );
        $query->leftjoin('manufacturers','vehicles.manufacturer_id','=', 'manufacturers.id');
        $query->leftjoin('models',       'vehicles.model_id',       '=', 'models.id');
        $query->leftjoin('yards',        'vehicles.yard_id',        '=', 'yards.id');

        // メーカー名
        if(!empty(Funcs::vl('manufacturer_id', $prms))){
            $query->where('vehicles.manufacturer_id', '=', Funcs::vl('manufacturer_id', $prms));
        }

        // 車両名
        if(!empty(Funcs::vl('model_name', $prms))){
            $query->where('vehicles.model_name','like','%'.Funcs::vl('model_name', $prms).'%');
        }

        // 車体番号
        if(!empty(Funcs::vl('vehicle_number', $prms))){
            $query->where('vehicles.vehicle_number','like','%'.Funcs::vl('vehicle_number', $prms).'%');
        }

        // 初年度検査年月
        if(!empty(Funcs::vl('first_registration_date', $prms))){
            $query->where('vehicles.first_registration_date','like','%'.Funcs::vl('first_registration_date', $prms).'%');
        }

        // カラーカテゴリー
        if(!empty(Funcs::vl('vehicle_color', $prms))){
            $query->where('vehicles.vehicle_color','=', Funcs::vl('vehicle_color', $prms));
        }

        // カラー識別コード
        if(!empty(Funcs::vl('color_code', $prms))){
            $query->where('vehicles.color_code','like','%'.Funcs::vl('color_code', $prms).'%');
        }

        // 型式
        if(!empty(Funcs::vl('model_number', $prms))){
            $query->where('vehicles.model_number','like','%'.Funcs::vl('model_number', $prms).'%');
        }

        // 型式指定番号
        if(!empty(Funcs::vl('model_designation_number', $prms))){
            $query->where('vehicles.model_designation_number','like','%'.Funcs::vl('model_designation_number', $prms).'%');
        }

        // 類別区分番号
        if(!empty(Funcs::vl('category_classification_number', $prms))){
            $query->where('vehicles.category_classification_number','like','%'.Funcs::vl('category_classification_number', $prms).'%');
        }

        // ヤード
        if(!empty(Funcs::vl('yard_id', $prms))){
            $query->where('vehicles.yard_id','like','%'.Funcs::vl('yard_id', $prms).'%');
        }

        // 検索キーワード
        if(!empty(Funcs::vl('search_keyword', $prms))){
            $keys = str_replace(array(" ", "　",'、'), ',', Funcs::vl('search_keyword', $prms));
            $arr_search_keyword = explode(',', $keys);

            $query->Where(function ($query_l) use($arr_search_keyword) {
                for ($i = 0; $i < count($arr_search_keyword); $i++)
                {
                    if ( !empty($arr_search_keyword[$i]) ) {
                        $query_l->orwhere('vehicles.search_keyword', 'like', '%' . $arr_search_keyword[$i] .'%');
                    }
                }
            });
        }

        $query->orderBy('models.updated_at','asc');

        $data = $query->paginate(Config::get('const.default_30rows'))->appends($prms);
        // </editor-fold>

		return $data;
    }

    /**
     * 登録更新画面へ遷移
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id=null)
    {
        /*-----------------------------------------
         * 1. 初期化
         *----------------------------------------- */
        // 1.1 データ受け渡し用
        $data = [];
        $data['results'] = [];
        $models_list = array();

        // 1.2 メーカー：プルダウン
        $query = Manufacturers::orderBy('manufacturer_name','asc');
        $manufacturer_list = $query->pluck('manufacturer_name', 'id');
        $manufacturer_list = $manufacturer_list->prepend('', '');

        // 1.3 パーツ：プルダウン
        $parts_list = Parts::orderBy('parts_name','asc')->get();

        // 1.4 車両パーツ：リスト
        $query = VehiclesParts::select('vehicles_parts.*'
                                      ,'parts.parts_name'
                );
        $query->where('vehicles_id', '=', $id);
        $query->where('parts_status', '!=', 1);
        $query->leftjoin('parts','vehicles_parts.parts_id','=', 'parts.id');
        $query->orderBy('parts.display_no','asc');
        $v_parts_list = $query->get();

        // 1.5 ヤード：プルダウン
        $query = Yards::orderBy('yard_name','asc');
        $yard_list = $query->pluck('yard_name', 'id');
        $yard_list = $yard_list->prepend('', '');

        // 1.6 初年度検査年月：プルダウン
        $now = date("Y");
        $regi_date = array();
        for($y=1980; $now>=$y; $y++) {
            $m = "01";
            $date = $y."/".$m."/01";
            $key  = $y;
//            $key  = $y."-".$m;
            $seireki = $y;
            $wareki = $this->convGtJDate( $date );
            $vlue = $seireki." ".$wareki;
//            $regi_date = array_merge($regi_date, array($key => $vlue));
            $regi_date[$key] = $vlue;
        }
        arsort($regi_date);

        /*-----------------------------------------
         * 2. 登録更新データの取得
         *----------------------------------------- */
        // <editor-fold defaultstate="collapsed" desc=" 登録更新データの取得">
        $hasold = !empty(Input::old());
        if(!$hasold){
            if (empty($id))
            {
                /*---------------------------
                 * 2.1 新規
                 *---------------------------*/
                $data['results']['parts'] = '';

            }
            else
            {
                /*---------------------------
                 * 2.2 修正
                 *---------------------------*/
                /* ===========================
                 * 2.2.1 初期表示データの取得
                 * =========================== */
                // <editor-fold defaultstate="collapsed" desc=" 2.2.1 初期表示データの取得">
                $results = Vehicles::find( $id );
                if(empty( $results )) {
                    // 該当がない場合、エラーにして一覧へ戻す
                    Session::flash('flash_danger', '車両情報が存在しませんでした。');
                    return redirect('vehicle'); //一覧へ戻す
                }

                // 2.2.1.1 車種：プルダウン
                $query = Models::where('manufacturer_id', '=', $results['manufacturer_id'])->orderBy('model_name','asc');
                $models_list = $query->pluck('model_name', 'id');
                $models_list = $models_list->prepend('', '');

                $data['results']['id']             					= $results['id'];                       		// ID
                $data['results']['manufacturer_id']             	= $results['manufacturer_id'];                  // メーカーID
//                $data['results']['model_id']             			= $results['model_id'];                       	// 車種ID
                $data['results']['model_name']             			= $results['model_name'];                       // 車種名
                $data['results']['vehicle_number']             		= $results['vehicle_number'];                   // 車体番号
//                $data['results']['first_registration_date']         = $results['first_registration_date'];          // 初年度検査年月
                $data['results']['regi_date_year']                  = substr($results['first_registration_date'], 0, 4);          // 初年度検査年月
                $data['results']['regi_date_month']                 = substr($results['first_registration_date'], 5, 2);          // 初年度検査年月
                $data['results']['vehicle_color']             		= $results['vehicle_color'];                    // カラーカテゴリー
                $data['results']['color_code']                      = $results['color_code'];                       // カラーコード
                $data['results']['parts']             				= $results['parts'];                       		// パーツ
                $data['results']['transmission']             		= $results['transmission'];                     // トランスミッション（1：AT、2：MT）
                $data['results']['model_number']					= $results['model_number'];                     // 型式
                $data['results']['prime_mover_model']               = $results['prime_mover_model'];                // 原動機型式
                $data['results']['model_designation_number']        = $results['model_designation_number'];         // 型式指定番号
                $data['results']['category_classification_number']  = $results['category_classification_number'];	// 類別区分番号
                $data['results']['yard_id']             			= $results['yard_id'];                          // ヤード
                $data['results']['remarks']             			= $results['remarks'];                       	// 特記事項
                $data['results']['provider_name']             		= $results['provider_name'];                    // 提供者名
                $data['results']['acquisition_amount']             	= $results['acquisition_amount'];               // 取得金額
                $data['results']['search_keyword']             		= $results['search_keyword'];                   // 検索キーワード
                $data['results']['main_file_names']             	= $results['main_file_names'];                  // 画像ファイル名（格納パスはConst定義）
                // </editor-fold>
            }

        }
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return view('vehicle.edit', compact('data', 'manufacturer_list', 'models_list', 'parts_list', 'v_parts_list', 'yard_list', 'regi_date'));

    }

    /**
     * 登録更新処理
     *
     * @return Response
     */
    public function save()
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge  = new ChargeInfo;

        // リクエスト
        $request                 = Request::all();
        $id                      = Funcs::rq('id',                      $request);
        $manufacturer_id         = Funcs::rq('manufacturer_id',         $request);
        $parts                   = Funcs::rq('parts',                   $request);
        $first_registration_date = Funcs::rq('first_registration_date', $request);
        $request['first_registration_date'] = str_replace('/', '-', $first_registration_date);

        // 結果格納用
        $data['results'] = $request;

        /* ----------------------------------------------
         * 2. バリデーション
         * ----------------------------------------------*/
        $validator = Validator::make( $request, FormVehicles::rules(), FormVehicles::messages() );

        // バリデーションエラーだった場合
        if ( $validator->fails() ) {
//            // 2.1 メーカー：プルダウン
//            $query = Manufacturers::orderBy('manufacturer_name','asc');
//            $manufacturer_list = $query->pluck('manufacturer_name', 'id');
//            $manufacturer_list = $manufacturer_list->prepend('', '');
//
//            // 2.2 車種：プルダウン
//            $query = Models::where('manufacturer_id', '=', $manufacturer_id)->orderBy('model_name','asc');
//            $models_list = $query->pluck('model_name', 'id');
//            $models_list = $models_list->prepend('', '');
//
//            // 2.3 パーツ：プルダウン
//            $parts_list = Parts::orderBy('parts_name','asc')->get();
//
//            // 2.4 パーツをVIEW用に整理
//            if ( Funcs::rq('parts', $request) ) {
//                $data['results']['parts'] = implode(',', Funcs::rq('parts', $request));
//            }else{
//                $data['results']['parts'] = '';
//            }
//
//            // 2.5 車両パーツ：リスト
//            $query = VehiclesParts::select('vehicles_parts.*'
//                                          ,'parts.parts_name'
//                    );
//            $query->where('vehicles_id', '=', $id);
//            $query->where('parts_status', '!=', 1);
//            $query->leftjoin('parts','vehicles_parts.parts_id','=', 'parts.id');
//            $query->orderBy('parts.display_no','asc');
//            $v_parts_list = $query->get();

//            // メイン車両画像
//            $data['results']['main_file_names'] = $data['results']['item_db_filename'];

//            return view('vehicle.edit', compact('data', 'manufacturer_list', 'models_list', 'parts_list', 'v_parts_list'))->withErrors($validator);
            return back()->withErrors($validator)->withInput();
        }

        /* ----------------------------------------------
         * 3. DB登録
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3. DB登録">
        //存在チェック
        $isnew = empty(Vehicles::find($id));
        if( $isnew ){
            /* ----------------------------------------
             * 3.1 新規
             * ----------------------------------------*/
            $table = new Vehicles();
            $table->add_user_name = $charge->user_name;    // 登録者
            $edit_message = "登録";
        }else{
            /* ----------------------------------------
             * 3.2 更新
             * ----------------------------------------*/
            $table = Vehicles::find($id);
            $edit_message = "更新";
        }

        /* ----------------------------------------
         * 3.3. アップロードファイル編集
         * ----------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3.3. アップロードファイル編集">
        $filenameWithExt = "";
        if(!empty($request['main_file_names'])) {
            /* ----------------------------------------
             * 3.3.1 アップロードされていた
             *       メイン画像ファイルを削除
             * ----------------------------------------*/
            if ( !empty($table->main_file_names) ) {
                File::delete(storage_path() . '/app/public/items/'.$table->main_file_names);
                File::delete(storage_path() . '/app/public/items/100-'.$table->main_file_names);
                File::delete(storage_path() . '/app/public/items/200-'.$table->main_file_names);
            }

            /* ----------------------------------------
             * 3.3.2 画像保存準備
             * ----------------------------------------*/
            // 1. ファイル名で使用
            $dt = Carbon::now();
            $sysdate = $dt->format('Ymdhis');
            $rand = mt_rand( 1, 10000 );

            // 2. アップロードしたファイルのオリジナル名の取得
            $filenameWithExt = 'Main-'.$request['vehicle_number'].'_'.$sysdate.$rand.'.'.$request['main_file_names']->getClientOriginalExtension();

            // 3. アップロードしたファイルのパスを取得
            $image = Image::make($request['main_file_names']->getRealPath());

            // 4. imagesフォルダがあるかどうかを確認し、ない場合は新規作成する
            File::exists(storage_path() . '/app/public/items') or File::makeDirectory(storage_path() . '/app/public/items');

            /* ----------------------------------------
             * 3.3.3 画像の保存
             * ----------------------------------------*/
            // 1. Original Imageを保存する
            $image->save(storage_path() . '/app/public/items/' . $filenameWithExt);

            // 2. Original Image → Sumnail Image 200px を作成保存する
            $image->resize(200, null, function ($constraint) {$constraint->aspectRatio();})
                  ->save(storage_path() . '/app/public/items/200-' . $filenameWithExt);

            // 3. Original Image → Sumnail Image 100px を作成保存する
            $image->resize(100, null, function ($constraint) {$constraint->aspectRatio();})
                  ->save(storage_path() . '/app/public/items/100-' . $filenameWithExt);

        } else {
            /* ----------------------------------------
             * 3.3.4 アップロードされていた
             *       メイン画像ファイルを削除
             * ----------------------------------------*/
            if(!empty($request['item_del_filename'])) {
                $filenameWithExt = null;
                //アップロードされている画像ファイルを削除
                File::delete(storage_path() . '/app/public/items/'.$request['item_del_filename']);
                File::delete(storage_path() . '/app/public/items/100-'.$request['item_del_filename']);
                File::delete(storage_path() . '/app/public/items/200-'.$request['item_del_filename']);
            } else {
                $filenameWithExt = $request['item_db_filename'];
            }
        }
        // </editor-fold>

        //入力項目のセット
        $table->manufacturer_id   				= $request['manufacturer_id'];    				// メーカーID
//        $table->model_id   						= $request['model_id'];    						// 車種ID
        $table->model_name 						= $request['model_name'];    					// 車種名
        $table->vehicle_number   				= $request['vehicle_number'];    				// 車体番号
//        $table->first_registration_date   		= $request['first_registration_date'];			// 初年度検査年月
        $table->first_registration_date   		= $request['regi_date_year']."-".$request['regi_date_month'];			// 初年度検査年月
        $table->vehicle_color   				= $request['vehicle_color'];    				// カラーカテゴリー
        $table->color_code                      = $request['color_code'];                       // カラー識別コード
        $table->transmission   					= $request['transmission'];    					// トランスミッション（1：AT、2：MT）
        $table->model_number   					= $request['model_number'];    					// 型式
        $table->prime_mover_model   			= $request['prime_mover_model'];    			// 原動機型式
        $table->model_designation_number   		= $request['model_designation_number'];			// 型式指定番号
        $table->category_classification_number	= $request['category_classification_number'];	// 類別区分番号
        $table->yard_id   						= $request['yard_id'];    						// ヤードID
        $table->remarks   						= $request['remarks'];    						// 特記事項
        $table->provider_name   				= $request['provider_name'];    				// 提供者名
        $table->acquisition_amount   			= $request['acquisition_amount'];    			// 取得金額
        $table->search_keyword   				= $request['search_keyword'];    				// 検索キーワード
        $table->main_file_names                 = $filenameWithExt;                             // 画像ファイル名（格納パスはConst定義）
        $table->upd_user_name                   = $charge->user_name;                           // 更新者

        // 登録更新処理
        $ret = $table->save();

        // 正常終了メッセージ
        Session::flash('flash_success', $edit_message."が完了しました。");
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return redirect('vehicle');
    }

    /**
     * 登録更新画面へ遷移
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function images($id=null)
    {
        /*-----------------------------------------
         * 1. 初期化
         *----------------------------------------- */
        // 1.1 データ受け渡し用
        $data = [];
        $data['results'] = [];
        $results = [];

        /*-----------------------------------------
         * 2. 登録更新データの取得
         *----------------------------------------- */
        // <editor-fold defaultstate="collapsed" desc=" 登録更新データの取得">
        $hasold = !empty(Input::old());
        if(!$hasold){
            if (empty($id))
            {
                /*---------------------------
                 * 2.1 新規
                 *---------------------------*/
                $data['results']['parts'] = '';

            }
            else
            {
                /*---------------------------
                 * 2.2 修正
                 *---------------------------*/
                /* ===========================
                 * 2.2.1 初期表示データの取得
                 * =========================== */
                // <editor-fold defaultstate="collapsed" desc=" 2.2.1 初期表示データの取得">
                $query = Vehicles::select('vehicles.*'
                                        ,'manufacturers.manufacturer_name'
//                                        ,'models.model_name'
                                        ,'images.vehicles_id'
                                        ,'images.vehicle_file_name'
                                        ,'images.add_user_name AS add_uname'
                                        ,'images.upd_user_name AS upd_uname'
                        );
                $query->leftjoin('images', 'vehicles.id', '=', 'images.vehicles_id');
                $query->leftjoin('manufacturers', 'vehicles.manufacturer_id', '=', 'manufacturers.id');
                $query->leftjoin('models', 'vehicles.model_id', '=', 'models.id');
                $query->where('vehicles.id', '=', $id);
                $query->orderBy('vehicles.updated_at','asc');
                $results = $query->get();

                if(empty( $results )) {
                    // 該当がない場合、エラーにして一覧へ戻す
                    Session::flash('flash_danger', '車両情報が存在しませんでした。');
                    return redirect('vehicle'); //一覧へ戻す
                }
                // </editor-fold>
            }

        }
        $php_results = json_encode($results);
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return view('vehicle.editimg', compact('results', 'php_results'));
    }

    /**
     * 登録更新処理
     *
     * @return Response
     */
    public function saveimages()
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge  = new ChargeInfo;

        // リクエスト
        $request                = Request::all();
        $id                     = Funcs::rq('id',                   $request);
        $manufacturer_id        = Funcs::rq('manufacturer_id',      $request);
        $parts                  = Funcs::rq('parts',                $request);

        $vehicle_file_names     = Funcs::rq('file',    $request);

        // 結果格納用
        $data['results'] = array();
        $product = [];

        /* ----------------------------------------------
         * 2. バリデーション
         * ----------------------------------------------*/
        $validator = Validator::make( $request, FormImagesRequest::rules(), FormImagesRequest::messages() );

        // バリデーションエラーだった場合
        if ( $validator->fails() ) {

            return view('vehicle.editimg', compact('data'))->withErrors($validator);
        }

        /* ----------------------------------------------
         * 3. DB登録
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3. DB登録">
        //存在チェック
        $isnew = empty(Vehicles::find($id));
        if( !$isnew ){
            /* ----------------------------------------
             * 3.1 新規
             * ----------------------------------------*/
            $table = new Images();
            $table->add_user_name = $charge->user_name;    // 登録者
            $edit_message = "登録";
        }else{
            /* ----------------------------------------
             * 3.2 更新
             * ----------------------------------------*/
            $table = Images::find($id);
            $edit_message = "更新";
        }

        /* ----------------------------------------
         * 3.3 画像アップロード
         * ----------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3.3 画像アップロード">
        if(!empty($request['file'])) {
            /* ----------------------------------------
             * 3.3.1 画像保存準備
             * ----------------------------------------*/
            // 1. ファイル名で使用
            $dt = Carbon::now();
            $sysdate = $dt->format('Ymdhis');
            $rand = mt_rand( 1, 10000 );

            // 2. アップロードしたファイルのオリジナル名の取得
            $item_file_name = $request['vehicle_number'].'_'.$sysdate.$rand.'.'.$request['file']->getClientOriginalExtension();

            // 3. アップロードしたファイルのパスを取得
            $image = Image::make(file_get_contents($request['file']->getRealPath()));

            // 4. imagesフォルダがあるかどうかを確認し、ない場合は新規作成する
            File::exists(storage_path() . '/app/public/items') or File::makeDirectory(storage_path() . '/app/public/items');

            /* ----------------------------------------
             * 3.3.2 画像の保存
             * ----------------------------------------*/
            // Original Image | Sumnail Image 100px | Sumnail Image 200px を作成保存する
            $image->save(storage_path() . '/app/public/items/' . $item_file_name)
                  ->resize(100, null, function ($constraint) {$constraint->aspectRatio();})
                  ->save(storage_path() . '/app/public/items/100-' . $item_file_name)
                  ->resize(200, null, function ($constraint) {$constraint->aspectRatio();})
                  ->save(storage_path() . '/app/public/items/200-' . $item_file_name);

        } else {
            if(!empty($request['item_del_filename'])) {
                $item_file_name = null;
                //アップロードされている画像ファイルを削除
//                File::delete(storage_path() . '/app/public/items/'.$request['item_del_filename']);
//                File::delete(storage_path() . '/app/public/items/100-'.$request['item_del_filename']);
//                File::delete(storage_path() . '/app/public/items/200-'.$request['item_del_filename']);
            } else {
//                $item_file_name = $request['item_db_filename'];
            }
        }

        // </editor-fold>

        //入力項目のセット
        $table->vehicles_id         = $id;    				// メーカーID
        $table->vehicle_file_name   = $item_file_name;      // 画像ファイル名（格納パスはConst定義）
        $table->upd_user_name       = $charge->user_name;   // 更新者

        // 登録更新処理
        $ret = $table->save();

        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return redirect('vehicle');
    }

    /**
     * 登録更新画面へ遷移
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function imagedelete($id=null) {

        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge  = new ChargeInfo;

        // リクエスト
        $request            = Request::all();
        $id                 = Funcs::rq('id',    $request);
        $vehicle_file_names = Funcs::rq('name',  $request);

        if(!empty($id)){
            $data = Images::where('vehicles_id','=', $id)->where('vehicle_file_name','=', $vehicle_file_names)->first();
            if(empty($data)) {
                Session::flash('flash_danger', '削除情報が存在しませんでした。');
            }else{
                $data->delete();                            // 論理削除処理
//                $data->upd_user_name = $charge->user_name;  // 更新者
//                $data->save();

                // 正常終了メッセージ
//                Session::flash('flash_success', "削除が完了しました。");
            }
		}

        // 画像ファイルを削除
        File::delete(storage_path() . '/app/public/items/'.$vehicle_file_names);
        File::delete(storage_path() . '/app/public/items/100-'.$vehicle_file_names);
        File::delete(storage_path() . '/app/public/items/200-'.$vehicle_file_names);

    }

    /**
     * 論理削除処理
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // ログイン情報
        $charge = new ChargeInfo;
        $dt_imgs = array();

        if(!empty($id)){
            $data = Vehicles::find($id);
            if(empty($data)) {
                Session::flash('flash_danger', '削除情報が存在しませんでした。');
            }else{
                // メイン画像ファイル群を物理削除
                File::delete(storage_path() . '/app/public/items/'.$data['main_file_names']);
                File::delete(storage_path() . '/app/public/items/100-'.$data['main_file_names']);
                File::delete(storage_path() . '/app/public/items/200-'.$data['main_file_names']);

                // パーツ画像ファイル群を「物理」削除
                $dt_imgs = Images::where('vehicles_id', '=', $id)->get();
                if ( !$dt_imgs->isEmpty() ) {
                    foreach ( $dt_imgs as $row ) {
                        $vehicle_file_name = $row->vehicle_file_name;
                        File::delete(storage_path() . '/app/public/items/'.$vehicle_file_name);
                        File::delete(storage_path() . '/app/public/items/100-'.$vehicle_file_name);
                        File::delete(storage_path() . '/app/public/items/200-'.$vehicle_file_name);
                    }
                }

                // パーツ画像ファイル群を「論理」削除
                Images::where('vehicles_id', '=', $id)->delete();

                // 車両論理削除
                $data->delete();                            // 論理削除処理
                $data->upd_user_name = $charge->user_name;  // 更新者
                $data->save();

                // 正常終了メッセージ
                Session::flash('flash_success', "削除が完了しました。");
            }
		}

        // 一覧に戻す
        return redirect('vehicle');
    }

    /**
     * 車両パーツ一覧
     *
     * @return Response
     */
    public function parts($id=null)
    {
        /*-----------------------------------------
         * 1. 初期化
         *----------------------------------------- */
        // 1.1 データ受け渡し用
        $data = [];
        $data['results'] = [];
        $models_list = array();
        $vehicles_id = $id; //車両ID

        /*-----------------------------------------
         * 2. 登録更新データの取得
         *----------------------------------------- */
        // <editor-fold defaultstate="collapsed" desc=" 登録更新データの取得">
        $hasold = !empty(Input::old());
        if(!$hasold){
            if (empty($id))
            {
                /*---------------------------
                 * 2.1 URLの引数IDがない場合
                 *---------------------------*/
                // 正常終了メッセージ
//                Session::flash('flash_danger', "削除が完了しました。");
                return redirect('vehicle');
            }
            else
            {
                /*---------------------------
                 * 2.2 修正
                 *---------------------------*/
                $query = Parts::select('vehicles_parts.id'
                                      ,'vehicles_parts.vehicles_id'
                                      ,'vehicles_parts.parts_status'
                                      ,'vehicles_parts.remarks'
                                      ,'parts.id AS parts_id'
                                      ,'parts.parts_name'
                        );
                $query->leftjoin('vehicles_parts', function($join) use($id) {
                                     $join->on('vehicles_parts.parts_id','=', 'parts.id');
                                     $join->on('vehicles_parts.vehicles_id','=', DB::raw("'$id'"));
                                });
                $query->orderByRaw('parts.display_no ASC');
//                $query->orderByRaw('parts.display_no IS NULL ASC');

                $data = $query->get();
            }

        }
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return view('vehicle.parts_edit', compact('data', 'vehicles_id', 'vehicles_id'));

    }

    /**
     * 登録更新処理
     *
     * @return Response
     */
    public function saveparts()
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge  = new ChargeInfo;

        // リクエスト
        $request        = Request::all();
        $arry_id        = Funcs::rq('id',       $request);
        $arry_parts_id  = Funcs::rq('parts_id', $request);
        $arry_remarks   = Funcs::rq('remarks',  $request);
        $arry_parts_status   = Funcs::rq('parts_status',  $request);

        // 結果格納用
        $data['results'] = [];

        /* ----------------------------------------------
         * 2. DB登録
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 2. DB登録">
        $i = 1;
        foreach ( $arry_id as $id ) {
            $save_flg = false;

            if (empty($id)) {
                /* ----------------------------------------
                 * 2.1 新規
                 * ----------------------------------------*/
                if ($arry_parts_status[$i] != 1) {
                    $table = new VehiclesParts();
                    $table->add_user_name = $charge->user_name;    // 登録者
                }else{
                    //更新しない
                    $save_flg = true;
                }

            }else{
                //存在チェック
                $isnew = empty(VehiclesParts::find($id));
                if( $isnew ){
                    /* ----------------------------------------
                     * 2.1 新規
                     * ----------------------------------------*/
                    $table = new VehiclesParts();
                    $table->add_user_name = $charge->user_name;    // 登録者
                }else{
                    /* ----------------------------------------
                     * 2.2 更新
                     * ----------------------------------------*/
                    $table = VehiclesParts::find($id);
                }
            }

            if (!$save_flg) {
                //入力項目のセット
                $table->vehicles_id   	= $request['vehicles_id'];          // 車両ID
                $table->parts_id   		= $arry_parts_id[$i];               // 車両パーツID
                $table->parts_status   	= $arry_parts_status[$i];           // パーツステータス（1：-、2：OK、3：NG）
                $table->remarks   		= $arry_remarks[$i];                // 備考
                $table->upd_user_name   = $charge->user_name;               // 更新者

                // 登録更新処理
                $table->save();
            }

            $i++;
        }

        // 正常終了メッセージ
        Session::flash('flash_success', "登録が完了しました。");
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return redirect('vehicle');
    }

    /**
     * 西暦⇔和暦変換
     *
     * @return Response
     */
    function convGtJDate($src)
    {
        list($year, $month, $day) = explode('/', $src);
        if (!@checkdate($month, $day, $year) || $year < 1869 || strlen($year) !== 4
                || strlen($month) !== 2 || strlen($day) !== 2) return false;
        $date = str_replace('/', '', $src);
        if ($date >= 20190501) {
            $gengo = '令和';
            $wayear = $year - 2018;
        }elseif ($date >= 19890108) {
            $gengo = '平成';
            $wayear = $year - 1988;
        } elseif ($date >= 19261225) {
            $gengo = '昭和';
            $wayear = $year - 1925;
        } elseif ($date >= 19120730) {
            $gengo = '大正';
            $wayear = $year - 1911;
        } else {
            $gengo = '明治';
            $wayear = $year - 1868;
        }
        switch ($wayear) {
            case 1:
//                $wadate = $gengo.'元年'.$month.'月'.$day.'日';
                $wadate = $gengo.'元年';
                break;
            default:
//                $wadate = $gengo.sprintf("%02d", $wayear).'年'.$month.'月'.$day.'日';
                $wadate = $gengo.sprintf("%02d", $wayear).'年';
        }
        return $wadate;
    }

}
