<?php

namespace partsmanagement\Http\Controllers\Api\Master;

use Request;
use partsmanagement\Http\Controllers\Controller;
use partsmanagement\Models\Items;
use partsmanagement\Libs\Funcs;

class ItemController extends Controller
{
    public function json($id) {
        return Items::find($id);
    }

    public function search($key){
         $function = sprintf('_search_for_%s', $key);
        if(method_exists($this,$key)){
            return $this->$function();
        } else {
            try {
                return $this->$function();
            }catch (Exception $e){
                return;
            }
        }
        return ;
    }
    
    private function _search_for_catalog(){
        // GETパラメタ
        $request = Request::all();
        // ビルダ
        $query = Items::select('*');
        // 並び順
        $query->orderBy('item_name_kana','ASC')
              ->orderBy('item_name', 'ASC')
              ->orderBy('updated_at', 'DESC')
        ;
        // 検索条件
        if(!empty(Funcs::vl('finditem_keyword', $request))){
            // カナ or 名前
            $query->where(function($q) use ($request) {
                $q->where('item_name_kana','LIKE',sprintf('%%%s%%', Funcs::vl('finditem_keyword', $request)));
                $q->orWhere('item_name','LIKE',sprintf('%%%s%%', Funcs::vl('finditem_keyword', $request)));
            });
        }
        // VIEWを返す
        return view('item.modal.item_list')->with('result', $query->get());
    }


}
