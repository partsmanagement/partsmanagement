<?php

namespace partsmanagement\Http\Controllers\Api\Master;

// use Illuminate\Http\Request;
use Request;
use partsmanagement\Http\Controllers\Controller;
use partsmanagement\Models\CustomerDetails;
use partsmanagement\Libs\Funcs;

class CustomerDetailsController extends Controller
{
    /**
    * @remark Routeでも対応可能であるが、Routeが複雑、肥大する事をさけるため
    * @remark Apiを作れば良いかも？Todo
    */
    public function json($id) {
        // GETパラメタ
        $request = Request::all();
        
        if(!empty(Funcs::vl('customer_id', $request))){
            $data = CustomerDetails::where('id',$id)->where('customer_id','=', Funcs::vl('customer_id', $request))->first();
            if(empty($data)) {
                return array();
            } else {
                return $data;
            }
        } else {
            return CustomerDetails::where('id',$id)->first();
        }
    }

    public function json_root($id) {
        $query = CustomerDetails::select(
                                'customer_details.*'
                               ,'customers.customer_name'   // 得意先名
                    );
        $query->leftJoin('customers', 'customer_details.customer_id','=', 'customers.id');
        $query->where('customer_details.id', '=', $id);
        $data = $query->first();

        return $data;
    }

    public function json_cust($id) {
        // GETパラメタ
        $request = Request::all();
        
        $query = CustomerDetails::select('*');
        $query->where('customer_details.id', '=', $id);
        $query->where('customer_details.customer_id','=', Funcs::vl('customer_id', $request));
        $data = $query->first();

        if(is_null($data)) {
            return array();
        } else {
            return $data;
        }

    }
    
    public function search($key){
         $function = sprintf('_search_for_%s', $key);
        if(method_exists($this,$key)){
            return $this->$function();
        } else {
            try {
                return $this->$function();
            }catch (Exception $e){
                return;
            }
        }
        return ;
    }
    private function _search_for_catalog(){
        // GETパラメタ
        $request = Request::all();
        // ビルダ
        $query = CustomerDetails::select('*');
        // 並び順
        $query->orderBy('customer_detail_name_kana','ASC')  //得意先詳細名カナ
              ->orderBy('customer_detail_name', 'ASC')      //得意先詳細名
              ->orderBy('updated_at', 'DESC')               //更新日（降順）
        ;
        
        if(!empty(Funcs::vl('customer_id', $request))){
            $query->where('customer_details.customer_id','=', Funcs::vl('customer_id', $request));
        }

        // 検索条件
        if(!empty(Funcs::vl('findcustomer_keyword', $request))){
            // カナ or 名前
            $query->where(function($q) use ($request) {
                $q->where('customer_detail_name_kana','LIKE',sprintf('%%%s%%', Funcs::vl('findcustomer_keyword', $request)));
                $q->orWhere('customer_detail_name','LIKE',sprintf('%%%s%%', Funcs::vl('findcustomer_keyword', $request)));
            });
        }
        // VIEWを返す
        return view('customer_details.modal.customer_details_list')->with('result', $query->get());
    }

    private function _search_for_dept(){
        // GETパラメタ
        $request = Request::all();
        // ビルダ
        $query = CustomerDetails::select('*');
        // 並び順
        $query->orderBy('customer_detail_name_kana','ASC')  //得意先詳細名カナ
              ->orderBy('customer_detail_name', 'ASC')      //得意先詳細名
              ->orderBy('updated_at', 'DESC')               //更新日（降順）
        ;
        
        if(!empty(Funcs::vl('customer_id', $request))){
            $query->where('customer_details.customer_id','=', Funcs::vl('customer_id', $request));
        }
        
        // 検索条件
        if(!empty(Funcs::vl('findcustomer_keyword', $request))){
            // カナ or 名前
            $query->where(function($q) use ($request) {
                $q->where('customer_detail_name_kana','LIKE',sprintf('%%%s%%', Funcs::vl('findcustomer_keyword', $request)));
                $q->orWhere('customer_detail_name','LIKE',sprintf('%%%s%%', Funcs::vl('findcustomer_keyword', $request)));
            });
        }
        // VIEWを返す
        return view('customer_details.modal.customer_details_list')->with('result', $query->get());
    }

}
