<?php

namespace partsmanagement\Http\Controllers\Api\Master;

use Request;
use partsmanagement\Http\Controllers\Controller;
use partsmanagement\Models\Items;
use partsmanagement\Models\CustomerItemPrices;
use partsmanagement\Libs\Funcs;
use partsmanagement\Libs\ChargeInfo;

class CustomerItemPricesController extends Controller
{
    public function json($id) {
        // ログイン情報
        $charge = new ChargeInfo;
        $user_type = $charge->user_type;

        // GETパラメタ
        $request = Request::all();

        $query = CustomerItemPrices::select(
             'items.id'                                                                     // 商品ID
            ,'items.item_code'                                                              // 商品コード
            ,'items.item_name'                                                              // 商品名
            ,'items.item_name_kana'                                                         // 商品名カナ
            ,'items.item_file_name'                                                         // 商品画像ファイル名（格納パスはConst定義）
            ,'items.item_method'                                                            // 商品規格
            ,'items.item_quantity'                                                          // 商品入数
            ,'items.item_quantity_unit'                                                     // 商品入数単位
            ,'items.item_unit'                                                              // 商品単位
            ,'items.item_disp_type'                                                         // 商品表示区分（1：表示、99：非表示）
            ,'customer_item_prices.cust_wholesale_rank_price AS wholesale_basic_price'      // 得意先別卸売価格
            ,'customer_item_prices.cust_retail_rank_price AS retail_basic_price'            // 得意先別小売価格
            ,'customer_item_prices.cust_wholesale_sale_price AS wholesale_sale_price'       // 得意先別卸売特売価格
            ,'customer_item_prices.cust_retail_sale_price AS retail_sale_price'             // 得意先別小売特売価格
        );

        // 得意先ユーザでログイン
        if($user_type == 3) {
            $query->join('items', function ($join) {
                $join->on('customer_item_prices.item_id', '=', 'items.id');
                $join->where('items.item_disp_type', '=', 1);
            });
        } else {
            $query->join('items','customer_item_prices.item_id','=','items.id');
        }

        if(!empty(Funcs::vl('customer_detail_id', $request))){
            $query->where('customer_item_prices.customer_detail_id','=', Funcs::vl('customer_detail_id', $request));
        }

        $query->where('customer_item_prices.item_id','=', $id);
        
        $custitem_data = $query->get();

        if(count($custitem_data) > 0) {
            return $custitem_data[0];
        } else {
            $query = Items::select('*');
            $query->where('items.id','=', $id);
            // 得意先ユーザでログイン
            if($user_type == 3) {
                $query->where('items.item_disp_type', '=', 1);
            }
            $item_data = $query->get();
            if(count($item_data)> 0) {
                return $item_data[0];
            } else {
                return array();
            }
        }
        
    }

    public function jsonitemcd() {
        // ログイン情報
        $charge = new ChargeInfo;
        $user_type = $charge->user_type;

        // GETパラメタ
        $request = Request::all();
    
        $query = CustomerItemPrices::select(
             'items.id'                                                                     // 商品ID
            ,'items.item_code'                                                              // 商品コード
            ,'items.item_name'                                                              // 商品名
            ,'items.item_name_kana'                                                         // 商品名カナ
            ,'items.item_file_name'                                                         // 商品画像ファイル名（格納パスはConst定義）
            ,'items.item_method'                                                            // 商品規格
            ,'items.item_quantity'                                                          // 商品入数
            ,'items.item_quantity_unit'                                                     // 商品入数単位
            ,'items.item_unit'                                                              // 商品単位
            ,'items.item_disp_type'                                                         // 商品表示区分（1：表示、99：非表示）
            ,'customer_item_prices.cust_wholesale_rank_price AS wholesale_basic_price'      // 得意先別卸売価格
            ,'customer_item_prices.cust_retail_rank_price AS retail_basic_price'            // 得意先別小売価格
            ,'customer_item_prices.cust_wholesale_sale_price AS wholesale_sale_price'       // 得意先別卸売特売価格
            ,'customer_item_prices.cust_retail_sale_price AS retail_sale_price'             // 得意先別小売特売価格
        );

        // 得意先ユーザでログイン
        if($user_type == 3) {
            $query->join('items', function ($join) {
                $join->on('customer_item_prices.item_id', '=', 'items.id');
                $join->where('items.item_disp_type', '=', 1);
            });
        } else {
            $query->join('items','customer_item_prices.item_id','=','items.id');
        }


        if(!empty(Funcs::vl('customer_detail_id', $request))){
            $query->where('customer_item_prices.customer_detail_id','=', Funcs::vl('customer_detail_id', $request));
        }
        if(!empty(Funcs::vl('item_cd', $request))){
            $query->where('items.item_code','=', Funcs::vl('item_cd', $request));
        }

        $custitem_data = $query->get();

        if(count($custitem_data) > 0) {
            return $custitem_data[0];
        } else {
            $query = Items::select('*');
            $query->where('items.item_code','=', Funcs::vl('item_cd', $request));
            // 得意先ユーザでログイン
            if($user_type == 3) {
                $query->where('items.item_disp_type', '=', 1);
            }
            $item_data = $query->get();
            if(count($item_data)> 0) {
                return $item_data[0];
            } else {
                return array();
            }
        }
    }
    
    public function search($key){
         $function = sprintf('_search_for_%s', $key);
        if(method_exists($this,$key)){
            return $this->$function();
        } else {
            try {
                return $this->$function();
            }catch (Exception $e){
                return;
            }
        }
        return ;
    }
    
    private function _search_for_catalog(){
        // ログイン情報
        $charge = new ChargeInfo;
        $user_type = $charge->user_type;

        // GETパラメタ
        $request = Request::all();
        $custitem_key = array();
        $resultdata = array();

        $query = CustomerItemPrices::select('items.id');
        $query->join('customer_details','customer_item_prices.customer_detail_id','=','customer_details.id');
        $query->join('items','customer_item_prices.item_id','=','items.id');
        $query->where('customer_details.customer_id','=', Funcs::vl('customer_id', $request));
        $custitem_data_check = $query->get();

        if(count($custitem_data_check) > 0) {
            $query = CustomerItemPrices::select(
                 'items.id'                                             // 商品ID
                ,'items.item_code'                                      // 商品コード
                ,'items.item_name'                                      // 商品名
                ,'items.item_name_kana'                                 // 商品名カナ
                ,'items.item_file_name'                                 // 商品画像ファイル名（格納パスはConst定義）
                ,'items.item_method'                                    // 商品規格
                ,'items.item_quantity'                                  // 商品入数
                ,'items.item_quantity_unit'                             // 商品入数単位
                ,'items.item_unit'                                      // 商品単位
                ,'items.item_disp_type'                                 // 商品表示区分（1：表示、99：非表示）
                ,'customer_item_prices.cust_wholesale_rank_price'       // 得意先別卸売価格
                ,'customer_item_prices.cust_retail_rank_price'          // 得意先別小売価格
                ,'customer_item_prices.cust_wholesale_sale_price'       // 得意先別卸売特売価格
                ,'customer_item_prices.cust_retail_sale_price'          // 得意先別小売特売価格
            );
            $query->join('customer_details','customer_item_prices.customer_detail_id','=','customer_details.id');
            
            // 得意先ユーザでログイン
            if($user_type == 3) {
                $query->join('items', function ($join) {
                    $join->on('customer_item_prices.item_id', '=', 'items.id');
                    $join->where('items.item_disp_type', '=', 1);
                });
            } else {
                $query->join('items','customer_item_prices.item_id','=','items.id');
            }

            $query->where('customer_details.customer_id','=', Funcs::vl('customer_id', $request));
            $query->where('customer_item_prices.customer_detail_id','=', Funcs::vl('customer_detail_id', $request));

            // 検索条件
            if(!empty(Funcs::vl('finditem_keyword', $request))){
                // カナ or 名前
                $query->where(function($q) use ($request) {
                    $q->where('items.item_name_kana','LIKE',sprintf('%%%s%%', Funcs::vl('finditem_keyword', $request)));
                    $q->orWhere('items.item_name','LIKE',sprintf('%%%s%%', Funcs::vl('finditem_keyword', $request)));
                });
            }
            
            // 並び順
            $query->orderBy('items.item_name_kana','ASC')
                  ->orderBy('items.item_name', 'ASC')
                  ->orderBy('items.item_code', 'ASC')
                  ->orderBy('items.updated_at', 'DESC');

            $custitem_data = $query->get();

            if(count($custitem_data) > 0) {
                foreach($custitem_data as $key=>$val) {
                    //$custitem_key[] = $val['id'];
                    $resultdata[$key]['id'] = $val['id'];
                    $resultdata[$key]['item_code'] = $val['item_code'];
                    $resultdata[$key]['item_name'] = $val['item_name'];
                    $resultdata[$key]['item_name_kana'] = $val['item_name_kana'];
                    $resultdata[$key]['item_file_name'] = $val['item_file_name'];
                    $resultdata[$key]['item_method'] = $val['item_method'];
                    $resultdata[$key]['item_quantity'] = $val['item_quantity'];
                    $resultdata[$key]['item_quantity_unit'] = $val['item_quantity_unit'];
                    $resultdata[$key]['item_unit'] = $val['item_unit'];
                    $resultdata[$key]['item_disp_type'] = $val['item_disp_type'];
                    $resultdata[$key]['cust_wholesale_rank_price'] = $val['cust_wholesale_rank_price'];
                    $resultdata[$key]['cust_retail_rank_price'] = $val['cust_retail_rank_price'];
                    $resultdata[$key]['cust_wholesale_sale_price'] = $val['cust_wholesale_sale_price'];
                    $resultdata[$key]['cust_retail_sale_price'] = $val['cust_retail_sale_price'];
                }
                // VIEWを返す
                return view('customer_item_prices.modal.custitem_list')->with('result', $resultdata);
            }
        }

        $query = Items::select('*');
        
        // 得意先ユーザでログイン
        if($user_type == 3) {
            $query->where('items.item_disp_type', '=', 1);
        }

        // 検索条件
        if(!empty(Funcs::vl('finditem_keyword', $request))){
            // カナ or 名前
            $query->where(function($q) use ($request) {
                $q->where('item_name_kana','LIKE',sprintf('%%%s%%', Funcs::vl('finditem_keyword', $request)));
                $q->orWhere('item_name','LIKE',sprintf('%%%s%%', Funcs::vl('finditem_keyword', $request)));
            });
        }

        // 並び順
        $query->orderBy('items.item_code', 'ASC');
        $query->orderBy('items.item_name_kana','ASC');
        $query->orderBy('items.item_name', 'ASC');
        $query->orderBy('items.updated_at', 'DESC');
        
        $item_data = $query->get();
        if(count($item_data) > 0) {
            foreach($item_data as $key=>$val) {
                $resultdata[$key]['id'] = $val['id'];
                $resultdata[$key]['item_code'] = $val['item_code'];
                $resultdata[$key]['item_name'] = $val['item_name'];
                $resultdata[$key]['item_name_kana'] = $val['item_name_kana'];
                $resultdata[$key]['item_file_name'] = $val['item_file_name'];
                $resultdata[$key]['item_method'] = $val['item_method'];
                $resultdata[$key]['item_quantity'] = $val['item_quantity'];
                $resultdata[$key]['item_quantity_unit'] = $val['item_quantity_unit'];
                $resultdata[$key]['item_unit'] = $val['item_unit'];
                $resultdata[$key]['item_disp_type'] = $val['item_disp_type'];
                $resultdata[$key]['cust_wholesale_rank_price'] = $val['wholesale_basic_price'];
                $resultdata[$key]['cust_retail_rank_price'] = $val['retail_basic_price'];
                $resultdata[$key]['cust_wholesale_sale_price'] = $val['wholesale_sale_price'];
                $resultdata[$key]['cust_retail_sale_price'] = $val['retail_sale_price'];
            }
        }

        
        // VIEWを返す
        return view('customer_item_prices.modal.custitem_list')->with('result', $resultdata);
    }


}
