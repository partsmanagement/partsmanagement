<?php

namespace partsmanagement\Http\Controllers\Api\Master;

// use Illuminate\Http\Request;
use Request;
use partsmanagement\Http\Controllers\Controller;
use partsmanagement\Models\Customers;
use partsmanagement\Libs\Funcs;

class CustomerController extends Controller
{
    /**
    * @remark Routeでも対応可能であるが、Routeが複雑、肥大する事をさけるため
    * @remark Apiを作れば良いかも？Todo
    */
    public function json($id) {
        $data = Customers::where('id',$id)->first();
        if(is_null($data)) {
            return array();
        } else {
            return $data;
        }
    }

    public function search($key){
         $function = sprintf('_search_for_%s', $key);
        if(method_exists($this,$key)){
            return $this->$function();
        } else {
            try {
                return $this->$function();
            }catch (Exception $e){
                return;
            }
        }
        return ;
    }
    private function _search_for_catalog(){
        // GETパラメタ
        $request = Request::all();
        // ビルダ
        $query = Customers::select('*');
        // 並び順
        $query->orderBy('customer_name_kana','ASC')  //得意先名カナ
              ->orderBy('customer_name', 'ASC')      //得意先名
              ->orderBy('updated_at', 'DESC')        //更新日（降順）
        ;
        // 検索条件
        if(!empty(Funcs::vl('findcustomer_keyword', $request))){
            // カナ or 名前
            $query->where(function($q) use ($request) {
                $q->where('customer_name_kana','LIKE',sprintf('%%%s%%', Funcs::vl('findcustomer_keyword', $request)));
                $q->orWhere('customer_name','LIKE',sprintf('%%%s%%', Funcs::vl('findcustomer_keyword', $request)));
            });
        }
        // VIEWを返す
        return view('customer.modal.customer_list')->with('result', $query->get());
    }

    private function _search_for_dept(){
        // GETパラメタ
        $request = Request::all();
        // ビルダ
        $query = Customers::select('*');
        // 並び順
        $query->orderBy('customer_name_kana','ASC')  //得意先名カナ
              ->orderBy('customer_name', 'ASC')      //得意先名
              ->orderBy('updated_at', 'DESC')        //更新日（降順）
        ;
        // 検索条件
        if(!empty(Funcs::vl('findcustomer_keyword', $request))){
            // カナ or 名前
            $query->where(function($q) use ($request) {
                $q->where('customer_name_kana','LIKE',sprintf('%%%s%%', Funcs::vl('findcustomer_keyword', $request)));
                $q->orWhere('customer_name','LIKE',sprintf('%%%s%%', Funcs::vl('findcustomer_keyword', $request)));
            });
        }
        // VIEWを返す
        return view('customer.modal.customer_list')->with('result', $query->get());
    }

    private function _search_for_claim(){
        // GETパラメタ
        $request = Request::all();
        // ビルダ
        $query = Customers::select('*');
        // 並び順
        $query->orderBy('customer_name_kana','ASC')  //得意先名カナ
              ->orderBy('customer_name', 'ASC')      //得意先名
              ->orderBy('updated_at', 'DESC')        //更新日（降順）
        ;
        // 検索条件
        if(!empty(Funcs::vl('findcustomerclaim_keyword', $request))){
            // カナ or 名前
            $query->where(function($q) use ($request) {
                $q->where('customer_name_kana','LIKE',sprintf('%%%s%%', Funcs::vl('findcustomerclaim_keyword', $request)));
                $q->orWhere('customer_name','LIKE',sprintf('%%%s%%', Funcs::vl('findcustomerclaim_keyword', $request)));
            });
        }
        // VIEWを返す
        return view('customer.modal.customer_claim_list')->with('result', $query->get());
    }

}
