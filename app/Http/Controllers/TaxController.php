<?php

namespace partsmanagement\Http\Controllers;

//use Illuminate\Http\Request;
use partsmanagement\Http\Requests\FormTaxRequest;
use partsmanagement\Libs\Funcs;
use partsmanagement\Libs\ChargeInfo;
use partsmanagement\Models\Taxes;

use Config;
use Carbon\Carbon;
use DB;
use Input;
use Request;
use Response;
use Session;
use Validator;


class TaxController extends Controller
{
    /**
     * コンストラクタ
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 初期表示
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->search();
    }

    /**
     * 検索
     *
     * @return Response
     */
    public function search()
    {
        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */

        /* ------------------------------------------------------
         * データ取得
         *------------------------------------------------------ */
        //リクエスト取得
        $prms = Request::all();

        //データ取得
        $results = $this->getData($prms);

        /* ---------------------------
         * VIEW
         *--------------------------- */
        return view('tax.index', compact('results', 'prms'));
    }
    /**
     * SQLの生成
     * 一覧データの取得
     * @return Response
     */
	public function getData($prms)
	{
        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */
        // リクエスト取得
        $prms = Request::all();
		$data['results'] = null;

        /* ------------------------------------------------------
         * 一覧データの取得
         *------------------------------------------------------ */
        // <editor-fold defaultstate="collapsed" desc=" クエリ">
        $query = Taxes::select(
                              'id'                          //税率ID
                             ,'const_date'					//施工日
                             ,'tax_basic_rate'				//消費税率
                             ,'reduction_tax_rate'          //軽減税率
                             ,'tax_flag'				    //税区分（1:外税、2:内税）
                             ,'tax_calc_flag'				//税計算区分（1:伝票単位、2:商品単位）
                             ,'tax_fraction_type'			//税端数計算区分（1:切捨て、2:四捨五入、3:切り上げ）
                    );

        // 施工日
        if(!empty(Funcs::vl('const_date', $prms))){
            $query->whereYear('const_date', substr($prms['const_date'], 0, 4));
            $query->whereMonth('const_date', substr($prms['const_date'], 5, 2));
        }

        // 消費税率
        if(!empty(Funcs::vl('tax_basic_rate', $prms))){
            $query->where('tax_basic_rate','like','%'.Funcs::vl('tax_basic_rate', $prms).'%');
        }

        // 軽減税率
        if(!empty(Funcs::vl('reduction_tax_rate', $prms))){
            $query->where('reduction_tax_rate','like','%'.Funcs::vl('reduction_tax_rate', $prms).'%');
        }

        // 税区分
        if(!empty(Funcs::vl('tax_flag', $prms))){
            $query->where('tax_flag','=',Funcs::vl('tax_flag', $prms));
        }

        // 税計算区分
        if(!empty(Funcs::vl('tax_calc_flag', $prms))){
            $query->where('tax_calc_flag','=',Funcs::vl('tax_calc_flag', $prms));
        }

        // 税端数計算区分
        if(!empty(Funcs::vl('tax_fraction_type', $prms))){
            $query->where('tax_fraction_type','=',Funcs::vl('tax_fraction_type', $prms));
        }

        $query->orderBy('id','asc');

        $data = $query->paginate(Config::get('const.default_maxrowsize'))->appends($prms);
        // </editor-fold>

		return $data;
    }

    /**
     * 登録更新画面へ遷移
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id=null)
    {
        /*-----------------------------------------
         * 1. 初期化
         *----------------------------------------- */
        // 1.1 データ受け渡し用
        $data = [];
        $data['results'] = [];

        /*-----------------------------------------
         * 2. 登録更新データの取得
         *----------------------------------------- */
        // <editor-fold defaultstate="collapsed" desc=" 登録更新データの取得">
        $hasold = !empty(Input::old());
        if(!$hasold){
            if (empty($id))
            {
                /*---------------------------
                 * 2.1 新規
                 *---------------------------*/

            }
            else
            {
                /*---------------------------
                 * 2.2 修正
                 *---------------------------*/
                /* ===========================
                 * 2.2.1 初期表示データの取得
                 * =========================== */
                // <editor-fold defaultstate="collapsed" desc=" 2.2.1 初期表示データの取得">
                $results = Taxes::find( $id );
                if(empty( $results )) {
                    // 該当がない場合、エラーにして一覧へ戻す
                    Session::flash('flash_danger', '税率情報が存在しませんでした。');
                    return redirect('tax'); //一覧へ戻す
                }
                $data['results']['id']                  = $results['id'];                       // 税率ID
                $data['results']['const_date']          = $results['const_date'];               // 施工日
                $data['results']['tax_basic_rate']      = $results['tax_basic_rate'];           // 消費税率
                $data['results']['reduction_tax_rate']  = $results['reduction_tax_rate'];       // 軽減税率
                $data['results']['tax_flag']            = $results['tax_flag'];                 // 税区分（1:外税、2:内税）
                $data['results']['tax_calc_flag']       = $results['tax_calc_flag'];            // 税計算区分（1:伝票単位、2:商品単位）
                $data['results']['tax_fraction_type']   = $results['tax_fraction_type'];        // 税端数計算区分（1:切捨て、2:四捨五入、3:切り上げ）
                // </editor-fold>
            }

        }
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return view('tax.edit', compact('data'));

    }

    /**
     * 登録更新処理
     *
     * @return Response
     */
    public function save()
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge  = new ChargeInfo;

        // リクエスト
        $request = Request::all();
        $id      = Funcs::rq('id', $request);

        // 結果格納用
        $data['results'] = array();

        /* ----------------------------------------------
         * 2. バリデーション
         * ----------------------------------------------*/
        $validator = Validator::make( $request, FormTaxRequest::rules(), FormTaxRequest::messages() );

        // バリデーションエラーだった場合
        if ( $validator->fails() ) {
            return view('tax.edit', compact('data'))->withErrors($validator);
        }

        /* ----------------------------------------------
         * 3. DB登録
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3. DB登録">
        //存在チェック
        $isnew = empty(Taxes::find($id));
        if( $isnew ){
            /* ----------------------------------------
             * 4.1 新規
             * ----------------------------------------*/
            $table = new Taxes();
            $table->add_user_name = $charge->user_name;    // 登録者
            $edit_message = "登録";
        }else{
            /* ----------------------------------------
             * 4.2 更新
             * ----------------------------------------*/
            $table = Taxes::find($id);
            $edit_message = "更新";
        }
        //入力項目のセット
        $table->const_date          = $request['const_date'];               // 施工日
        $table->tax_basic_rate      = $request['tax_basic_rate'];           // 消費税率
        $table->reduction_tax_rate  = $request['reduction_tax_rate'];       // 軽減税率
        $table->tax_flag            = $request['tax_flag'];                 // 税区分（1:外税、2:内税）
        $table->tax_calc_flag       = $request['tax_calc_flag'];            // 税計算区分（1:伝票単位、2:商品単位）
        $table->tax_fraction_type   = $request['tax_fraction_type'];        // 税端数計算区分（1:切捨て、2:四捨五入、3:切り上げ）
        $table->upd_user_name       = $charge->user_name;                   // 更新者

        // 登録更新処理
        $ret = $table->save();

        // 正常終了メッセージ
        Session::flash('flash_success', $edit_message."が完了しました。");
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return redirect('tax');
    }

    /**
     * 論理削除処理
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // ログイン情報
        $charge = new ChargeInfo;

        if(!empty($id)){
            $data = Taxes::find($id);
            if(empty($data)) {
                Session::flash('flash_danger', '削除情報が存在しませんでした。');
            }else{
                $data->delete();                            // 論理削除処理
                $data->upd_user_name = $charge->user_name;  // 更新者
                $data->save();

                // 正常終了メッセージ
                Session::flash('flash_success', "削除が完了しました。");
            }
		}

        // 一覧に戻す
        return redirect('tax');
    }
}
