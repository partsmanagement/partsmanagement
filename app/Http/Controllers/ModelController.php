<?php

namespace partsmanagement\Http\Controllers;

//use Illuminate\Http\Request;
use partsmanagement\Http\Requests\FormModels;
use partsmanagement\Libs\Funcs;
use partsmanagement\Libs\ChargeInfo;
use partsmanagement\Models\Manufacturers;
use partsmanagement\Models\Models;
use partsmanagement\Models\Taxes;

use Config;
use Carbon\Carbon;
use DB;
use Input;
use Request;
use Response;
use Session;
use Validator;


class ModelController extends Controller
{
    /**
     * コンストラクタ
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 初期表示
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->search();
    }

    /**
     * 検索
     *
     * @return Response
     */
    public function search()
    {
        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */

        /* ------------------------------------------------------
         * データ取得
         *------------------------------------------------------ */
        //リクエスト取得
        $prms = Request::all();

        // メーカー：プルダウン
        $query = Manufacturers::orderBy('manufacturer_name','asc');
        $manufacturer_list = $query->pluck('manufacturer_name', 'id');
        $manufacturer_list = $manufacturer_list->prepend('', '');

        //データ取得
        $results = $this->getData($prms);

        /* ---------------------------
         * VIEW
         *--------------------------- */
        return view('model.index', compact('results', 'prms', 'manufacturer_list'));
    }
    /**
     * SQLの生成
     * 一覧データの取得
     * @return Response
     */
	public function getData($prms)
	{
        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */
        // リクエスト取得
        $prms = Request::all();
		$data['results'] = null;

        /* ------------------------------------------------------
         * 一覧データの取得
         *------------------------------------------------------ */
        // <editor-fold defaultstate="collapsed" desc=" クエリ">
        $query = Models::select('*',
                                'models.id',
                                'manufacturers.manufacturer_name'
                );
        $query->leftjoin('manufacturers', 'manufacturers.id', '=', 'models.manufacturer_id');

        // メーカー名
        if(!empty(Funcs::vl('model_name', $prms))){
            $query->where('model_name','like','%'.Funcs::vl('model_name', $prms).'%');
        }

        // 車種名カナ
        if(!empty(Funcs::vl('model_kana', $prms))){
            $query->where('model_kana','like','%'.Funcs::vl('model_kana', $prms).'%');
        }

        // メーカー
        if(!empty(Funcs::vl('manufacturer_id', $prms))){
            $query->where('manufacturer_id','=', Funcs::vl('manufacturer_id', $prms));
        }

        $query->orderBy('models.model_name','asc');

        $data = $query->paginate(Config::get('const.default_30rows'))->appends($prms);
        // </editor-fold>

		return $data;
    }

    /**
     * 登録更新画面へ遷移
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id=null)
    {
        /*-----------------------------------------
         * 1. 初期化
         *----------------------------------------- */
        // 1.1 データ受け渡し用
        $data = [];
        $data['results'] = [];

        // 1.2 メーカー：プルダウン
        $query = Manufacturers::orderBy('manufacturer_name','asc');
        $manufacturer_list = $query->pluck('manufacturer_name', 'id');
        $manufacturer_list = $manufacturer_list->prepend('', '');

        /*-----------------------------------------
         * 2. 登録更新データの取得
         *----------------------------------------- */
        // <editor-fold defaultstate="collapsed" desc=" 登録更新データの取得">
        $hasold = !empty(Input::old());
        if(!$hasold){
            if (empty($id))
            {
                /*---------------------------
                 * 2.1 新規
                 *---------------------------*/

            }
            else
            {
                /*---------------------------
                 * 2.2 修正
                 *---------------------------*/
                /* ===========================
                 * 2.2.1 初期表示データの取得
                 * =========================== */
                // <editor-fold defaultstate="collapsed" desc=" 2.2.1 初期表示データの取得">
                $query = Models::select('*',
                                        'models.id',
                                        'manufacturers.manufacturer_name'
                        );
                $query->leftjoin('manufacturers', 'manufacturers.id', '=', 'models.manufacturer_id');
                $query->where('models.id','=', $id);

                $results = $query->first();
                if(empty( $results )) {
                    // 該当がない場合、エラーにして一覧へ戻す
                    Session::flash('flash_danger', 'メーカー情報が存在しませんでした。');
                    return redirect('model'); //一覧へ戻す
                }
                $data['results']['id']                  = $results['id'];                   // ID
                $data['results']['model_name']          = $results['model_name'];           // 車種名
                $data['results']['model_kana']          = $results['model_kana'];           // 車種名カナ
                $data['results']['manufacturer_id']     = $results['manufacturer_id'];      // メーカーID
                $data['results']['manufacturer_name']   = $results['manufacturer_name'];    // メーカー名カナ
                $data['results']['remarks']             = $results['remarks'];              // 備考
                // </editor-fold>
            }

        }
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return view('model.edit', compact('data', 'manufacturer_list'));

    }

    /**
     * 登録更新処理
     *
     * @return Response
     */
    public function save()
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge  = new ChargeInfo;

        // リクエスト
        $request = Request::all();
        $id      = Funcs::rq('id', $request);

        // 結果格納用
        $data['results'] = array();

        /* ----------------------------------------------
         * 2. バリデーション
         * ----------------------------------------------*/
        $validator = Validator::make( $request, FormModels::rules(), FormModels::messages() );

        // バリデーションエラーだった場合
        if ( $validator->fails() ) {

            // メーカー：プルダウン
            $query = Manufacturers::orderBy('manufacturer_name','asc');
            $manufacturer_list = $query->pluck('manufacturer_name', 'id');
            $manufacturer_list = $manufacturer_list->prepend('', '');

            return view('model.edit', compact('data', 'manufacturer_list'))->withErrors($validator);
        }

        /* ----------------------------------------------
         * 3. DB登録
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3. DB登録">
        //存在チェック
        $isnew = empty(Manufacturers::find($id));
        if( $isnew ){
            /* ----------------------------------------
             * 4.1 新規
             * ----------------------------------------*/
            $table = new Models();
            $table->add_user_name = $charge->user_name;    // 登録者
            $edit_message = "登録";
        }else{
            /* ----------------------------------------
             * 4.2 更新
             * ----------------------------------------*/
            $table = Models::find($id);
            $edit_message = "更新";
        }
        //入力項目のセット
        $table->model_name      = $request['model_name'];           // 車種名
        $table->model_kana      = $request['model_kana'];           // 車種名カナ
        $table->manufacturer_id = $request['manufacturer_id'];      // メーカーID
        $table->remarks         = $request['remarks'];              // 備考
        $table->upd_user_name   = $charge->user_name;               // 更新者

        // 登録更新処理
        $ret = $table->save();

        // 正常終了メッセージ
        Session::flash('flash_success', $edit_message."が完了しました。");
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return redirect('model');
    }

    /**
     * 論理削除処理
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // ログイン情報
        $charge = new ChargeInfo;

        if(!empty($id)){
            $data = Models::find($id);
            if(empty($data)) {
                Session::flash('flash_danger', '削除情報が存在しませんでした。');
            }else{
                $data->delete();                            // 論理削除処理
                $data->upd_user_name = $charge->user_name;  // 更新者
                $data->save();

                // 正常終了メッセージ
                Session::flash('flash_success', "削除が完了しました。");
            }
		}

        // 一覧に戻す
        return redirect('model');
    }
}
