<?php

namespace partsmanagement\Http\Controllers;

//use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Auth;
use partsmanagement\Http\Requests\FormUsersRequest;
use partsmanagement\Libs\Funcs;
use partsmanagement\Libs\ChargeInfo;
use partsmanagement\User;
use partsmanagement\Models\CustomerDetails;

use Config;
use Carbon\Carbon;
use DB;
use Input;
use Request;
use Response;
use Session;
use Validator;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 初期表示
     *
     * @return Response
     */
    public function index()
    {
        return $this->search();
    }

    /**
     * 検索
     *
     * @return Response
     */
    public function search()
    {
        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */
        $prms = Request::all();

        // ログイン情報
        $charge  = new ChargeInfo;
        $user_id = $charge->user_id;

        // 1.2 ユーザー種別セレクトボックス
        $user_type_list = Config::get('const.user_type_list');
        array_unshift($user_type_list,'');

        /* ------------------------------------------------------
         * データ取得
         *------------------------------------------------------ */
        //一覧データ表示データ
        $results = $this->getData("search");

        /* ---------------------------
         * VIEW
         *--------------------------- */
        return view('auth.index', compact('results', 'user_id', 'user_type_list'));
    }

    /**
     * SQLの生成
     * 一覧データの取得
     * @return Response
     */
	public function getData($get_mode)
	{

        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */
        // リクエスト取得
        $prms = Request::all();
		$data['results'] = null;

        /* ------------------------------------------------------
         * 一覧データの取得
         *------------------------------------------------------ */
        // <editor-fold defaultstate="collapsed" desc=" クエリ">
        $query = User::select('users.*'
                             );

        //管理者ID
        if(!empty(Funcs::vl('user_id', $prms))){
            $query->where('user_id','like','%'.Funcs::vl('user_id', $prms).'%');
        }
        //管理者名
        if(!empty(Funcs::vl('user_name', $prms))){
            $query->where('user_name','like','%'.Funcs::vl('user_name', $prms).'%');
        }
        //ユーザー種別
        if(!empty(Funcs::vl('user_type', $prms))){
            $query->where('user_type', '=', Funcs::vl('user_type', $prms));
        }

        $query->orderBy('users.user_id','asc');
        // </editor-fold>

        $data = $query->paginate(Config::get('const.default_maxrowsize'))->appends($prms);

        return $data;
    }

    /**
     * 登録更新画面へ遷移
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id=null)
    {
        /*-----------------------------------------------------
         * 1. 初期化
         *----------------------------------------------------- */
        $data = [];
        $data['results'] = [];

        // ログイン情報
        $charge     = new ChargeInfo;
        $compare_id = $charge->id;
        $user_id    = $charge->user_id;
        $user_type  = $charge->user_type;

        /*-----------------------------------------------------
         * 2. 権限チェック
         *----------------------------------------------------- */
        if (!empty($id)) {
            if ( $compare_id != $id && $user_type != Config::get('const.user_type_list_cd')['admin'] ) {
                // 該当がない場合、エラーにして一覧へ戻す
                Session::flash('flash_danger', 'ユーザー情報が存在しませんでした。');
                return redirect('auth'); //一覧へ戻す
            }
        }

        /*-----------------------------------------------------
         * 3. 登録更新データの取得
         *----------------------------------------------------- */
        // <editor-fold defaultstate="collapsed" desc=" 登録更新データの取得">
        if (empty($id))
        {
            /*---------------------------
             * 3.1 新規
             *---------------------------*/
        }
        else
        {
            /*---------------------------
             * 3.2 修正
             *---------------------------*/
            //usersテーブル
            $query = User::select('users.*');
            $query->where('users.id', '=', $id );
            $main = $query->first();

            if(empty( $main )) {
                // 該当がない場合、エラーにして一覧へ戻す
                Session::flash('flash_danger', 'ユーザー情報が存在しませんでした。');
                return redirect('auth'); //一覧へ戻す
            }
            $data['results'] = $main; // 取得したデータをセット
        }

        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return view('auth.edit', compact('data', 'user_id'));

    }

    /**
     * パスワード更新画面へ遷移
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_customer($id=null)
    {
        /*-----------------------------------------------------
         * 1. 初期化
         *----------------------------------------------------- */
        $data = [];
        $data['results'] = [];

        // ログイン情報
        $charge  = new ChargeInfo;
        $user_id = $charge->user_id;

        /*-----------------------------------------------------
         * 2. 登録更新データの取得
         *----------------------------------------------------- */
        //usersテーブル
        $query = User::select('users.*'
                             ,'customer_details.customer_detail_name'
                             );
        $query->leftjoin('customer_details', 'users.customer_detail_id', '=', 'customer_details.id');
        $query->where('users.id', '=', $id );
        $main = $query->first();

        if( $id != $charge->id ) {
            // 該当がない場合、エラーにして一覧へ戻す
            Session::flash('flash_danger', '詳細情報が存在しませんでした。');
            return redirect('home');
        }

        if(empty( $main )) {
            // 該当がない場合、エラーにして一覧へ戻す
            Session::flash('flash_danger', '詳細情報が存在しませんでした。');
            return redirect('home');
        }
        $data['results'] = $main; // 取得したデータをセット

        //---------------------------
        // VIEW
        //---------------------------
        return view('auth.edit_customer', compact('data', 'user_id'));

    }

    /**
     * 登録更新処理
     *
     * @return Response
     */
    public function save()
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge   = new ChargeInfo;
        $mode_chk = null; // バリデーションチェック用　0:新規,1:更新(PASS更新無),2:更新(PASS更新有)

        // リクエスト
        $request = Request::all();
        $id      = $request['id'];

        // 結果格納用
        $data['results'] = array();

        /* ----------------------------------------------
         * 2. DB登録
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3. DB登録">
        //存在チェック
        if( empty($id) ){

			/* -----------------------------------------------
			 * 2.1 新規
			 * -----------------------------------------------*/
            // バリデーションチェック
			$mode_chk = 0;
            $validator = Validator::make( $request, FormUsersRequest::rules($mode_chk), FormUsersRequest::messages() );

            // バリデーションエラーだった場合
            if ( $validator->fails()) {
                return view('auth.edit', compact('data'))->withErrors($validator);
            }

            //DB登録　usersテーブル
            $table = new User();
            $table->user_id             = $request['user_id'];                  // スタッフID
	        $table->password            = bcrypt($request['password']);         // パスワード
	        $table->add_user_name       = $charge->user_name;                   // 登録者

            $edit_message = "登録";
        }else{

			/* -----------------------------------------------
			 * 2.2 更新
			 * -----------------------------------------------*/
            // DB登録　usersテーブル
            $table = User::find($id);

			/* ---------------------------------------
			 * 2.2.1 パスワードの更新があるかチェック
			 * ---------------------------------------*/
            if( $request['password'] == '' || $request['password'] == null ){

                // バリデーションチェック　(PASS更新無)
				$mode_chk = 1;
            }else{

                // バリデーションチェック　(PASS更新有)
				$mode_chk = 2;

                // DB登録　usersテーブル
		        $table->password            = bcrypt($request['password']);         // パスワード
            }

            // バリデーション実行
            $validator = Validator::make( $request, FormUsersRequest::rules($mode_chk), FormUsersRequest::messages() );

            //得意先が存在するかチェック
//            $validator->after(function($validator) use( $request ) {
//                $user_type   = $request['hdn_user_type'];
//                $cus_deta_id = $request['customer_detail_id'];
//
//                if ( $user_type == Config::get('const.user_type_list_cd')['admin'] ) {
//                    if ( !empty($cus_deta_id) ) {
//                        $dt = CustomerDetails::find($cus_deta_id);
//                        if (empty($dt)) {
//                            $msg = '選択された得意先は存在しません。';
//                            $validator->errors()->add('customer_detail_id', $msg);
//                        }
//                    }
//                }
//            });

            // バリデーションエラーだった場合
            if ( $validator->fails()) {
                $user_id = $request['user_id'];
                return view('auth.edit', compact('data', 'user_id'))->withErrors($validator);
            }

            $edit_message   = "更新";
        }

        // 項目のセット
        $table->user_name       = $request['user_name'];                            // スタッフ名
        $table->user_type       = Config::get('const.user_type_list_cd')['admin'];  // スタッフタイプ(1：管理者)固定
//        if(empty($request['hdn_user_type'])) {
//            $table->user_type           = $request['user_type'];                // スタッフタイプ（1：管理者、2：一般、3：得意先)
//        } else {
//            $table->user_type           = $request['hdn_user_type'];            // スタッフタイプ（1：管理者、2：一般、3：得意先)
//        }
//        $table->customer_detail_id  = $request['customer_detail_id'];       // 得意先詳細ID
        $table->upd_user_name       = $charge->user_name;                   // 更新者

        // 登録更新処理実行
        $ret = $table->save();

        // 正常終了メッセージ
        Session::flash('flash_success', $edit_message."が完了しました。");
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return redirect('auth');
    }

    /**
     * パスワード更新処理
     *
     * @return Response
     */
    public function save_customer()
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge     = new ChargeInfo;
        $mode_chk   = null; // バリデーションチェック用　0:新規,1:更新(PASS更新無),2:更新(PASS更新有)

        // リクエスト
        $request = Request::all();
        $id      = $request['id'];

        // DB登録　usersテーブル
        $table = User::find($id);

        // 結果格納用
        $data['results'] = array();

        /* ----------------------------------------------
         * 2. DB登録
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3. DB登録">
        //存在チェック
        if( empty($id) ){
        }else{
            /* ---------------------------------------
			 * 2.2 パスワードの更新があるかチェック
			 * ---------------------------------------*/
            if( $request['password'] == '' || $request['password'] == null ){

                // バリデーションチェック　(PASS更新無)
				$mode_chk = 3;
            }else{

                // バリデーションチェック　(PASS更新有)
				$mode_chk = 2;
                // DB登録　usersテーブル
                $table->password = bcrypt($request['password']);  // パスワード
            }

			/* -----------------------------------------------
			 * 更新
			 * -----------------------------------------------*/
            // パスワードの更新があるかチェック
            $validator = Validator::make( $request, FormUsersRequest::rules($mode_chk), FormUsersRequest::messages() );

            // バリデーションエラーだった場合
            if ( $validator->fails()) {
                $user_id = $request['user_id'];
                return view('auth.edit_customer', compact('data', 'user_id'))->withErrors($validator);
            }

            $table->user_name      = $request['user_name'];         // スタッフ名
            $table->upd_user_name  = $charge->user_name;            // 更新者

            $edit_message   = "更新";

        }

        $ret = $table->save();

        // 正常終了メッセージ
        Session::flash('flash_success', $edit_message."が完了しました。");
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return redirect('home');
    }

    /**
     * 論理削除処理
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // ログイン情報
        $charge = new ChargeInfo;

        if(!empty($id)){
            $data = User::find($id);

            if(empty($data)) {

                Session::flash('flash_danger', '削除情報が存在しませんでした。');

            }else{

                $data->delete();                          // 削除日
//                $data->upd_user_name = $charge->user_id;  // 更新者
//                $data->save();

                // 正常終了メッセージ
                Session::flash('flash_success', "削除が完了しました。");
            }
		}

        // 一覧に戻す
        return redirect('auth');
    }

}
