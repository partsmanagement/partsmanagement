<?php

namespace partsmanagement\Http\Controllers\Auth;

use partsmanagement\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Config;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    //AuthenticatesUsersトレイトで定義されたlogout()をオーバーライト。
    //最終行のreturn redirect();の内容を変更する。
    public function logout(Request $request)
    {
        $this->guard()->logout();

//        $request->session()->flush();

    //        $request->session()->regenerate();

        return redirect('login');
    }

    protected function authenticated(\Illuminate\Http\Request $request, $user)
    {
        /*-----------------------------------------------------
         * 初期設定
         *----------------------------------------------------- */
        /** @var User $user */
            $user = $request->user();

        // スタッフタイプ（1：管理者、2：一般、3：得意先)
        $user_type = $user->user_type;

        /*-----------------------------------------------------
         * ログイン後のリダイレクト
         *----------------------------------------------------- */

        /*
         *  スタッフタイプが（3：得意先)：発注画面(得意先用)へ遷移
         *  スタッフタイプが（1：管理者、2：一般)
         *      ：PCでのログインの場合は発注画面()へ遷移
         *      ：スマホでのログインの場合は配送担当者モードTOP画面へ遷移
         */
        if ( $user_type == Config::get('const.user_type_list_cd')['customer'] ) {

            // 車両一覧へ遷移
            return redirect('vehicle');
//            return redirect('home');
        }else{

            // PC画面
            if(!\Agent::isMobile()){

    //            return redirect()->intended($this->redirectPath());
                //PCでのログインの場合は車両一覧へ遷移
                return redirect('vehicle');
            }else{
            // スマートフォン画面

                //スマホでのログインの場合はTOP画面へ遷移
                return redirect('top');
            }
        }
    }

}
