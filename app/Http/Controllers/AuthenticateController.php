<?php

namespace partsmanagement\Http\Controllers;

use Illuminate\Http\Request;
//use partsmanagement\Http\Requests\FormUserRequest;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use partsmanagement\Libs\Funcs;
use partsmanagement\Libs\ChargeInfo;
use partsmanagement\User;

use Config;
use Carbon\Carbon;
use DB;
use Input;
//use Request;
use Response;
use Session;

class AuthenticateController extends Controller {

    public function authenticate(Request $request) {
      // grab credentials from the request
      $credentials = $request->only('user_id', 'password');
      try {
        // attempt to verify the credentials and create a token for the user
        if (! $token = JWTAuth::attempt($credentials)) {
          return response()->json(['error' => 'invalid_credentials'], 401);
        }
      } catch (JWTException $e) {
        // something went wrong whilst attempting to encode the token
          return response()->json(['error' => 'could_not_create_token'], 500);
      }

      // all good so return the token
      return response()->json(compact('token'));
    }

    public function getCurrentUser(Request $request)
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge  = new ChargeInfo;

        // リクエスト
        $request    = $request->only('user_id', 'password', 'fcm_token');
        $user_id    = $request["user_id"];
        $password   = $request["password"];
        $fcm_token  = $request["fcm_token"];

        // 結果格納用
        $data = array();

        // 更新判別用
        $data = User::where("user_id","=",$user_id)->first();

        /* ----------------------------------------------
         * 2. バリデーション
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 2. バリデーション">
//        $validator = Validator::make( $request, FormUserRequest::rules(), FormUserRequest::messages() );
//
//        // バリデーションエラーだった場合
//        if ( $validator->fails() ) {
//            $user = "Validation error";
//        }
//        // </editor-fold>
//
        /* ----------------------------------------------
         * 3. DB登録
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3. DB登録">
        //存在チェック
        if( empty($data) ){
            $status = "Nothing Data";
        }else{
            $data->fcm_token = $fcm_token;          // トークン
            $data->save();
            $status = "UPDATEOK";
        }
        // </editor-fold>

//        $user = JWTAuth::parseToken()->authenticate();
//        return response()->json(compact('user'));
        return $fcm_token;
    }
}