<?php

namespace partsmanagement\Http\Controllers;

//use Illuminate\Http\Request;
use partsmanagement\Http\Requests\FormParts;
use partsmanagement\Libs\Funcs;
use partsmanagement\Libs\ChargeInfo;
use partsmanagement\Models\Manufacturers;
use partsmanagement\Models\Models;
use partsmanagement\Models\Parts;

use Config;
use Carbon\Carbon;
use DB;
use Input;
use Request;
use Response;
use Session;
use Validator;


class PartsController extends Controller
{
    /**
     * コンストラクタ
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 初期表示
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->search();
    }

    /**
     * 検索
     *
     * @return Response
     */
    public function search()
    {
        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */
        //リクエスト取得
        $prms = Request::all();

        /* ------------------------------------------------------
         * データ取得
         *------------------------------------------------------ */
        //データ取得
        $results = $this->getData($prms);

        /* ---------------------------
         * VIEW
         *--------------------------- */
        return view('parts.index', compact('results', 'prms'));
    }
    /**
     * SQLの生成
     * 一覧データの取得
     * @return Response
     */
	public function getData($prms)
	{
        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */
        // リクエスト取得
        $prms = Request::all();
		$data['results'] = null;

        /* ------------------------------------------------------
         * 一覧データの取得
         *------------------------------------------------------ */
        // <editor-fold defaultstate="collapsed" desc=" クエリ">
        $query = Parts::select('*');

        // メーカー名
        if(!empty(Funcs::vl('parts_name', $prms))){
            $query->where('parts_name','like','%'.Funcs::vl('parts_name', $prms).'%');
        }

        $query->orderBy('parts.display_no','asc');

        $data = $query->get();
//        $data = $query->paginate(Config::get('const.default_30rows'))->appends($prms);
        // </editor-fold>

		return $data;
    }

    /**
     * 登録更新画面へ遷移
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id=null)
    {
        /*-----------------------------------------
         * 1. 初期化
         *----------------------------------------- */
        // 1.1 データ受け渡し用
        $data = [];
        $data['results'] = [];

        /*-----------------------------------------
         * 2. 登録更新データの取得
         *----------------------------------------- */
        // <editor-fold defaultstate="collapsed" desc=" 登録更新データの取得">
        $hasold = !empty(Input::old());
        if(!$hasold){
            if (empty($id))
            {
                /*---------------------------
                 * 2.1 新規
                 *---------------------------*/

            }
            else
            {
                /*---------------------------
                 * 2.2 修正
                 *---------------------------*/
                /* ===========================
                 * 2.2.1 初期表示データの取得
                 * =========================== */
                // <editor-fold defaultstate="collapsed" desc=" 2.2.1 初期表示データの取得">
                $query = Parts::select('*');
                $query->where('id','=', $id);

                $results = $query->first();
                if(empty( $results )) {
                    // 該当がない場合、エラーにして一覧へ戻す
                    Session::flash('flash_danger', 'パーツ情報が存在しませんでした。');
                    return redirect('parts'); //一覧へ戻す
                }
                $data['results']['id']                  = $results['id'];                   // ID
                $data['results']['parts_name']          = $results['parts_name'];           // 車種名
                $data['results']['remarks']             = $results['remarks'];              // 備考
                // </editor-fold>
            }

        }
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return view('parts.edit', compact('data'));

    }

    /**
     * 登録更新処理
     *
     * @return Response
     */
    public function save()
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge  = new ChargeInfo;

        // リクエスト
        $request = Request::all();
        $id      = Funcs::rq('id', $request);

        // 結果格納用
        $data['results'] = array();

        /* ----------------------------------------------
         * 2. バリデーション
         * ----------------------------------------------*/
        $validator = Validator::make( $request, FormParts::rules(), FormParts::messages() );

        // バリデーションエラーだった場合
        if ( $validator->fails() ) {

            return view('parts.edit', compact('data'))->withErrors($validator);
        }

        /* ----------------------------------------------
         * 3. DB登録
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3. DB登録">
        //存在チェック
        $isnew = empty(Parts::find($id));
        if( $isnew ){
            /* ----------------------------------------
             * 4.1 新規
             * ----------------------------------------*/
            $table = new Parts();
            $table->add_user_name = $charge->user_name;    // 登録者
            $edit_message = "登録";
        }else{
            /* ----------------------------------------
             * 4.2 更新
             * ----------------------------------------*/
            $table = Parts::find($id);
            $edit_message = "更新";
        }
        //入力項目のセット
        $table->parts_name      = $request['parts_name'];           // パーツ名
        $table->remarks         = $request['remarks'];              // 備考
        $table->upd_user_name   = $charge->user_name;               // 更新者

        // 登録更新処理
        $ret = $table->save();

        // 正常終了メッセージ
        Session::flash('flash_success', $edit_message."が完了しました。");
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return redirect('parts');
    }

    /**
     * 表示順変更処理
     *
     * @return Response
     */
    public function saveno()
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge  = new ChargeInfo;

        // リクエスト
        $request = Request::all();
        $id      = Funcs::rq('id', $request);

        // 結果格納用
        $data['results']    = array();
        $data['detail']     = array();

        /* ----------------------------------------------
         * 2. DB登録
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 2. DB登録">
        //存在チェック
        $isnew = empty(Parts::all());
        if( $isnew ){
        }else{
            /* ----------------------------------------
             * 2.1 パーツテーブル登録
             * ----------------------------------------*/
            $cnt = count($id);
            if ( $cnt > 0 ) {

                $i = 1;
                foreach ( $id as $idrow ) {

                    if ( !empty($idrow) ) {
                        $dt = Parts::find($idrow);

                        //入力項目のセット
                        $dt->display_no     = $i;                       // 表示番号
                        $dt->upd_user_name  = $charge->user_name;       // 更新者

                        // 登録更新処理
                        $ret = $dt->save();
                        $i++;
                    }
                }

            }
        }

        // 正常終了メッセージ
        Session::flash('flash_success', "表示順を更新しました。");
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return redirect('parts'); //エラーがあった時は詳細ページに留まる
    }

    /**
     * 論理削除処理
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // ログイン情報
        $charge = new ChargeInfo;

        if(!empty($id)){
            $data = Parts::find($id);
            if(empty($data)) {
                Session::flash('flash_danger', '削除情報が存在しませんでした。');
            }else{
                $data->delete();                            // 論理削除処理
                $data->upd_user_name = $charge->user_name;  // 更新者
                $data->save();

                // 正常終了メッセージ
                Session::flash('flash_success', "削除が完了しました。");
            }
		}

        // 一覧に戻す
        return redirect('parts');
    }
}
