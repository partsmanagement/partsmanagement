<?php

namespace partsmanagement\Http\Middleware;

use partsmanagement\User;
use Closure;
use Config;
use Illuminate\Http\Request;

//use Illuminate\Foundation\Http\Middleware\VerifyAdminUser as Middleware;

class VerifyAdminUser
{
    /**
     * @param Request $request
     * @param Closure $next
     * @param null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user_type = "";

        /** @var User $user */
        $user = $request->user();

        // スタッフタイプ（1：管理者、2：一般、3：得意先)
        if ( !empty($user) ) {
            $user_type = $user->user_type;
        }

        // 管理者、一般以外の権限(得意先)の場合404表示
        // 404該当ページはweb.phpにてグループ化
        if ( $user_type == Config::get('const.user_type_list_cd')['customer'] ) {
//            abort(404);
            return redirect('/home');
        }

        return $next($request);
    }
}