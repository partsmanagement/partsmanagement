<?php

namespace partsmanagement\Http\Requests;

//use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class FormTaxRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            // 施工日
            'const_date' => 'required|date',
            // 消費税率
            'tax_basic_rate'  => 'required|integer|min:0|max:99',
            // 軽減税率
            'reduction_tax_rate'  => 'required|integer|min:0|max:99',
            // 税区分（1:外税、2:内税）
            'tax_flag'  => 'required|integer|between:1,2',
            // 税計算区分（1:伝票単位、2:商品単位）
            'tax_calc_flag'  => 'required|integer|between:1,2',
            // 税端数計算区分（1:切捨て、2:四捨五入、3:切り上げ）
            'tax_fraction_type'  => 'required|integer|between:1,3',
        ];
    }

    public static function messages()
    {
        return [
//            'customer_kana.all_en_kana'     => ':attributeは半角カタカナで入力してください。',
//            'postalcode.zip_hyphen'         => ':attributeはハイフン付き（半角数字3桁-半角数字4桁）で入力してください。',
        ];
    }

}
