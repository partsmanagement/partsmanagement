<?php

namespace partsmanagement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class FormUsersRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules($mode_chk)
    {

        if($mode_chk == 0){

            return [
                // ユーザーID
                'user_id'                    => 'required | max:20 | unique:users,user_id',
                // ユーザー名
                'user_name'                  => 'required | max:40',
                // スタッフタイプ
                'user_type'                  => '',
                // 得意先詳細ID
                'customer_detail_id'         => 'required_if:user_type,3',
                // パスワード
                'password'                   => 'required | min:6 | confirmed | string | nullable  | alpha_num_check',
                // パスワード(確)
                'password_confirmation'      => 'required | min:6 | string | nullable | alpha_num_check',
            ];

        }else if($mode_chk == 1){

            return [
                // ユーザー名
                'user_name'                  => 'required | max:40',
                // スタッフタイプ
                'user_type'                  => '',
                // 得意先詳細ID
                'customer_detail_id'         => 'required_if:user_type,3',
            ];

        //スマホ：パス更新無し
        }else if($mode_chk == 3){

            return [
                // ユーザー名
                'user_name'                  => 'required | max:40',
            ];

        }else{

            return [
                // ユーザー名
                'user_name'                  => 'required | max:40',
                // ユーザー種別
                'user_type'                  => '',
                // 得意先詳細ID
                'customer_detail_id'         => 'required_if:user_type,3',
                // パスワード
                'password'                   => 'required | min:6 | confirmed | string | nullable  | alpha_num_check',
                // パスワード(確)
                'password_confirmation'      => 'required | min:6 | string | nullable | alpha_num_check',
            ];

        }

    }

    public static function messages()
    {
        return [
            'customer_detail_id.required_if'    => 'ユーザー種別が「得意先」の時、得意先は必須です。',
            'customer_detail_id.exists'         => '選択された得意先は存在しません。',
//            'tel.tel_hankaku_num'               => ':attributeは半角数字で入力してください。',
//            'exh_time.*.required_unless'      => '最大プレート数が入力されています。:attributeを選択してください。',
//            'plate_maxnum.*.required_with'  => '開催時間が選択されています。:attributeを入力してください。',
        ];
    }

}
