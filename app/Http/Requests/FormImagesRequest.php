<?php

namespace partsmanagement\Http\Requests;

//use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class FormImagesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            // メイン画像ファイル名
            'main_file_names'                   => 'nullable|file|image|mimes:jpeg,png,jpg|max:5000',
        ];
    }

    public static function messages()
    {
        return [
//            'customer_kana.all_en_kana'     => ':attributeは半角カタカナで入力してください。',
//            'postalcode.zip_hyphen'         => ':attributeはハイフン付き（半角数字3桁-半角数字4桁）で入力してください。',
        ];
    }

}
