<?php

namespace partsmanagement\Http\Requests;

//use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class FormYards extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            // ヤード名
            'yard_name'        => 'required|string|min:0|max:60',
        ];
    }

    public static function messages()
    {
        return [
//            'customer_kana.all_en_kana'     => ':attributeは半角カタカナで入力してください。',
//            'postalcode.zip_hyphen'         => ':attributeはハイフン付き（半角数字3桁-半角数字4桁）で入力してください。',
        ];
    }

}
