<?php

namespace partsmanagement\Http\Requests;

//use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class FormModels extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            // 車種名
            'model_name'        => 'required|string|min:0|max:60',
            // 車種名カナ
            'model_kana'        => 'nullable|string|min:0|max:80',
            // メーカーID
            'manufacturer_id'   => 'required',
        ];
    }

    public static function messages()
    {
        return [
//            'customer_kana.all_en_kana'     => ':attributeは半角カタカナで入力してください。',
//            'postalcode.zip_hyphen'         => ':attributeはハイフン付き（半角数字3桁-半角数字4桁）で入力してください。',
        ];
    }

}
