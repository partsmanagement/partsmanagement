<?php

namespace partsmanagement\Http\Requests;

//use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class FormVehicles extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            // 車体番号
            'vehicle_number'                    => 'nullable|string|max:40',
            // 初年度検査年月
//            'first_registration_date'           => 'required|nullable|date_format:Y-m|max:7',
            // メーカーID
            'manufacturer_id'                   => 'required',
//            // 車種ID
//            'model_id'                          => 'required',
            // 車種名
            'model_name'                        => 'required|string|max:60',
            // メイン画像ファイル名
            'main_file_names'                   => 'nullable|file|image|mimes:jpeg,png,jpg|max:5000',
//            // カラー
//            'vehicle_color'                     => 'nullable|string|max:40',
//            // パーツ
//            'parts'                             => 'required',
            // トランスミッション（1：AT、2：MT）
            'transmission'                      => 'nullable|string|max:40',
            // 型式
            'model_number'                      => 'nullable|string|max:40',
            // 原動機型式
            'prime_mover_model'                 => 'nullable|string|max:40',
            // 型式指定番号
            'model_designation_number'          => 'nullable|numeric',
            // 類別区分番号
            'category_classification_number'    => 'nullable|numeric',
//            // ヤード
//            'yard'                              => 'nullable|string|max:40',
            // 特記事項
            'remarks'                           => 'nullable|string|max:40',
            // 提供者名
            'provider_name'                     => 'nullable|string|max:40',
            // 取得金額
            'acquisition_amount'                => 'nullable|numeric|max:9999999999',
            // 検索キーワード
            'search_keyword'                    => 'nullable|string|max:40',
        ];
    }

    public static function messages()
    {
        return [
//            'customer_kana.all_en_kana'     => ':attributeは半角カタカナで入力してください。',
//            'postalcode.zip_hyphen'         => ':attributeはハイフン付き（半角数字3桁-半角数字4桁）で入力してください。',
        ];
    }

}
