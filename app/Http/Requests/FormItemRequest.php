<?php

namespace partsmanagement\Http\Requests;

//use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class FormItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            // 商品コード
            'item_code'             => 'required|string|max:20',
            // 商品名
            'item_name'             => 'required|string|max:60',
            // 商品名カナ
            //'item_name_kana'        => 'nullable|string|max:80|all_kana_all',
            'item_name_kana'        => 'nullable|string|max:80|all_kana',
            // 商品簡略名
            'item_short_name'       => 'nullable|string|max:40',
            // 商品画像ファイル名
            //'item_file_name'      => 'nullable',
            // 商品画像
            'item_file_name'        => 'nullable|file|image|mimes:jpeg,png,jpg,gif|max:3000',            // 商品画像
            //'photo'                 => 'nullable|file|image|mimes:jpeg,png,jpg,gif|max:3000',
            // 商品原価
            'item_cost_price'       => 'nullable|numeric|min:-99999999|max:99999999.99',
            // 商品規格
            'item_method'           => 'nullable|string|max:60',
            // 商品入数
            'item_quantity'         => 'nullable|numeric|min:-999999|max:99999.99',
            // 商品入数単位
            'item_quantity_unit'    => 'nullable|string|max:20',
            // 商品単位
            'item_unit'             => 'nullable|string|max:20',
            // 卸売通常価格
            'wholesale_basic_price' => 'nullable|numeric|min:-9999999|max:99999999',
            // 小売通常価格
            'retail_basic_price'    => 'nullable|numeric|min:-9999999|max:99999999',
            // 卸売特売価格
            'wholesale_sale_price'  => 'nullable|numeric|min:-9999999|max:99999999',
            // 小売特売価格
            'retail_sale_price'     => 'nullable|numeric|min:-9999999|max:99999999',
            // 卸売ランク価格１
            'wholesale_rank_price1' => 'numeric|min:-9999999|max:99999999',
            // 小売ランク価格１
            'retail_rank_price1'    => 'numeric|min:-9999999|max:99999999',
            // 卸売ランク価格２
            'wholesale_rank_price2' => 'numeric|min:-9999999|max:99999999',
            // 小売ランク価格２
            'retail_rank_price2'    => 'numeric|min:-9999999|max:99999999',
            // 卸売ランク価格３
            'wholesale_rank_price3' => 'numeric|min:-9999999|max:99999999',
            // 小売ランク価格３
            'retail_rank_price3'    => 'numeric|min:-9999999|max:99999999',
            // 卸売ランク価格４
            'wholesale_rank_price4' => 'numeric|min:-9999999|max:99999999',
            // 小売ランク価格４
            'retail_rank_price4'    => 'numeric|min:-9999999|max:99999999',
            // 卸売ランク価格５
            'wholesale_rank_price5' => 'numeric|min:-9999999|max:99999999',
            // 小売ランク価格５
            'retail_rank_price5'    => 'numeric|min:-9999999|max:99999999',
            // 商品表示区分（1：表示、99：非表示）
            'item_disp_type'        => 'required',
        ];

    }

    public static function messages()
    {
        return [
            'item_name_kana.all_kana_all'     => ':attributeはカタカナで入力してください。',
        ];
    }

}
