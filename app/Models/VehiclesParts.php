<?php

namespace partsmanagement\Models;

use partsmanagement\Libs\Funcs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehiclesParts extends Model
{
//    use SoftDeletes;

    // モデルと関連しているテーブル ※クラス名と不一致の場合に設定する
    protected $table = 'vehicles_parts';

    // 主キーとなるカラム
    protected $primaryKey = 'id';

    // 主キーがAutoIncrementであるか
    public $incrementing = true;

    // 主キーとなるカラム
    protected $keyType = 'int';

    // モデルのタイムスタンプを更新するかの指示
    public $timestamps = true;


}
