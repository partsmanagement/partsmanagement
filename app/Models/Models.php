<?php

namespace partsmanagement\Models;

use partsmanagement\Libs\Funcs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Models extends Model
{
    use SoftDeletes;

    // モデルと関連しているテーブル ※クラス名と不一致の場合に設定する
    protected $table = 'models';

    // 主キーとなるカラム
    protected $primaryKey = 'id';

    // 主キーがAutoIncrementであるか
    public $incrementing = true;

    // 主キーとなるカラム
    protected $keyType = 'int';

    // モデルのタイムスタンプを更新するかの指示
    public $timestamps = true;

    // アクティブな行を取得
    public static function activerows() {
        // return self::where('del_flg',config('const.alive'));
        return self::current();
    }

}
