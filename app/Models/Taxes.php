<?php

namespace partsmanagement\Models;

use partsmanagement\Libs\Funcs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Taxes extends Model
{
    use SoftDeletes;

    // モデルと関連しているテーブル ※クラス名と不一致の場合に設定する
    protected $table = 'taxes';

    // 主キーとなるカラム
    protected $primaryKey = 'id';

    // 主キーがAutoIncrementであるか
    public $incrementing = true;

    // 主キーとなるカラム
    protected $keyType = 'int';

    // モデルのタイムスタンプを更新するかの指示
    public $timestamps = true;

    // アクティブな行を取得
    public static function activerows() {
        // return self::where('del_flg',config('const.alive'));
        return self::current();
    }

    // 消費税率を取得
    public static function getBasicRate() {
        $result = 0;
        $row = self::current();

        if(!empty($row)){
            $result = Funcs::val($row['tax_basic_rate']);
        }
        return $result;
    }

    // 軽減税率を取得
    public static function getReductionTaxRate() {
        $result = 0;
        $row = self::current();

        if(!empty($row)){
            $result = Funcs::val($row['reduction_tax_rate']);
        }
        return $result;
    }

    // 税区分（1:外税、2:内税）を取得
    public static function getTaxFlag() {
        $result = 1;
        $row = self::current();

        if(!empty($row)){
            $result = Funcs::val($row['tax_flag']);
        }
        return $result;
    }

    // 税計算区分（1:伝票単位、2:商品単位）を取得
    public static function getTaxCalcFlag() {
        $result = 1;
        $row = self::current();

        if(!empty($row)){
            $result = Funcs::val($row['tax_calc_flag']);
        }
        return $result;
    }

    // 税端数計算区分（1:切捨て、2:四捨五入、3:切り上げ）を取得
    public static function getTaxFractionType() {
        $result = 1;
        $row = self::current();

        if(!empty($row)){
            $result = Funcs::val($row['tax_fraction_type']);
        }
        return $result;
    }

    // 現在、施工されている税情報を取得
    public static function current() {
        return self::where('const_date','<',date('Y/m/d H:i:s'))->orderBy('const_date', 'DESC')->first();
    }

}
