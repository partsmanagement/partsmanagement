<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
    return redirect('login');
});

Route::get('/home',                 'HomeController@index')->name('home');

/*
|--------------------------------------------------------------------------
| ユーザー管理
|--------------------------------------------------------------------------
 */
//ログイン/登録
Auth::routes();

/*
|--------------------------------------------------------------------------
| データ取得（AJAX）
|--------------------------------------------------------------------------
*/
// 車種マスタ
Route::get('json/models/{id}',                'Api\Master\ModelController@json');
Route::get('search/models/{key}',             'Api\Master\ModelController@search');
Route::get('getmanufacturer/models/{key}',    'Api\Master\ModelController@getManufacturer');

// 商品マスタ
Route::get('json/item/{id}',                'Api\Master\ItemController@json');
Route::get('search/item/{key}',             'Api\Master\ItemController@search');
// 得意先マスタ
Route::get('json/customer/{id}',            'Api\Master\CustomerController@json');
Route::get('search/customer/{key}',         'Api\Master\CustomerController@search');
Route::get('searchonly/customer/{key}',     'Api\Master\CustomerController@searchonly');
// 得意先詳細マスタ
Route::get('json/cusdetails/{id}',          'Api\Master\CustomerDetailsController@json');
Route::get('jsoncust/cusdetails/{id}',      'Api\Master\CustomerDetailsController@json_cust');
Route::get('jsonroot/cusdetails/{id}',      'Api\Master\CustomerDetailsController@json_root');
Route::get('search/cusdetails/{key}',       'Api\Master\CustomerDetailsController@search');
Route::get('searchonly/cusdetails/{key}',   'Api\Master\CustomerDetailsController@searchonly');
// 得意先商品価格マスタ
Route::get('json/custitem/{id}',             'Api\Master\CustomerItemPricesController@json');
Route::get('json/custitemcd',                'Api\Master\CustomerItemPricesController@jsonitemcd');
Route::get('search/custitem/{key}',          'Api\Master\CustomerItemPricesController@search');

/*
|--------------------------------------------------------------------------
| 下記ルートグループ(得意先は遷移できない)
|--------------------------------------------------------------------------
 */
Route::group(['middleware' => 'administrator'], function () {
    /*
    |--------------------------------------------------------------------------
    | TOP
    |--------------------------------------------------------------------------
     */
    Route::get('/top',                  'TopController@index')->name('top');

    /*
    |--------------------------------------------------------------------------
    | ユーザー管理
    |--------------------------------------------------------------------------
     */
    //初期表示
    Route::get('/auth',                 'UsersController@index')->name('auth');
    // 一覧、検索
    Route::get('auth/search',           'UsersController@search');
    // 更新
    Route::get('auth/edit',             'UsersController@edit');
    Route::get('auth/edit/{id}',        'UsersController@edit');
    // 登録更新処理
    Route::post('auth/save',            'UsersController@save');
    // 削除
    Route::post('auth/delete/{id}',     'UsersController@delete');
    Route::get('auth/delete/{id}',      'UsersController@delete');

    /*
    |--------------------------------------------------------------------------
    | メーカーマスタ
    |--------------------------------------------------------------------------
     */
    // 一覧、検索
    Route::get('manufacturer',               'ManufacturerController@index');
    Route::get('manufacturer/search',        'ManufacturerController@search');
    // 更新
    Route::get('manufacturer/edit',          'ManufacturerController@edit');
    Route::get('manufacturer/edit/{id}',     'ManufacturerController@edit');
    // 登録更新処理
    Route::post('manufacturer/save',         'ManufacturerController@save');
    // 削除
    Route::post('manufacturer/delete/{id}',  'ManufacturerController@delete');
    Route::get('manufacturer/delete/{id}',   'ManufacturerController@delete');

    /*
    |--------------------------------------------------------------------------
    | 車種マスタ
    |--------------------------------------------------------------------------
     */
    // 一覧、検索
    Route::get('model',                     'ModelController@index');
    Route::get('model/search',              'ModelController@search');
    // 更新
    Route::get('model/edit',                'ModelController@edit');
    Route::get('model/edit/{id}',           'ModelController@edit');
    // 登録更新処理
    Route::post('model/save',               'ModelController@save');
    // 削除
    Route::post('model/delete/{id}',        'ModelController@delete');
    Route::get('model/delete/{id}',         'ModelController@delete');

    /*
    |--------------------------------------------------------------------------
    | パーツマスタ
    |--------------------------------------------------------------------------
     */
    // 一覧、検索
    Route::get('parts',                     'PartsController@index');
    Route::get('parts/search',              'PartsController@search');
    // 更新
    Route::get('parts/edit',                'PartsController@edit');
    Route::get('parts/edit/{id}',           'PartsController@edit');
    // 登録更新処理
    Route::post('parts/save',               'PartsController@save');
    // 表示順変更処理
    Route::post('parts/saveno',             'PartsController@saveno');
    // 削除
    Route::post('parts/delete/{id}',        'PartsController@delete');
    Route::get('parts/delete/{id}',         'PartsController@delete');

    /*
    |--------------------------------------------------------------------------
    | 商品マスタ
    |--------------------------------------------------------------------------
     */
    // 一覧、検索
    Route::get('item',               'ItemController@index');
    Route::get('item/search',        'ItemController@search');
    // 更新
    Route::get('item/edit',          'ItemController@edit');
    Route::get('item/edit/{id}',     'ItemController@edit');
    // 登録更新処理
    Route::post('item/save',         'ItemController@save');
    // 削除
    Route::post('item/delete/{id}',  'ItemController@delete');
    Route::get('item/delete/{id}',   'ItemController@delete');
    // CSVダウンロード
    Route::get('item/csvput',        'ItemController@csvput');

    /*
    |--------------------------------------------------------------------------
    | ヤードマスタ
    |--------------------------------------------------------------------------
     */
    // 一覧、検索
    Route::get('yard',                     'YardController@index');
    Route::get('yard/search',              'YardController@search');
    // 更新
    Route::get('yard/edit',                'YardController@edit');
    Route::get('yard/edit/{id}',           'YardController@edit');
    // 登録更新処理
    Route::post('yard/save',               'YardController@save');
    // 削除
    Route::post('yard/delete/{id}',        'YardController@delete');
    Route::get('yard/delete/{id}',         'YardController@delete');

    /*
    |--------------------------------------------------------------------------
    | 税率マスタ
    |--------------------------------------------------------------------------
     */
    // 一覧、検索
    Route::get('tax',               'TaxController@index');
    Route::get('tax/search',        'TaxController@search');
    // 更新
    Route::get('tax/edit',          'TaxController@edit');
    Route::get('tax/edit/{id}',     'TaxController@edit');
    // 登録更新処理
    Route::post('tax/save',         'TaxController@save');
    // 削除
    Route::post('tax/delete/{id}',  'TaxController@delete');
    Route::get('tax/delete/{id}',   'TaxController@delete');

    /*
    |--------------------------------------------------------------------------
    | 車両
    |--------------------------------------------------------------------------
     */
    // 一覧、検索
    Route::get('vehicle',                   'VehiclesController@index');
    Route::get('vehicle/search',            'VehiclesController@search');
    // 更新
    Route::get('vehicle/edit',              'VehiclesController@edit');
    Route::get('vehicle/edit/{id}',         'VehiclesController@edit');
    // 登録更新処理
    Route::post('vehicle/save',             'VehiclesController@save');
    Route::post('vehicle/saveimages',       'VehiclesController@saveimages');
    // 画像更新
    Route::get('vehicle/images',            'VehiclesController@images');
    Route::get('vehicle/images/{id}',       'VehiclesController@images');
    // 削除
    Route::post('vehicle/delete/{id}',      'VehiclesController@delete');
    Route::get('vehicle/delete/{id}',       'VehiclesController@delete');
    Route::post('vehicle/imagedelete',      'VehiclesController@imagedelete');
    Route::get('vehicle/imagedelete/{id}',  'VehiclesController@imagedelete');
    // 車両パーツ一覧、検索
    Route::get('vehicle/parts',             'VehiclesController@parts');
    Route::get('vehicle/parts/{id}',        'VehiclesController@parts');
    Route::post('vehicle/saveparts',        'VehiclesController@saveparts');



});
